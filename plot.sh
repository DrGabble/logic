#!/bin/bash

set -eu


rm -rf run/*
mkdir -p run

echo "Building dot files..."
RUST_BACKTRACE=1 cargo run --bin sim -- plot $@

# dot -Tps run/chip_000.dot -o run/chip_000.ps && firefox run/chip_000.ps
# exit 1

file_type=svg

echo "Creating images..."
for dot_file in run/*.dot; do
    dot -T$file_type $dot_file -o ${dot_file%.dot}.$file_type
done

echo "Opening viewer"
eog run/chip*.$file_type