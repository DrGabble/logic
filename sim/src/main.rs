#![allow(incomplete_features)]
use structopt::StructOpt;

mod input;
mod session;
mod stats;

use circuit::pinout::Pinout;
use circuit::plot;
use circuit::{TestCase, TestSuite};

use input::{BuildArgs, Input};
use session::Session;

#[derive(StructOpt)]
#[structopt(
    name = "sim",
    about = "Simulates chips, with interactive debugging and plotting."
)]
enum Args {
    #[structopt(about = "Start a repl loop to run and examine a chip")]
    Run,

    #[structopt(about = "Build and plot a chip of a sequence of simulated steps")]
    Plot(PlotArgs),

    Stats(stats::Args),
}

#[derive(StructOpt)]
struct PlotArgs {
    #[structopt(flatten)]
    build_args: BuildArgs,

    #[structopt(
        short,
        long,
        default_value = "0",
        help = "Delay in steps before starting loop"
    )]
    wait: usize,

    #[structopt(
        short,
        long,
        default_value = "10",
        help = "Number of simulation steps to perform"
    )]
    steps: usize,

    #[structopt(
        short = "C",
        long,
        default_value = "0",
        help = "Steps per single clock cycle"
    )]
    clock: usize,

    #[structopt(short, long, help = "Don't render the lowest logic primitives")]
    ignore_primitives: bool,

    #[structopt(
        short,
        long,
        default_value = "auto",
        possible_values=&["auto", "never", "always"],
        help="Wether to split multiple transputs between the same components"
    )]
    transputs_mode: plot::SplitTransputMode,

    #[structopt(parse(try_from_str))]
    inputs: Vec<Input>,
}

fn main() -> Result<(), String> {
    match Args::from_args() {
        Args::Run => repl_loop(),
        Args::Plot(args) => {
            let mut device = args.build_args.build_device()?;
            let test_suite = build_test_suite(&args, &device)?;
            let mut settings = plot::Settings::default();
            settings.set_plot_primitives(!args.ignore_primitives);
            settings.set_split_transputs(args.transputs_mode);
            std::fs::create_dir_all("./run").map_err(|_| "could not create ./run".to_string())?;
            let mut sequence = plot::Sequence::new("./run/chip", settings);
            sequence.take(&mut device, &test_suite);
        }
        Args::Stats(args) => stats::print(&args)?,
    }
    Ok(())
}

fn build_test_suite(args: &PlotArgs, device: &dyn Pinout) -> Result<TestSuite, String> {
    let mut suite = TestSuite::default();
    if args.wait > 0 {
        let mut case = TestCase::default();
        case.set_steps(args.wait);
        suite.add(case);
    }
    {
        let mut case = TestCase::default();
        case.set_steps(args.steps);
        case.set_clock_period(args.clock);
        let pinout = device.labels();
        for input in args.inputs.iter() {
            input.set(&pinout, &mut case)?;
        }
        suite.add(case);
    }
    Ok(suite)
}

const HISTORY: &str = "./.logic_history.txt";

fn repl_loop() {
    use rustyline::error::ReadlineError;
    // `()` can be used when no completer is required
    let mut editor = rustyline::Editor::<(), rustyline::history::FileHistory>::new().unwrap();
    if editor.load_history(HISTORY).is_err() {
        println!("(No previous history found)");
    }
    let mut session = Session::default();
    while !session.is_complete() {
        match editor.readline(">> ") {
            Ok(line) => {
                editor.add_history_entry(line.as_str()).unwrap();
                if let Err(error) = session.run_command(line.as_str()) {
                    println!("{}", error);
                }
            }
            Err(ReadlineError::Interrupted) => {
                continue;
            }
            Err(ReadlineError::Eof) => {
                break;
            }
            Err(err) => {
                println!("Error: {:?}", err);
                break;
            }
        }
    }
    editor.save_history(HISTORY).unwrap();
}
