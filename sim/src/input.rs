use structopt::StructOpt;

use circuit::gate;
use circuit::pinout::{FindNamedRange, NamedRange};
use circuit::{ChipDevice, Optimizer, Plan, SimpleEmulator, TestCase};

//
// Build Arguments
//

#[derive(StructOpt)]
pub struct BuildArgs {
    #[structopt(possible_values=&*gate::builder_labels(), case_insensitive=true, help="Logic gate to build")]
    pub name: String,

    #[structopt(short = "c", parse(from_str))]
    pub constructors: Vec<Constructor>,

    #[structopt(
        short,
        long,
        default_value = "3",
        help = "Level of nested components to save for rendering"
    )]
    pub depth: usize,

    #[structopt(
        short,
        long,
        help = "Attempt to minimise the number of components and runtime"
    )]
    pub optimize: bool,
}

impl BuildArgs {
    pub fn build_device(&self) -> Result<ChipDevice<SimpleEmulator>, String> {
        let mut builder = gate::find_builder(&self.name)?;
        for constructor in self.constructors.iter() {
            builder.set_field(&constructor.name, &constructor.value)?;
        }
        let mut plan = Plan::from_chip(&*builder, self.depth);
        if self.optimize {
            Optimizer::new(&mut plan).run();
        }
        Ok(ChipDevice::<SimpleEmulator>::from(&plan))
    }
}

//
// Constructor (Builder Argument)
//

#[derive(Debug)]
pub struct Constructor {
    //bits=1
    //shiftype=logical
    pub name: String,
    pub value: String,
}

impl std::convert::From<&str> for Constructor {
    fn from(string: &str) -> Self {
        // TODO use TryFrom?
        let equals = string.find('=').expect("Must contain equals");

        Self {
            name: String::from(&string[0..equals]),
            value: string[(equals + 1)..string.len()].to_string(),
        }
    }
}

//
// Input
//

#[derive(Debug)]
pub enum Input {
    //power=1
    NamedRange {
        name: String,
        value: String,
    },
    //power:0=1
    NamedPin {
        name: String,
        offset: usize,
        value: bool,
    },
    //0..1=1
    Range {
        indices: std::ops::Range<usize>,
        value: usize,
    },
    //0=1
    Pin {
        index: usize,
        value: bool,
    },
}

impl std::str::FromStr for Input {
    type Err = String;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        let equals = string
            .find('=')
            .ok_or_else(|| "Input must contain an equals ('=')".to_string())?;

        if let Some(colon) = string[0..equals].find(':') {
            return Ok(Input::NamedPin {
                name: String::from(&string[0..colon]),
                offset: string[(colon + 1)..equals]
                    .parse()
                    .map_err(|_| "Expected integer after colon")?,
                value: string[(equals + 1)..string.len()]
                    .parse()
                    .map_err(|_| "Expected boolean after equals")?,
            });
        }

        if let Some(dots) = string[0..equals].find("..") {
            let start: usize = string[0..dots]
                .parse()
                .map_err(|_| "Expected integer before elipsis")?;
            let end: usize = string[(dots + 2)..equals]
                .parse()
                .map_err(|_| "Expected integer after elipsis")?;
            return Ok(Input::Range {
                indices: start..end,
                value: string[(equals + 1)..string.len()]
                    .parse()
                    .map_err(|_| "Expected integer after equals")?,
            });
        }

        if let Ok(index) = string[0..equals].parse() {
            return Ok(Input::Pin {
                index,
                value: string[(equals + 1)..string.len()]
                    .parse()
                    .map_err(|_| "Expected boolean after equals")?,
            });
        }

        Ok(Input::NamedRange {
            name: String::from(&string[0..equals]),
            value: string[(equals + 1)..string.len()]
                .parse()
                .map_err(|_| "Expected integer after equals")?,
        })
    }
}

impl Input {
    pub fn set(&self, pinout: &[NamedRange], case: &mut TestCase) -> Result<(), String> {
        match self {
            Input::Pin { index, value } => case.set_input(*index, *value),
            Input::NamedPin {
                name,
                offset,
                value,
            } => {
                let index = pinout
                    .find(name)
                    .ok_or(format!("No input or output '{}'", name))?
                    .get_nth(*offset)
                    .ok_or(format!("Index '{}' is out of range for '{}'", offset, name))?;
                case.set_input(index, *value);
            }
            Input::Range { indices, value } => {
                let mut value = *value;
                for index in indices.clone() {
                    case.set_input(index, value & 1 == 1);
                    value >>= 1;
                }
            }
            Input::NamedRange { name, value } => {
                let mut value: usize = value
                    .parse()
                    .map_err(|_| format!("Value for '{}' is not integer", name))?;
                for index in pinout
                    .find(name)
                    .ok_or(format!("No input or output '{}'", name))?
                    .range()
                {
                    case.set_input(index, value & 1 == 1);
                    value >>= 1;
                }
            }
        }
        Ok(())
    }
}
