use std::convert::TryFrom;

use structopt::StructOpt;

use circuit::device::Device;
use circuit::pinout::Pinout;
use circuit::{gate, ChipBuilder, ChipDevice, Optimizer, SimpleEmulator};
use circuit::{Component, Plan, TestCase};

use super::input::{BuildArgs, Input};
use super::stats;

//
// Arguments to Session commands
//
#[derive(StructOpt)]
#[structopt(name = "", version = " ")]
enum Command {
    #[structopt(about = "Exit the program immediately")]
    Quit,

    #[structopt(about = "List all gates available to test")]
    List,

    Stats(stats::Args),

    #[structopt(about = "Build a new chip to simulate, replacing the current (if any)")]
    Build(BuildArgs),

    #[structopt(about = "Show current chip being simulated, and all it's outputs")]
    Show,

    #[structopt(about = "Show current chip being simulated, and all it's outputs")]
    Tree {
        depth: Option<usize>,
    },

    #[structopt(about = "Set an input for the current simulation")]
    Set {
        #[structopt(parse(try_from_str))]
        inputs: Vec<Input>,
    },

    #[structopt(about = "Set clock period (steps between clock toggling)")]
    Clock {
        clock_period: usize,
    },

    #[structopt(about = "Run one step of the simulation")]
    Step {
        #[structopt(default_value = "1")]
        steps: usize,
    },

    #[structopt(about = "Run multiple steps of the simulation, until gate delay reached")]
    Run,

    #[structopt(about = "Reset device to step 0, to restart simulation")]
    Reset,

    #[structopt(about = "Clear all inputs")]
    Clear,
}

//
// Session Object
//

const NO_SIM_ERR: &str = "No simulation device built (see 'build')";

pub type CommandResult = Result<(), String>;

#[derive(Default)]
pub struct Session {
    is_complete: bool,
    simulation: Option<Simulation>,
    test_case: TestCase,
}

impl Session {
    pub fn is_complete(&self) -> bool {
        self.is_complete
    }

    pub fn run_command(&mut self, line: &str) -> CommandResult {
        if line.is_empty() {
            return Ok(());
        }
        let arguments = std::iter::once("").chain(line.split_ascii_whitespace());
        let command = match Command::from_iter_safe(arguments) {
            Ok(command) => command,
            Err(error) => return Err(error.message),
        };
        match command {
            Command::Quit => self.quit(),
            Command::List => self.list(),
            Command::Stats(args) => stats::print(&args),
            Command::Build(args) => self.build(&args),
            Command::Set { inputs } => self.set_inputs(&inputs),
            Command::Clock { clock_period } => self.set_clock(clock_period),
            Command::Show => self.show(),
            Command::Tree { depth } => self.tree(depth),
            Command::Step { steps } => self.step(steps),
            Command::Run => self.run(),
            Command::Reset => self.reset(),
            Command::Clear => self.clear(),
        }
    }

    fn quit(&mut self) -> CommandResult {
        self.is_complete = true;
        Ok(())
    }

    fn list(&self) -> CommandResult {
        for label in gate::builder_labels() {
            let builder = gate::find_builder(label).unwrap();
            println!("  {:20} '{}'", label, builder.name());
        }
        Ok(())
    }

    fn build(&mut self, build_args: &BuildArgs) -> CommandResult {
        let simulation = Simulation::try_from(build_args)?;
        println!(
            "Replacing '{}' with '{}'",
            self.simulation
                .as_ref()
                .map(|s| s.builder.name())
                .unwrap_or_else(|| "(none)".into()),
            simulation.builder.name(),
        );
        self.simulation = Some(simulation);
        Ok(())
    }

    fn set_inputs(&mut self, inputs: &[Input]) -> CommandResult {
        let pinout = self.simulation.as_ref().ok_or(NO_SIM_ERR)?.device.labels();
        for input in inputs {
            input.set(&pinout, &mut self.test_case)?;
        }
        self.show()
    }

    fn set_clock(&mut self, clock_period: usize) -> CommandResult {
        self.test_case.set_clock_period(clock_period);
        Ok(())
    }

    fn show(&self) -> CommandResult {
        let sim = self.simulation.as_ref().ok_or(NO_SIM_ERR)?;
        let component = &sim.plan.component;

        println!("Gate '{}'", &component.name);
        println!(
            "  Gate Delay:    {}",
            sim.device
                .gate_delay()
                .map(|n| n.to_string())
                .unwrap_or_else(|| "<Unknown>".into())
        );
        println!("  Clock Period:  {}", self.test_case.get_clock_period());
        println!("  Steps Elapsed: {}", sim.steps_elapsed);

        // Inputs
        println!("  Inputs:");
        for input in component.labels.iter().filter(|n| n.is_sink()) {
            let values: Vec<_> = input
                .range()
                .map(|i| {
                    self.test_case
                        .get_input(&component.labels, sim.steps_elapsed, i)
                })
                .collect();
            println!(
                "    {:10} ({}..{}): {:10} ({})",
                input.name(),
                input.range().start,
                input.range().end,
                TestCase::bits_to_string(&values, '0'),
                sum_values(&values)
            );
        }

        // Outputs
        println!("  Outputs:");
        for output in component.labels.iter().filter(|n| !n.is_sink()) {
            let values: Vec<_> = output.range().map(|i| Some(sim.device.read(i))).collect();
            println!(
                "    {:10} ({}..{}): {:10} ({})",
                output.name(),
                output.range().start,
                output.range().end,
                TestCase::bits_to_string(&values, '0'),
                sum_values(&values)
            );
        }
        Ok(())
    }

    fn tree(&self, max_depth: Option<usize>) -> CommandResult {
        let sim = self.simulation.as_ref().ok_or(NO_SIM_ERR)?;
        tree_print_component(&sim.plan.component, 0, max_depth.unwrap_or(std::usize::MAX));
        Ok(())
    }

    fn step(&mut self, steps: usize) -> CommandResult {
        let sim = self.simulation.as_mut().ok_or(NO_SIM_ERR)?;
        for _ in 0..steps {
            self.test_case
                .apply_inputs(&mut sim.device, sim.steps_elapsed);
            sim.device.step();
            sim.steps_elapsed += 1;
        }
        self.show()
    }

    fn run(&mut self) -> CommandResult {
        let sim = self.simulation.as_mut().ok_or(NO_SIM_ERR)?;
        let gate_delay = sim.device.gate_delay().ok_or(format!(
            "Gate delay not know for {}, try using 'step'",
            sim.builder.name()
        ))?;
        self.step(gate_delay)
    }

    fn reset(&mut self) -> CommandResult {
        let sim = self.simulation.as_mut().ok_or(NO_SIM_ERR)?;
        sim.steps_elapsed = 0;
        sim.device.reset();
        Ok(())
    }

    fn clear(&mut self) -> CommandResult {
        self.test_case = TestCase::default();
        self.show()
    }
}

//
// Simulation of a single Gate Device
//

struct Simulation {
    builder: Box<dyn ChipBuilder>,
    steps_elapsed: usize,
    plan: Plan,
    device: ChipDevice<SimpleEmulator>,
}

impl<'a> TryFrom<&'a BuildArgs> for Simulation {
    type Error = String;

    fn try_from(build_args: &'a BuildArgs) -> Result<Self, Self::Error> {
        let mut builder = gate::find_builder(&build_args.name)?;
        for constructor in build_args.constructors.iter() {
            builder.set_field(&constructor.name, &constructor.value)?;
        }
        let mut plan = Plan::from_chip(&*builder, build_args.depth);
        if build_args.optimize {
            Optimizer::new(&mut plan).run();
        }
        let device = ChipDevice::<SimpleEmulator>::from(&plan);
        Ok(Self {
            builder,
            plan,
            device,
            steps_elapsed: 0,
        })
    }
}

//
// Misc helper functions
//

fn sum_values(values: &[Option<bool>]) -> usize {
    let mut scale = 1;
    let mut total = 0;
    for value in values.iter() {
        if let Some(true) = value {
            total += scale;
        }
        scale <<= 1;
    }
    total
}

fn tree_print_component(component: &Component, depth: usize, max_depth: usize) {
    print_indent(depth);
    println!("{}", component.name);

    print_indent(depth + 1);
    println!("(inputs)");
    for input in component.labels.iter() {
        print_indent(depth + 2);
        println!(
            "{} ({}..{})",
            input.name(),
            input.range().start,
            input.range().end,
        );
    }

    if depth < max_depth {
        print_indent(depth + 1);
        println!("(components)");
        for sub_component in component.sub_components.iter() {
            tree_print_component(sub_component, depth + 2, max_depth);
        }
    }
}

fn print_indent(depth: usize) {
    for _ in 1..depth {
        print!("|  ");
    }
    print!("|-- ");
}
