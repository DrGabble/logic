use structopt::StructOpt;

use circuit::device::Device;
use circuit::pinout::Pinout;
use circuit::Plan;
use circuit::{gate, ChipDevice, DenseEmulator, Optimizer, SimpleEmulator};

use super::input::BuildArgs;
use super::session;

#[derive(StructOpt)]
#[structopt(about = "Print stats about a given chip")]
pub struct Args {
    #[structopt(flatten)]
    build_args: BuildArgs,

    #[structopt(short, long, default_value = "10")]
    steps: usize,
}

pub fn print(args: &Args) -> session::CommandResult {
    let mut builder = gate::find_builder(&args.build_args.name)?;
    for constructor in args.build_args.constructors.iter() {
        builder.set_field(&constructor.name, &constructor.value)?;
    }
    let mut plan = Plan::from_chip(&*builder, 0);
    let transistors = plan.transistor_count();
    let junctions = plan.junction_count();
    let wires = plan.wire_count();
    let unoptimised = ChipDevice::<SimpleEmulator>::from(&plan);
    Optimizer::new(&mut plan).run();
    let mut optimised = ChipDevice::<DenseEmulator>::from(&plan);
    let mut optimised_simple = ChipDevice::<SimpleEmulator>::from(&plan);

    // stats
    println!("Chip '{}' ('{}')", args.build_args.name, optimised.name());
    println!(
        "  Gate Delay: {}ns ({}ns unoptimised)",
        optimised
            .gate_delay()
            .map(|n| n.to_string())
            .unwrap_or_else(|| "<Unknown>".into()),
        unoptimised
            .gate_delay()
            .map(|n| n.to_string())
            .unwrap_or_else(|| "<Unknown>".into())
    );
    println!(
        "  Transistors: {} ({} unoptimised)",
        plan.transistor_count(),
        transistors
    );
    println!(
        "  Junctions: {} ({} unoptimised)",
        plan.junction_count(),
        junctions
    );
    println!(
        "  Wire Count: {}nm ({}nm unoptimised)",
        plan.wire_count(),
        wires
    );

    // options
    println!("  Options:");
    for option in builder.fields() {
        println!("    {}", option);
    }

    let labels = optimised.labels();

    // Inputs
    println!("  Inputs:");
    for input in labels.iter().filter(|n| n.is_sink()) {
        println!(
            "    {} ({}..{})",
            input.name(),
            input.range().start,
            input.range().end
        );
    }

    // Outputs
    println!("  Outputs:");
    for output in labels.iter().filter(|n| !n.is_sink()) {
        println!(
            "    {} ({}..{})",
            output.name(),
            output.range().start,
            output.range().end
        );
    }

    // compare simulation runtime
    let simple_time = {
        let start = std::time::Instant::now();
        for _ in 0..args.steps {
            optimised_simple.step();
        }
        std::time::Instant::now() - start
    };

    let dense_time = {
        let start = std::time::Instant::now();
        for _ in 0..args.steps {
            optimised.step();
        }
        std::time::Instant::now() - start
    };

    println!("  Simulation time ({} steps):", args.steps);
    println!(
        "    Simple: {:?} Dense: {:?} (x{:?})",
        simple_time,
        dense_time,
        simple_time.as_nanos() / dense_time.as_nanos()
    );

    Ok(())
}
