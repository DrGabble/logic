use petgraph::visit::NodeIndexable;

use super::super::emulator::Emulator;
use super::super::plan::{Graph, Plan, Sink, Source};
use crate::device::Device;
use itertools::izip;

//
// Densely packed Transistor representations, and indices, aim for cache performance
// Somce there seem to be very few Junctions compared to Transistors in practice,
// Junctions are represented by Transistors (using power and ground only).
//
// TODO could pack source and sink into same u64, if distances are short?
// Or some sort of hybrid "sometimes long, sometimes short" address system?
const TRANSISTORS_PER_BLOCK: usize = 64;

pub struct DenseEmulator {
    power: Vec<u64>,
    collector: Vec<u64>,
    ground: Vec<u64>,
    emitter: Vec<u64>,
    wires: Vec<(Output, Input)>,
    chunk_ranges: Vec<ChunkRange>,
    input_map: Vec<Input>,
    output_map: Vec<Output>,
}

impl<'a> Emulator<'a> for DenseEmulator {}

impl<'a> From<&'a Plan> for DenseEmulator {
    fn from(plan: &'a Plan) -> Self {
        let index_map = Self::build_index_map(&plan.graph);

        let num_transistors = plan.graph.node_count();
        let remainder = (num_transistors % TRANSISTORS_PER_BLOCK) != 0;
        let num_blocks = (num_transistors / TRANSISTORS_PER_BLOCK) + remainder as usize;

        // translate to new layout
        let mut wires = Self::build_wires(&plan.graph, index_map.as_slice());

        let max_block_size = num_blocks / (2 * 4); // TODO arbitaryish (number of sinks * number of cores)
        let chunk_ranges = Self::create_chunk_ranges(&mut wires, max_block_size);

        // store mapping for inputs and outputs only, to save space
        let input_map = plan
            .component
            .sinks
            .iter()
            .map(|p| {
                let index = index_map[p.index().index()].unwrap();
                Input::new(index, *p.pin_type())
            })
            .collect();

        let output_map = plan
            .component
            .sources
            .iter()
            .map(|p| {
                let index = index_map[p.index().index()].unwrap();
                Output::new(index, *p.pin_type())
            })
            .collect();

        Self {
            power: vec![0; num_blocks],
            collector: vec![0; num_blocks],
            ground: vec![0; num_blocks],
            emitter: vec![0; num_blocks],
            chunk_ranges,
            wires,
            input_map,
            output_map,
        }
    }
}

impl Device for DenseEmulator {
    fn write(&mut self, input: usize, value: bool) {
        if value {
            self.input_map[input].write(&mut self.power, &mut self.collector);
        }
    }

    fn step(&mut self) {
        let values = izip!(
            self.power.iter_mut(),
            self.collector.iter_mut(),
            self.ground.iter_mut(),
            self.emitter.iter_mut()
        );

        // NB adding rayon iterators here just seems to slow it down
        values.for_each(|(power, collector, ground, emitter)| {
            *ground = *power & !*collector;
            *emitter = *power & *collector;
            *power = 0;
            *collector = 0;
        });

        self.chunks().for_each(|mut c| c.step());
    }

    fn read(&self, output: usize) -> bool {
        self.output_map[output].read(&self.ground, &self.emitter)
    }

    fn reset(&mut self) {
        self.power.iter_mut().for_each(|a| *a = 0);
        self.collector.iter_mut().for_each(|a| *a = 0);
        self.ground.iter_mut().for_each(|a| *a = 0);
        self.emitter.iter_mut().for_each(|a| *a = 0);
    }
}

impl DenseEmulator {
    fn build_index_map(graph: &Graph) -> Vec<Option<usize>> {
        // create temporary full mapping to new index layout
        // assume node indices are densely-ish packed (so Vec instead of HashMap)
        // good optimisation might make this a bad guess though...
        let mut index_map = Vec::with_capacity(graph.node_bound());

        for (transistor_index, node_index) in graph.node_indices().enumerate() {
            while index_map.len() <= node_index.index() {
                index_map.push(None);
            }
            index_map[node_index.index()] = Some(transistor_index);
        }

        index_map
    }

    fn build_wires(graph: &Graph, index_map: &[Option<usize>]) -> Vec<(Output, Input)> {
        let mut wires = Vec::with_capacity(graph.edge_count());

        for edge_index in graph.edge_indices() {
            let (source_type, sink_type) = graph.edge_weight(edge_index).unwrap();
            let (old_source_index, old_sink_index) = graph.edge_endpoints(edge_index).unwrap();
            let new_source_index = index_map[old_source_index.index()].unwrap();
            let new_sink_index = index_map[old_sink_index.index()].unwrap();
            wires.push((
                Output::new(new_source_index, *source_type),
                Input::new(new_sink_index, *sink_type),
            ));
        }

        wires
    }

    fn create_chunk_ranges(wires: &mut [(Output, Input)], max_blocks: usize) -> Vec<ChunkRange> {
        if wires.is_empty() {
            return Vec::new();
        }

        wires.sort_by_key(|(source, sink)| {
            (sink.block(), *sink.tag(), *source.tag(), source.block())
        });

        let mut tag = *wires[0].1.tag();
        let mut wires_len = 0;
        let mut block_min = 0;
        let mut block_max = 0;

        let mut chunk_ranges = Vec::new();
        for (_, input) in wires.iter_mut() {
            let is_new_block = input.block() != block_max;
            let block_len = (block_max - block_min) + 1;
            if *input.tag() != tag || (is_new_block && block_len >= max_blocks) {
                chunk_ranges.push(ChunkRange {
                    tag,
                    wires_len,
                    block_len,
                });
                tag = *input.tag();
                wires_len = 0;
                block_min = input.block();
                block_max = input.block();
            }
            wires_len += 1;
            block_max = std::cmp::max(block_max, input.block());

            // adjust wires to be relative to chunk
            input.rebase_block(block_min);
        }

        chunk_ranges.push(ChunkRange {
            tag,
            wires_len,
            block_len: block_max + 1 - block_min,
        });

        chunk_ranges
    }

    fn chunks(&mut self) -> impl Iterator<Item = Chunk> + '_ {
        let power_ranges = self
            .chunk_ranges
            .iter()
            .filter(|c| matches!(c.tag, Sink::Power))
            .map(|c| c.block_len);
        let mut power_iter = self.power.as_mut_slice().split_mut_slices(power_ranges);
        let collector_ranges = self
            .chunk_ranges
            .iter()
            .filter(|c| matches!(c.tag, Sink::Collector))
            .map(|c| c.block_len);
        let mut collector_iter = self
            .collector
            .as_mut_slice()
            .split_mut_slices(collector_ranges);
        let wires_iter = self
            .wires
            .as_slice()
            .split_slices(self.chunk_ranges.iter().map(|c| c.wires_len));
        let ground = &self.ground;
        let emitter = &self.emitter;
        self.chunk_ranges
            .iter()
            .zip(wires_iter)
            .map(move |(chunk_range, wires)| {
                let sink = match chunk_range.tag {
                    Sink::Power => power_iter.next().unwrap(),
                    Sink::Collector => collector_iter.next().unwrap(),
                };
                Chunk {
                    ground,
                    emitter,
                    sink,
                    wires,
                }
            })
    }
}

//
// Chunk
//

#[derive(Debug)]
struct ChunkRange {
    tag: Sink,
    wires_len: usize,
    block_len: usize,
}

#[derive(Debug)]
struct Chunk<'a> {
    ground: &'a [u64],
    emitter: &'a [u64],
    sink: &'a mut [u64],
    wires: &'a [(Output, Input)],
}

impl<'a> Chunk<'a> {
    fn step(&mut self) {
        for (output, input) in self.wires.iter() {
            if output.read(self.ground, self.emitter) {
                input.no_tag_write(self.sink);
            }
        }
    }
}

//
// Transput
//

type Input = Transput<Sink>;
type Output = Transput<Source>;

#[derive(Debug)]
struct Transput<T> {
    tag: T,
    block: usize,
    bit_mask: u64,
}

impl<T> Transput<T> {
    fn new(transistor: usize, tag: T) -> Self {
        Self {
            tag,
            block: transistor / TRANSISTORS_PER_BLOCK,
            bit_mask: 1 << (transistor % TRANSISTORS_PER_BLOCK),
        }
    }

    fn rebase_block(&mut self, block: usize) {
        assert!(block <= self.block);
        self.block -= block;
    }

    fn tag(&self) -> &T {
        &self.tag
    }

    fn block(&self) -> usize {
        self.block
    }

    fn bit_mask(&self) -> u64 {
        self.bit_mask
    }
}

impl Transput<Source> {
    fn read(&self, ground: &[u64], emitter: &[u64]) -> bool {
        let block = match self.tag() {
            Source::Ground => &ground[self.block()],
            Source::Emitter => &emitter[self.block()],
        };
        (*block & self.bit_mask()) != 0
    }
}

impl Transput<Sink> {
    fn write(&self, power: &mut [u64], collector: &mut [u64]) {
        match self.tag() {
            Sink::Power => self.no_tag_write(power),
            Sink::Collector => self.no_tag_write(collector),
        }
    }

    fn no_tag_write(&self, sink: &mut [u64]) {
        sink[self.block()] |= self.bit_mask()
    }
}

//
// Slice Iterator
//
// Iterates over sub-slices of a vector
//

impl<T> SplitSlice<T> for [T] {
    fn split_slices<I: Iterator<Item = usize>>(&self, len_iter: I) -> IterSplitSlice<'_, T, I> {
        IterSplitSlice {
            slice: self,
            len_iter,
        }
    }
}

trait SplitSlice<T> {
    fn split_slices<I: Iterator<Item = usize>>(&self, len_iter: I) -> IterSplitSlice<'_, T, I>;
}

struct IterSplitSlice<'a, T: 'a, I> {
    slice: &'a [T],
    len_iter: I,
}

impl<'a, T, I: Iterator<Item = usize>> Iterator for IterSplitSlice<'a, T, I> {
    type Item = &'a [T];

    fn next(&mut self) -> Option<Self::Item> {
        let len = self.len_iter.next()?;
        if self.slice.is_empty() {
            return None;
        }
        let slice = std::mem::take(&mut self.slice);

        let (next, remainder) = slice.split_at(len);
        self.slice = remainder;
        Some(next)
    }
}

//
// Mutable Slice Iterator
//
// Iterators over mutable sub-slices of a vector.
//

impl<T> MutSlice<T> for [T] {
    fn split_mut_slices<I: Iterator<Item = usize>>(
        &mut self,
        len_iter: I,
    ) -> IterMutSlice<'_, T, I> {
        IterMutSlice {
            slice: self,
            len_iter,
        }
    }
}

trait MutSlice<T> {
    fn split_mut_slices<I: Iterator<Item = usize>>(
        &mut self,
        len_iter: I,
    ) -> IterMutSlice<'_, T, I>;
}

struct IterMutSlice<'a, T: 'a, I> {
    slice: &'a mut [T],
    len_iter: I,
}

impl<'a, T, I: Iterator<Item = usize>> Iterator for IterMutSlice<'a, T, I> {
    type Item = &'a mut [T];

    fn next(&mut self) -> Option<Self::Item> {
        let len = self.len_iter.next()?;
        if self.slice.is_empty() {
            return None;
        }
        let slice = std::mem::take(&mut self.slice);

        let (next, remainder) = slice.split_at_mut(len);
        self.slice = remainder;
        Some(next)
    }
}
