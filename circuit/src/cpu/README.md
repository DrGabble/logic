# Jub Processing Unit

16 bit CPU.

## TODO

- Need to reconsider IO

## Registers

| Address | Name            | Read / Write | Description |
|---------|-----------------|--------------|---|
|       0 | Program Pointer | rw           | The memory address of the next instruction |
|       1 | Constant        | r            | Constant from the instruction (or zero) |
|   2..16 | General Purpose | rw           | General purpose data registers |

## Decoder Outputs

L is the register address for the control check (if / if not).

A, B and Output are register addresses of the Alu inputs and output respectively.

| Format | Name        | IO / Alu |Mode      | Control | L | A | B | Output | Constant |
|--------|-------------|----------|----------|---------|---|---|---|--------|----------|
| A      | set         | alu      | set      | -       | - | C | - | G      | yes      | 
|        | shiftset    | alu      | shiftset | -       | - | C | - | G      |          | 
|        | ifnjump     | alu      | add      | if not  | G | P | C | P      |          |
|        | ifnjumpback | alu      | sub      | if not  | G | P | C | P      |          |
|        | ifjump      | alu      | add      | if      | G | P | C | P      |          |
|        | ifjumpback  | alu      | sub      | if      | G | P | C | P      |          |
| B      | (alu)       | alu      | (all)    | -       | - | G | G | G      | no       |
| B      | (io )       | io       | (all)    | -       | - | G | G | G      | no       |

## Alu Modes

| Address | Name
|---------|---
|       0 | add 
|       1 | sub
|       2 | set        | Set lower 8 bits of A to that of B
|       3 | shiftset   | Leftshift A 8 bits, and then do set (above)
|       4 | copy
|       5 | multiply
|       6 | leftshift
|       7 | rightshift
|       8 | and
|       9 | or
|      10 | xor
|      11 | not        | Bitwise not of input A, input B is ignored 
|      12 | read       | Read external memory from address A to output
|      13 | write      | Write to address A the value a B

## Instructions

There are two primary formats for the instruction, for different combinations of 4 bit register address and 8 bits constant.

| Format    | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|
|-----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| A         | 0| Opcode             | Address   | Address   |
| B         | 1| Opcode | Constant              | Address   |

### Type B (Constant and Address)

| Operation | 0| 1| 2| 3| 4| 5| 6| 7| 8| 9|10|11|12|13|14|15|
|-----------|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| Set       | 1| 0| 0| 0| Constant              | Address   |
| Shiftset  | 1| 0| 0| 1| Constant              | Address   |
| IfJump    | 1| 1| J| B| Constant              | Address   |

For both Set and ShiftSet, X is unused.

If J is zero, then a jump will occur if the value at Address is not zero. If J is one, then it will occur if the value is zero.

If B is zero, then the Constant will be added to the Program pointer. If B is one, then the Constant will be subtracted.
