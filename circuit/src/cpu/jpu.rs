use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::gate::{Basic, Demultiplexer, Multiplexer, Not, Word, OR};
use super::super::plan::{Builder, Pin, Sink, Source};
use crate::pinout::NamedRange;

use super::alu::{Alu, WORD_BITS};
use super::opdecode::{OpDecode, CONSTANT_ADDRESS, CONTROL_BITS, PROG_ADDRESS};

const REG_ADDRESS_BITS: usize = 4;

const NUM_REGISTERS: usize = 16;

//
// Jub Computation Unit (Jpu)
//

pub struct JpuChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(
    JpuChip;
    sink power |_: &JpuChip| 0,
    sinks data_in |c: &JpuChip| 1..c.sources.len(),
    source write |_: &JpuChip| 0,
    sources address |_: &JpuChip| 1..(WORD_BITS+1),
    sources data_out |c: &JpuChip| (WORD_BITS+1)..c.sources.len()
);

derive_builder!(Jpu; bits: usize = 8);

impl BuildChip for Jpu {
    type Output = JpuChip;

    fn name(&self) -> String {
        "Jub Computation Unit".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power = builder.new_junction();

        // NB one less register as constant register is decoded from instruction
        let registers = builder.new_chip_vec(&Word::new(WORD_BITS), NUM_REGISTERS - 1);
        for reg in registers.iter() {
            builder.join(power.output(), reg.power());
        }

        let decoder = builder.new_chip(&OpDecode::new());
        builder.join(power.output(), decoder.power());

        // TODO reading next opcode and storing in temporary word

        let condition = builder.new_chip(&Condition::new());
        builder.join_all(decoder.control(), condition.control());

        {
            let condition_mux = Self::multiplexer(
                builder,
                power.output(),
                decoder.constant(),
                &registers,
                decoder.control_address(),
            );
            builder.join_all(condition_mux.outputs(), condition.inputs());
        }

        let alu = builder.new_chip(&Alu::new());
        builder.join(power.output(), alu.power());
        builder.join_all(decoder.mode(), alu.mode());
        // TODO overflow

        {
            let alpha_mux = Self::multiplexer(
                builder,
                power.output(),
                decoder.constant(),
                &registers,
                decoder.alpha_address(),
            );
            builder.join_all(alpha_mux.outputs(), alu.alpha_in());
        }

        {
            let beta_mux = Self::multiplexer(
                builder,
                power.output(),
                decoder.constant(),
                &registers,
                decoder.beta_address(),
            );
            builder.join_all(beta_mux.outputs(), alu.beta_in());
        }

        {
            // TODO need to clock so all non-output registers don't get zerod!
            let output_demux = builder.new_chip(&Demultiplexer::new(REG_ADDRESS_BITS, WORD_BITS));
            builder.join(power.output(), output_demux.power());
            builder.join_all(decoder.output_address(), output_demux.address());
            builder.join_all(alu.output(), output_demux.inputs());
            // NB constant not wired here (because you can't write to it)
            for (index, reg) in registers.iter().enumerate() {
                builder.join_all(output_demux.outputs(index + 1), reg.inputs());
            }
        }

        JpuChip {
            sinks: vec![power.input()],
            sources: vec![],
        }
    }
}

impl Jpu {
    fn multiplexer(
        builder: &mut Builder,
        power: Pin<Source>,
        constant: &[Pin<Source>],
        registers: &[<Word as BuildChip>::Output],
        address: &[Pin<Source>],
    ) -> <Multiplexer as BuildChip>::Output {
        let multiplexer = builder.new_chip(&Multiplexer::new(REG_ADDRESS_BITS, WORD_BITS));
        builder.join(power, multiplexer.power());
        builder.join_all(address, multiplexer.address());
        builder.join_all(constant, multiplexer.inputs(CONSTANT_ADDRESS));
        for (index, reg) in registers.iter().enumerate() {
            builder.join_all(reg.outputs(), multiplexer.inputs(index + 1));
        }
        multiplexer
    }
}

//
// Conditional Chip
//
// If enable is low, output is always high
// If enable is high
//   and if invert is low, output is high if input != 0
//   and if invert is high, output is high if input == 0

pub struct ConditionChip {
    sinks: Vec<Pin<Sink>>,
    sources: [Pin<Source>; 1],
}

derive_chip!(
    ConditionChip;
    sinks control   |_: &ConditionChip| 0..CONTROL_BITS,
    sinks inputs   |c: &ConditionChip| CONTROL_BITS..c.sinks.len(),
    source output |_: &ConditionChip| 0
);

derive_builder!(Condition);

impl BuildChip for Condition {
    type Output = ConditionChip;

    fn name(&self) -> String {
        "Condition Unit".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // use the "enable" input as power, as we output 0 when its disabled anyway
        let enable = builder.new_junction();

        let is_true = builder.new_chip(&OR::new(WORD_BITS));

        let is_false = builder.new_chip(&Not::new());
        builder.join(enable.output(), is_false.power());
        builder.join(is_true.output(), is_false.input());

        let demux = builder.new_chip(&Multiplexer::new(2, 1));
        // zeroth input not wired to anything, for "disable" zero
        builder.join(is_false.output(), demux.inputs(1)[0]);
        // second input not wired to anything, for "disable" zero
        builder.join(is_true.output(), demux.inputs(3)[0]);

        builder.join(enable.output(), demux.address()[0]);
        builder.join(enable.output(), demux.power());
        let mut sinks = vec![enable.input(), demux.address()[1]];
        sinks.extend(is_true.inputs());

        ConditionChip {
            sinks,
            sources: [demux.outputs()[0]; 1],
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_condition_chip() {
        test_chip!(
            Condition::new();
            ("control" 0: false), ("control" 1: false), ("inputs": 0) => ("output": 0);
            ("control" 0: false), ("control" 1: true), ("inputs": 0) => ("output": 0);
            ("control" 0: true), ("control" 1: false), ("inputs": 0) => ("output": 1);
            ("control" 0: true), ("control" 1: true), ("inputs": 0) => ("output": 0)
        );
        for input in 1..15 {
            test_chip!(
                Condition::new();
                ("control" 0: false), ("control" 1: false), ("inputs": input) => ("output": 0);
                ("control" 0: false), ("control" 1: true), ("inputs": input) => ("output": 0);
                ("control" 0: true), ("control" 1: false), ("inputs": input) => ("output": 0);
                ("control" 0: true), ("control" 1: true), ("inputs": input) => ("output": 1)
            );
        }
    }
}
