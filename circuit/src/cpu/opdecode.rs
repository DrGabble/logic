use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::gate::{And, Basic, HalfMultiplexer, HardCoded, Not, Switch};
use super::super::plan::{Builder, Pin, Sink, Source};
use crate::pinout::NamedRange;

//
// Opcode Decoder
//
// Mode is the command signal to send to the Alu
// Control is wether to use an If statement, If Not, or And True
//   logic control before Alu
// Control Address is the address of the register to test for
//   logic (if statement) control
// Constant is the value of virtual constant register if in use
// Alpha, Beta and Output Address are the demux address of which
//   register to use for the two inputs and output of the Alu
//

pub const CONSTANT_ADDRESS: usize = 0;
pub const PROG_ADDRESS: usize = 1;

const ALU_ADDRESS_BITS: usize = 4;
const OPCODE_BITS: usize = 16;
pub const CONTROL_BITS: usize = 2;
const CONSTANT_BITS: usize = 8;
const OP_ADDRESS_BITS: usize = 4;

pub struct OpDecodeChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

const MODE_END: usize = ALU_ADDRESS_BITS;
const CONTROL_END: usize = MODE_END + CONTROL_BITS;
const CONT_ADDR_END: usize = CONTROL_END + OP_ADDRESS_BITS;
const CONSTANT_END: usize = CONT_ADDR_END + CONSTANT_BITS;
const ALPHA_END: usize = CONSTANT_END + OP_ADDRESS_BITS;
const BETA_END: usize = ALPHA_END + OP_ADDRESS_BITS;

derive_chip!(
    OpDecodeChip;
    sink power |_: &Self| 0,
    sinks opcode |_: &Self| 1..(1+OPCODE_BITS),
    sources mode |_: &Self| 0..MODE_END,
    sources control |_: &Self| MODE_END..CONTROL_END,
    sources control_address |_: &Self| CONTROL_END..CONT_ADDR_END,
    sources constant |_: &Self| CONT_ADDR_END..CONSTANT_END,
    sources alpha_address |_: &Self| CONSTANT_END..ALPHA_END,
    sources beta_address |_: &Self| ALPHA_END..BETA_END,
    sources output_address |c: &Self| BETA_END..c.sources.len()
);

derive_builder!(OpDecode);

impl BuildChip for OpDecode {
    type Output = OpDecodeChip;

    fn name(&self) -> String {
        "Opcode Decoder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // sinks
        let mut sinks = Vec::with_capacity(1 + OPCODE_BITS);

        let power_junction = builder.new_junction();
        sinks.push(power_junction.input());

        let mut inputs = Vec::with_capacity(OPCODE_BITS);
        for _ in 0..OPCODE_BITS {
            let input_junction = builder.new_junction();
            sinks.push(input_junction.input());
            inputs.push(input_junction.output());
        }

        let constant_value = &inputs[4..12];
        let alpha_address = &inputs[12..16];
        let beta_address = &inputs[8..12];

        // sources
        let mut sources = Vec::with_capacity(BETA_END + OP_ADDRESS_BITS);

        // find which mode we're in
        let is_const_and_addr = inputs[0];

        let is_two_addr = builder.new_chip(&Not::new());
        builder.join(power_junction.output(), is_two_addr.power());
        builder.join(inputs[0], is_two_addr.input());

        // more specific sub-modes
        let is_control = {
            // control enabled / disabled (sub of is_const_and_addr)
            let and = builder.new_chip(&And::new(2));
            builder.join(is_const_and_addr, and.inputs()[0]);
            builder.join(inputs[1], and.inputs()[1]);
            and.output()
        };

        let is_constant_shift = {
            // control enabled / disabled (sub of is_const_and_addr)
            let not = builder.new_chip(&Not::new());
            builder.join(power_junction.output(), not.power());
            builder.join(inputs[1], not.input());

            let and = builder.new_chip(&And::new(2));
            builder.join(is_const_and_addr, and.inputs()[0]);
            builder.join(not.output(), and.inputs()[1]);
            and.output()
        };

        // hard coded values
        let constant_address = builder.new_chip(&HardCoded::new(OP_ADDRESS_BITS, CONSTANT_ADDRESS));
        builder.join(power_junction.output(), constant_address.power());

        let program_address = builder.new_chip(&HardCoded::new(OP_ADDRESS_BITS, PROG_ADDRESS));
        builder.join(power_junction.output(), program_address.power());

        // Alu mode
        // TODO need to set from other opcode modes
        {
            let mux = builder.new_chip(&HalfMultiplexer::new(3, ALU_ADDRESS_BITS));

            // alu modes
            static_assertions::const_assert!(ALU_ADDRESS_BITS < 6);
            builder.join(is_two_addr.output(), mux.select()[0]);
            builder.join_all(&inputs[1..(ALU_ADDRESS_BITS + 1)], mux.inputs(0));

            // alu mode 0 or 1 (add or sub)
            builder.join(is_control, mux.select()[1]);
            builder.join(inputs[3], mux.inputs(1)[0]);

            // alu mode 2 or 3 (set or shiftset)
            builder.join(is_constant_shift, mux.select()[2]);
            builder.join(inputs[3], mux.inputs(2)[0]);
            builder.join(power_junction.output(), mux.inputs(2)[1]);

            sources.extend(mux.outputs().iter().cloned());
            assert_eq!(sources.len(), MODE_END);
        }

        // control logic mode
        {
            let is_jump = builder.new_chip(&And::new(2));
            builder.join(is_control, is_jump.inputs()[0]);
            builder.join(inputs[2], is_jump.inputs()[1]);

            sources.push(is_control);
            sources.push(is_jump.output());
            assert_eq!(sources.len(), CONTROL_END);
        }

        // control address
        {
            let address_switch = builder.new_chip(&Switch::new(OP_ADDRESS_BITS));
            builder.join(is_control, address_switch.control());
            builder.join_all(alpha_address, address_switch.inputs());
            sources.extend(address_switch.outputs());
            assert_eq!(sources.len(), CONT_ADDR_END);
        }

        // constant
        {
            let switch = builder.new_chip(&Switch::new(CONSTANT_BITS));
            builder.join(is_const_and_addr, switch.control());
            builder.join_all(constant_value, switch.inputs());
            sources.extend(switch.outputs().iter().cloned());
            assert_eq!(sources.len(), CONSTANT_END);
        }

        // alu input A address
        {
            let mux = builder.new_chip(&HalfMultiplexer::new(3, OP_ADDRESS_BITS));

            builder.join(is_constant_shift, mux.select()[0]);
            builder.join_all(constant_address.outputs(), mux.inputs(0));

            builder.join(is_control, mux.select()[1]);
            builder.join_all(program_address.outputs(), mux.inputs(1));

            builder.join(is_two_addr.output(), mux.select()[2]);
            builder.join_all(alpha_address, mux.inputs(2));

            sources.extend(mux.outputs());
            assert_eq!(sources.len(), ALPHA_END);
        }

        // alu input B address
        {
            let mux = builder.new_chip(&HalfMultiplexer::new(2, OP_ADDRESS_BITS));

            builder.join(is_control, mux.select()[0]);
            builder.join_all(constant_address.outputs(), mux.inputs(0));

            builder.join(is_two_addr.output(), mux.select()[1]);
            builder.join_all(beta_address, mux.inputs(1));

            sources.extend(mux.outputs());
            assert_eq!(sources.len(), BETA_END);
        }

        // alu output address
        {
            let mux = builder.new_chip(&HalfMultiplexer::new(2, OP_ADDRESS_BITS));

            builder.join(is_constant_shift, mux.select()[0]);
            builder.join(is_two_addr.output(), mux.select()[0]);
            builder.join_all(alpha_address, mux.inputs(0));

            builder.join(is_control, mux.select()[1]);
            builder.join_all(program_address.outputs(), mux.inputs(1));

            sources.extend(mux.outputs());
        }

        Self::Output { sinks, sources }
    }
}

#[cfg(test)]
#[allow(clippy::unusual_byte_groupings)]
mod test {
    use super::*;

    #[test]
    fn test_decode_nop() {
        test_chip!(OpDecode::new();
            ("opcode": 0b0000_0000_000000_0_0) =>
                ("mode": 0),
                ("control": 0),
                ("control_address": 0),
                ("constant": 0),
                ("alpha_address": 0),
                ("beta_address": 0),
                ("output_address": 0)
        );
    }

    #[test]
    fn test_decode_generic_alu() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_0110_0001001_0) =>
                ("mode": 0b1001),
                ("control": 0),
                ("control_address": 0),
                ("constant": 0),
                ("alpha_address": 0b1111),
                ("beta_address": 0b0110),
                ("output_address": 0b1111)
        );
    }

    #[test]
    fn test_decode_constant_set() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1010_01101001_000_1) =>
                ("mode": 0b0010),
                ("control": 0),
                ("control_address": 0),
                ("constant": 0b01101001),
                ("alpha_address": CONSTANT_ADDRESS),
                ("output_address": 0b1010)
        );
    }

    #[test]
    fn test_decode_constant_shift_and_set() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_01101001_100_1) =>
                ("mode": 0b0011),
                ("control": 0),
                ("control_address": 0),
                ("constant": 0b01101001),
                ("alpha_address": CONSTANT_ADDRESS),
                ("output_address": 0b1111)
        );
    }

    #[test]
    fn test_decode_if_jump_forward() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_01101001_001_1) =>
                ("mode": 0b0000),
                ("control": 0b01),
                ("control_address": 0b1111),
                ("constant": 0b01101001),
                ("alpha_address": PROG_ADDRESS),
                ("beta_address": CONSTANT_ADDRESS),
                ("output_address": PROG_ADDRESS)
        );
    }

    #[test]
    fn test_decode_if_jump_back() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_01101001_101_1) =>
                ("mode": 0b0001),
                ("control": 0b01),
                ("control_address": 0b1111),
                ("constant": 0b01101001),
                ("alpha_address": PROG_ADDRESS),
                ("beta_address": CONSTANT_ADDRESS),
                ("output_address": PROG_ADDRESS)
        );
    }

    #[test]
    fn test_decode_if_not_jump_forward() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_01101001_011_1) =>
                ("mode": 0b0000),
                ("control": 0b11),
                ("control_address": 0b1111),
                ("constant": 0b01101001),
                ("alpha_address": PROG_ADDRESS),
                ("beta_address": CONSTANT_ADDRESS),
                ("output_address": PROG_ADDRESS)
        );
    }

    #[test]
    fn test_decode_if_not_jump_back() {
        test_chip!(OpDecode::new();
            ("opcode": 0b1111_01101001_111_1) =>
                ("mode": 0b0001),
                ("control": 0b11),
                ("control_address": 0b1111),
                ("constant": 0b01101001),
                ("alpha_address": PROG_ADDRESS),
                ("beta_address": CONSTANT_ADDRESS),
                ("output_address": PROG_ADDRESS)
        )
    }
}
