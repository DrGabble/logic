use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::gate::{
    And, BarrelShift, Basic, Bitwise, DadaMultiplier, Decoder, HalfDemultiplexer,
    HalfDemultiplexerChip, Not, ORChip, RippleAdder, ShiftType, Xor, OR,
};
use super::super::plan::{Builder, Junction, Pin, Sink, Source};
use crate::pinout::NamedRange;

pub const WORD_BITS: usize = 16;

//
// Arithmetic Logic Unit (Alu)
//

const ALU_ADDRESS_BITS: usize = 4;

pub struct AluChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

const MODE_END: usize = 1 + ALU_ADDRESS_BITS;
const ALPHA_IN_END: usize = MODE_END + WORD_BITS;
const BETA_IN_END: usize = ALPHA_IN_END + WORD_BITS;
const READ_IN_END: usize = BETA_IN_END + WORD_BITS;

const ADDRESS_END: usize = 2 + WORD_BITS;
const WRITE_OUT_END: usize = ADDRESS_END + WORD_BITS;
const OUTPUT_END: usize = WRITE_OUT_END + WORD_BITS;

derive_chip!(
    AluChip;
    sink power |_: &Self| 0,
    sinks mode |_: &Self| 1..MODE_END,
    sinks alpha_in |_: &Self| MODE_END..ALPHA_IN_END,
    sinks beta_in |_: &Self| ALPHA_IN_END..BETA_IN_END,
    sinks read_in |_: &Self| BETA_IN_END..READ_IN_END,
    source overflow |_: &Self| 0,
    source write |_: &Self| 1,
    sources address |_: &Self| 2..ADDRESS_END,
    sources write_out |_: &Self| ADDRESS_END..WRITE_OUT_END,
    sources output |_: &Self| WRITE_OUT_END..OUTPUT_END
);

derive_builder!(Alu);

impl BuildChip for Alu {
    type Output = AluChip;

    fn name(&self) -> String {
        "Arithmetic Logic Unit".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // constants
        const NUM_MODES: usize = 14;
        static_assertions::const_assert!(NUM_MODES <= (1 << ALU_ADDRESS_BITS));

        // inputs
        let power_junction = builder.new_junction();

        let decoder = builder.new_chip(&Decoder::new(ALU_ADDRESS_BITS));
        builder.join(power_junction.output(), decoder.power());

        let alpha_demux = builder.new_chip(&HalfDemultiplexer::new(NUM_MODES, WORD_BITS));
        builder.join_all(decoder.outputs(), alpha_demux.select());

        let beta_demux = builder.new_chip(&HalfDemultiplexer::new(NUM_MODES, WORD_BITS));
        builder.join_all(decoder.outputs(), beta_demux.select());

        // outputs
        let output_ors = builder.new_chip_vec(&OR::new(NUM_MODES), WORD_BITS);
        let overflow_junction = builder.new_junction();

        // create sub components
        let alu_builder = AluBuilder {
            power_junction,
            alpha_demux,
            beta_demux,
            output_ors,
            overflow_junction,
        };

        alu_builder.build_add(builder, 0);
        // TODO alu_builder.build_subtract(1);
        alu_builder.build_set(builder, 2);
        alu_builder.build_shift_set(builder, 3);
        alu_builder.build_copy(builder, 4);
        alu_builder.build_multiply(builder, 5);
        alu_builder.build_leftshift(builder, 6);
        // TODO alu_builder.build_rightshift(7);
        alu_builder.build_bitwise_and(builder, 8);
        alu_builder.build_bitwise_or(builder, 9);
        alu_builder.build_bitwise_xor(builder, 10);
        alu_builder.build_bitwise_not(builder, 11);
        alu_builder.build_read(builder, 12);
        // TODO write

        // create actual chip
        let sinks = std::iter::once(alu_builder.power_junction.input())
            .chain(decoder.inputs().iter().cloned())
            .chain(alu_builder.alpha_demux.inputs().iter().cloned())
            .chain(alu_builder.beta_demux.inputs().iter().cloned())
            .collect();

        let sources = std::iter::once(alu_builder.overflow_junction.output())
            .chain(alu_builder.output_ors.iter().map(|o| o.output()))
            .collect();

        Self::Output { sinks, sources }
    }
}

struct AluBuilder {
    power_junction: Junction,
    alpha_demux: HalfDemultiplexerChip,
    beta_demux: HalfDemultiplexerChip,
    output_ors: Vec<ORChip>,
    overflow_junction: Junction,
}

impl AluBuilder {
    fn get_output(&self, address: usize) -> impl Iterator<Item = &Pin<Sink>> {
        self.output_ors.iter().map(move |o| &o.inputs()[address])
    }

    fn build_copy(&self, builder: &mut Builder, address: usize) {
        builder.join_all(self.alpha_demux.outputs(address), self.get_output(address));
    }

    fn build_set(&self, builder: &mut Builder, address: usize) {
        let midpoint = WORD_BITS / 2;
        let lower_beta = self.beta_demux.outputs(address)[..midpoint].iter();
        let upper_alpha = self.alpha_demux.outputs(address)[midpoint..].iter();
        builder.join_all(lower_beta.chain(upper_alpha), self.get_output(address));
    }

    fn build_shift_set(&self, builder: &mut Builder, address: usize) {
        let midpoint = WORD_BITS / 2;
        let lower_beta = self.beta_demux.outputs(address)[..midpoint].iter();
        let lower_alpha = self.alpha_demux.outputs(address)[..midpoint].iter();
        builder.join_all(lower_beta.chain(lower_alpha), self.get_output(address));
    }

    fn build_add(&self, builder: &mut Builder, address: usize) {
        let adder = builder.new_chip(&RippleAdder::new(WORD_BITS));
        builder.join_all(self.alpha_demux.outputs(address), adder.alpha_in());
        builder.join_all(self.beta_demux.outputs(address), adder.beta_in());
        builder.join_all(adder.output(), self.get_output(address));
        builder.join(adder.carry(), self.overflow_junction.input());
    }

    fn build_multiply(&self, builder: &mut Builder, address: usize) {
        let multiplier = builder.new_chip(&DadaMultiplier::uniform(WORD_BITS));
        builder.join_all(self.alpha_demux.outputs(address), multiplier.alpha_in());
        builder.join_all(self.beta_demux.outputs(address), multiplier.beta_in());
        builder.join_all(multiplier.output(), self.get_output(address));
        builder.join(multiplier.overflow(), self.overflow_junction.input());
    }

    fn build_leftshift(&self, builder: &mut Builder, address: usize) {
        // TODO shifts too large do NOT behave correctly
        let leftshift = builder.new_chip(&BarrelShift::new(WORD_BITS, ShiftType::Logical));
        builder.join(self.power_junction.output(), leftshift.power());
        builder.join_all(self.alpha_demux.outputs(address), leftshift.inputs());
        builder.join_all(self.beta_demux.outputs(address), leftshift.shift());
        builder.join_all(leftshift.outputs(), self.get_output(address));
    }

    fn build_bitwise_and(&self, builder: &mut Builder, address: usize) {
        let and = builder.new_chip(&Bitwise::new(And::new(2), WORD_BITS));
        builder.join_all(self.alpha_demux.outputs(address), and.alpha_in());
        builder.join_all(self.beta_demux.outputs(address), and.beta_in());
        builder.join_all(and.outputs(), self.get_output(address));
    }

    fn build_bitwise_or(&self, builder: &mut Builder, address: usize) {
        let or = builder.new_chip(&Bitwise::new(OR::new(2), WORD_BITS));
        builder.join_all(self.alpha_demux.outputs(address), or.alpha_in());
        builder.join_all(self.beta_demux.outputs(address), or.beta_in());
        builder.join_all(or.outputs(), self.get_output(7));
    }

    fn build_bitwise_xor(&self, builder: &mut Builder, address: usize) {
        let xor = builder.new_chip(&Bitwise::new(Xor::new(), WORD_BITS));
        builder.join_all(self.alpha_demux.outputs(address), xor.alpha_in());
        builder.join_all(self.beta_demux.outputs(address), xor.beta_in());
        builder.join_all(xor.outputs(), self.get_output(address));
    }

    fn build_bitwise_not(&self, builder: &mut Builder, address: usize) {
        let not_array = builder.new_chip_vec(&Not::new(), WORD_BITS);
        builder.join_all(
            self.alpha_demux.outputs(address),
            not_array.iter().map(|n| n.input()),
        );
        builder.join_all(
            not_array.iter().map(|n| n.output()),
            self.get_output(address),
        );
    }

    fn build_read(&self, builder: &mut Builder, address: usize) {
        unimplemented!();
    }
}
