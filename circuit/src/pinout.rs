pub trait Pinout {
    fn name(&self) -> String;
    fn labels(&self) -> Vec<NamedRange>;
    fn sinks(&self) -> Vec<usize> {
        self.labels()
            .iter()
            .filter(|n| n.is_sink())
            .flat_map(|n| n.range.clone())
            .collect()
    }
    fn sources(&self) -> Vec<usize> {
        self.labels()
            .iter()
            .filter(|n| !n.is_sink())
            .flat_map(|n| n.range.clone())
            .collect()
    }
}

//
// Find Index
//

pub trait FindNamedRange {
    fn find(&self, name: &str) -> Option<&NamedRange>;
}

impl FindNamedRange for [NamedRange] {
    fn find(&self, name: &str) -> Option<&NamedRange> {
        self.iter().find(|r| r.name == name)
    }
}

//
// Named Range
//

#[derive(Clone)]
pub struct NamedRange {
    name: String,
    is_sink: bool,
    range: std::ops::Range<usize>,
}

impl NamedRange {
    pub const fn sink(name: String, range: std::ops::Range<usize>) -> Self {
        Self {
            name,
            range,
            is_sink: true,
        }
    }

    pub const fn source(name: String, range: std::ops::Range<usize>) -> Self {
        Self {
            name,
            range,
            is_sink: false,
        }
    }

    pub fn get_nth(&self, offset: usize) -> Option<usize> {
        let index = self.range.start + offset;
        if index == self.range.start || index < self.range.end {
            Some(index)
        } else {
            None
        }
    }

    pub fn nth(&self, offset: usize) -> usize {
        self.get_nth(offset).unwrap()
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn range(&self) -> std::ops::Range<usize> {
        self.range.clone()
    }

    pub fn is_sink(&self) -> bool {
        self.is_sink
    }
}
