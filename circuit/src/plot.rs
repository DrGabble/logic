use std::collections::{HashMap, HashSet};

use dot_writer::Attributes;
use petgraph::graph::NodeIndex;

use super::plan::{self, Node, Sink, Source};
use super::testing::TestSuite;
use crate::device::NamedDevice;
use crate::pinout::NamedRange;

//
// Plottable
//
// Allows plotting an graph by requesting information on it's layout.
//

pub trait Plottable: NamedDevice {
    fn get_component(&self) -> &plan::Component;
    fn get_state(&self) -> &dyn State;
}

pub trait State {
    fn node_type(&self, node_index: &NodeIndex) -> Option<Node>;
    fn sink_value(&self, node_index: &NodeIndex, sink: &Sink) -> Option<bool>;
    fn source_value(&self, node_index: &NodeIndex, source: &Source) -> Option<bool>;
    fn visit_edges(
        &self,
        source_index: &NodeIndex,
        visitor: &mut dyn FnMut(&Source, &Sink, &NodeIndex),
    );
}

//
// Settings
//
// All the adjustable knobs and whistles.
//

pub struct Settings {
    plot_primitives: bool,
    split_transputs: SplitTransputMode,
}

impl Default for Settings {
    fn default() -> Self {
        Settings {
            plot_primitives: true,
            split_transputs: SplitTransputMode::Automatic,
        }
    }
}

impl Settings {
    pub fn set_plot_primitives(&mut self, value: bool) {
        self.plot_primitives = value;
    }

    pub fn set_split_transputs(&mut self, value: SplitTransputMode) {
        self.split_transputs = value;
    }
}

pub enum SplitTransputMode {
    Never,
    Automatic,
    Always,
}

impl SplitTransputMode {
    fn should_split(&self, size: usize) -> bool {
        match (self, size) {
            (_, 0..=1) => false,
            (Self::Never, _) => false,
            (Self::Automatic, 2..=6) => true,
            (Self::Automatic, _) => false,
            (Self::Always, _) => true,
        }
    }
}

impl std::str::FromStr for SplitTransputMode {
    type Err = String;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "never" => Ok(Self::Never),
            "auto" => Ok(Self::Automatic),
            "always" => Ok(Self::Always),
            _ => Err("No such split primitive mode".to_string()),
        }
    }
}

//
// Sequence
//
// A sequence of snapshots.
//

pub struct Sequence<'a> {
    file_prefix: &'a str,
    next_snapshot: usize,
    settings: Settings,
}

impl<'a> Sequence<'a> {
    pub fn new(file_prefix: &'a str, settings: Settings) -> Self {
        Self {
            file_prefix,
            next_snapshot: 0,
            settings,
        }
    }

    pub fn take<P: Plottable>(&mut self, plottable: &mut P, suite: &TestSuite) {
        for (case_number, case) in suite.cases().iter().enumerate() {
            for step in 0..(case.steps() + 1) {
                case.apply_inputs(plottable, step);
                if step > 0 {
                    plottable.step();
                }
                case.apply_inputs(plottable, step); // reset because step may clear primary inputs
                let title = format!(
                    "Case: {} / {}, Step: {} / {}",
                    case_number + 1,
                    suite.cases().len(),
                    step,
                    case.steps()
                );
                self.take_snapshot(plottable, &title);
            }
        }
    }

    pub fn take_snapshot(&mut self, plottable: &dyn Plottable, title: &str) {
        let path = format!("{}_{:03}.dot", self.file_prefix, self.next_snapshot);
        let mut snapshot = Snapshot {
            title,
            path: &path,
            settings: &self.settings,
            plottable,
        };
        snapshot.take();
        self.next_snapshot += 1;
    }
}

//
// Snapshot
//
// Writes a diagram of a Plottable device to file.
//

struct Snapshot<'a> {
    plottable: &'a dyn Plottable,
    title: &'a str,
    path: &'a str,
    settings: &'a Settings,
}

impl<'a> Snapshot<'a> {
    fn take(&mut self) {
        let mut file = std::fs::File::create(self.path).unwrap();
        let mut writer = dot_writer::DotWriter::from(&mut file);
        let mut graph = writer.digraph();
        graph.set_label(self.title);
        let font = "Sahadeva";
        graph.graph_attributes().set_font(font);
        graph.node_attributes().set_font(font);
        graph.edge_attributes().set_font(font);
        let mut component = ComponentPlot {
            cluster: graph.cluster(),
            plottable: self.plottable,
            settings: self.settings,
            component: self.plottable.get_component(),
            id: ComponentId(0),
        };
        component.plot();
    }
}

//
// Component
//

struct ComponentPlot<'a, 'b> {
    cluster: dot_writer::Scope<'a, 'b>,
    component: &'a plan::Component,
    plottable: &'a dyn Plottable,
    settings: &'a Settings,
    id: ComponentId,
}

#[derive(Clone, Eq, PartialEq)]
struct ComponentId(usize);

impl<'a, 'b> ComponentPlot<'a, 'b> {
    fn get_state(&self) -> &dyn State {
        self.plottable.get_state()
    }

    fn plot_primitives(&self) -> bool {
        self.settings.plot_primitives || !self.component.sub_components.is_empty()
    }

    fn plot(&mut self) -> TransputMap {
        self.cluster.set_label(&self.component.name);

        let mut sub_transputs = TransputMap::new();
        for (sub_id, sub_component) in self.component.sub_components.iter().enumerate() {
            let mut sub_plot = ComponentPlot {
                cluster: self.cluster.cluster(),
                component: sub_component,
                plottable: self.plottable,
                settings: self.settings,
                id: ComponentId(sub_id),
            };
            sub_transputs.extend(sub_plot.plot());
        }

        // must come after sub components are plotted
        if self.plot_primitives() {
            self.plot_nodes();
        }

        let transputs = self.plot_all_transputs(&sub_transputs);
        self.set_ranks(&transputs);

        if self.plot_primitives() {
            self.plot_all_edges(&sub_transputs);
        }

        transputs
    }

    fn plot_nodes(&mut self) {
        for node_index in self.component.nodes().map(NodeIndex::from) {
            // plot node
            // NB some nodes no longer exist, due to optimisation
            if let Some(node_type) = self.get_state().node_type(&node_index) {
                let node_name = format!("named_{}", node_index.index());
                match node_type {
                    Node::Transistor => self.plot_transistor(node_index, node_name),
                    Node::Junction => self.plot_junction(node_index, node_name),
                };
            }
        }
    }

    fn plot_transistor(&mut self, node_index: NodeIndex, node_name: String) {
        let mut node = self.cluster.node_named(node_name);
        let power = self
            .plottable
            .get_state()
            .sink_value(&node_index, &Sink::Power)
            .unwrap();
        let collector = self
            .plottable
            .get_state()
            .sink_value(&node_index, &Sink::Collector)
            .unwrap();
        let ground = self
            .plottable
            .get_state()
            .source_value(&node_index, &Source::Ground)
            .unwrap();
        let emitter = self
            .plottable
            .get_state()
            .source_value(&node_index, &Source::Emitter)
            .unwrap();
        node.set_html(&format!(
            "<<table border=\"0\" cellspacing=\"0\">\
                <tr>\
                    <td port=\"p\" border=\"1\"{}>P</td>\
                    <td port=\"c\" border=\"1\"{}>C</td>\
                </tr>\
                <tr>\
                    <td border=\"1\" colspan=\"2\">T{:03}</td>\
                </tr>\
                <tr>\
                    <td port=\"g\" border=\"1\"{}>G</td>\
                    <td port=\"e\" border=\"1\"{}>E</td>\
                </tr>\
            </table>>",
            if power { " bgcolor=\"red\"" } else { "" },
            if collector { " bgcolor=\"red\"" } else { "" },
            node_index.index(),
            if ground { " bgcolor=\"red\"" } else { "" },
            if emitter { " bgcolor=\"red\"" } else { "" },
        ));
        node.set_shape(dot_writer::Shape::None);
    }

    fn plot_junction(&mut self, node_index: NodeIndex, node_name: String) {
        let mut node = self.cluster.node_named(node_name);
        node.set_label(&format!("J{:03}", node_index.index()));
        node.set_shape(dot_writer::Shape::Circle);
        if self
            .plottable
            .get_state()
            .sink_value(&node_index, &Sink::Power)
            .unwrap()
        {
            node.set_color(dot_writer::Color::Red);
            node.set_style(dot_writer::Style::Filled);
        }
    }

    fn plot_all_transputs(&mut self, sub_transputs: &TransputMap) -> TransputMap {
        let mut transputs = TransputMap::new();
        for named_range in self.component.labels.iter() {
            let size = named_range.range().end - named_range.range().start;
            self.settings.split_transputs.should_split(size);
            if size == 1 || !self.settings.split_transputs.should_split(size) {
                self.plot_combined_transputs(sub_transputs, named_range, &mut transputs);
            } else {
                self.plot_individual_transputs(sub_transputs, named_range, &mut transputs);
            }
        }
        transputs
    }

    fn plot_individual_transputs(
        &mut self,
        sub_transputs: &TransputMap,
        named_range: &NamedRange,
        transputs: &mut TransputMap,
    ) {
        let is_sink = named_range.is_sink();
        for (offset, index) in named_range.range().enumerate() {
            // plot actual box showing transput
            let pin = GenericPin::from_component(self.component, index, is_sink);
            let is_set = pin.is_set(self.get_state()).unwrap_or(false);
            let label = format!("{} {}", named_range.name(), offset);
            let transput_address = self.plot_transput(label, is_set);
            transputs.add(
                pin.clone(),
                Transput::new(
                    self.id.clone(),
                    Address::node(transput_address.clone()),
                    None,
                ),
            );

            if !self.plot_primitives() {
                continue;
            }

            // plot edge to primitive it maps to
            let primitive_address = match sub_transputs
                .get_first(&pin)
                .map(|t| t.address.clone())
                .or_else(|| Address::from_pin(&pin, self.get_state()))
            {
                Some(address) => address,
                None => {
                    println!("WARNING: missing transput! Bad optimization");
                    continue;
                }
            };
            if is_sink {
                self.plot_edge(&transput_address, &primitive_address, is_set, None, None);
            } else {
                self.plot_edge(&primitive_address, &transput_address, is_set, None, None);
            }
        }
    }

    fn plot_edge(
        &mut self,
        source: &dyn AsRef<[u8]>,
        sink: &dyn AsRef<[u8]>,
        is_set: bool,
        tail_label: Option<usize>,
        head_label: Option<usize>,
    ) {
        let mut edge = self.cluster.edge(source, sink).attributes();
        if is_set {
            edge.set_color(dot_writer::Color::Red);
        }
        if is_set && (tail_label.is_some() || head_label.is_some()) {
            edge.set_font_color(dot_writer::Color::Red);
        }
        // let label = match (head_label, tail_label) {
        //     (None, None) => None,
        //     (Some(offset), None) | (None, Some(offset)) => Some(format!("{}", offset)),
        //     (Some(tail), Some(head)) => Some(format!("{} -> {}", tail, head)),
        // };
        // if let Some(label) = label {
        //     edge.attributes().set_label(&label);
        // }
        if let Some(offset) = head_label {
            edge.set_head_label(&format!("   {}   ", offset));
        }
        if let Some(offset) = tail_label {
            edge.set_tail_label(&format!("   {}   ", offset));
        }
    }

    fn plot_combined_transputs(
        &mut self,
        sub_transputs: &TransputMap,
        named_range: &NamedRange,
        transputs: &mut TransputMap,
    ) {
        // plot actual box showing transput
        let is_sink = named_range.is_sink();
        let is_set = named_range.range().any(|index| {
            let pin = GenericPin::from_component(self.component, index, is_sink);
            pin.is_set(self.get_state()).unwrap_or(false)
        });
        let transput_address = self.plot_transput(named_range.name().to_string(), is_set);
        let is_single = (named_range.range().end - named_range.range().start) == 1;

        for (offset, index) in named_range.range().enumerate() {
            // TODO label lines instead
            let offset = if is_single { None } else { Some(offset) };
            let pin = GenericPin::from_component(self.component, index, is_sink);
            transputs.add(
                pin.clone(),
                Transput::new(
                    self.id.clone(),
                    Address::node(transput_address.clone()),
                    offset,
                ),
            );
            if !self.plot_primitives() {
                return;
            }
            // plot edge to primitive it maps to
            let is_set = pin.is_set(self.get_state()).unwrap_or(false);
            let primitive_address = match sub_transputs
                .get_first(&pin)
                .map(|t| t.address.clone())
                .or_else(|| Address::from_pin(&pin, self.get_state()))
            {
                Some(address) => address,
                None => {
                    println!("WARNING: missing transput! Bad optimization");
                    continue;
                }
            };
            if is_sink {
                self.plot_edge(&transput_address, &primitive_address, is_set, offset, None);
            } else {
                self.plot_edge(&primitive_address, &transput_address, is_set, None, offset);
            }
        }
    }

    fn plot_transput(&mut self, label: String, is_set: bool) -> dot_writer::NodeId {
        let mut node = self.cluster.node_auto();
        node.set_label(&label);
        node.set_shape(dot_writer::Shape::Rectangle);
        if is_set {
            node.set_color(dot_writer::Color::Red);
            node.set_style(dot_writer::Style::Filled);
        }
        node.id()
    }

    fn set_ranks(&mut self, transputs: &TransputMap) -> Option<()> {
        {
            // inputs at the top of the cluster
            let mut inputs_grouping = self.cluster.subgraph();
            inputs_grouping.set_rank(dot_writer::Rank::Min);
            for input in transputs.inputs() {
                inputs_grouping.node_named(&input.address);
            }
        }
        {
            // outputs at the bottom
            let mut outputs_grouping = self.cluster.subgraph();
            outputs_grouping.set_rank(dot_writer::Rank::Max);
            for output in transputs.outputs() {
                outputs_grouping.node_named(&output.address);
            }
        }
        // invisible edge required if there are no other nodes,
        // so that relative ranking between "min" and "max" can be determined
        let sink_range = self.component.labels.iter().find(|n| n.is_sink())?;
        let source_range = self.component.labels.iter().find(|n| !n.is_sink())?;

        // complexity required here to make sure we always get the first output and first input,
        // to make images consistent (as hashmap ordering is nondetrministic)
        let sink_pin = &self.component.sinks.get(sink_range.nth(0))?;
        let source_pin = &self.component.sources.get(source_range.nth(0))?;
        let input_address = transputs
            .get_first(&GenericPin::from_sink(
                sink_pin.index(),
                *sink_pin.pin_type(),
            ))
            .map(|t| &t.address)
            .unwrap();
        let output_address = transputs
            .get_first(&GenericPin::from_source(
                source_pin.index(),
                *source_pin.pin_type(),
            ))
            .map(|t| &t.address)
            .unwrap();
        self.cluster
            .edge(input_address, output_address)
            .attributes()
            .set_style(dot_writer::Style::Invisible);

        Some(())
    }

    fn plot_all_edges(&mut self, sub_transputs: &TransputMap) {
        let node_indicies: HashSet<_> = self.component.nodes().map(NodeIndex::from).collect();

        // Node -> Node or Node -> Output edges
        for node_index in node_indicies.iter() {
            // plot outgoing edges from this node
            self.plottable.get_state().visit_edges(
                node_index,
                &mut |source, sink, neighbour_index| {
                    let source_address = match Address::new(node_index, source, self.get_state()) {
                        Some(address) => address,
                        None => return,
                    };
                    let output_transput =
                        sub_transputs.get_first(&GenericPin::from_sink(*neighbour_index, *sink));
                    if let Some(sink_address) =
                        output_transput.map(|t| t.address.clone()).or_else(|| {
                            Address::new(neighbour_index, sink, self.get_state())
                                .filter(|_| node_indicies.contains(neighbour_index))
                        })
                    {
                        let is_set = self
                            .get_state()
                            .source_value(node_index, source)
                            .unwrap_or(false);
                        let offset = output_transput.and_then(|t| t.offset);
                        self.plot_edge(&source_address, &sink_address, is_set, None, offset);
                    }
                },
            );
        }

        // Sub Output -> Node or Sub Output -> Sub Input (different components)
        for (pin, source_transput) in sub_transputs.iter() {
            self.plottable.get_state().visit_edges(
                &pin.node_index,
                &mut |source, sink, neighbour_index| {
                    if pin.pin_type != PinType::Source(*source) {
                        return;
                    }
                    let input_transput = sub_transputs
                        .get_first(&GenericPin::from_sink(*neighbour_index, *sink))
                        .filter(|t| t.parent != source_transput.parent);
                    if !node_indicies.contains(neighbour_index) && input_transput.is_none() {
                        return;
                    }
                    let is_set = self
                        .get_state()
                        .source_value(&pin.node_index, source)
                        .unwrap_or(false);
                    if let Some(input_transput) = input_transput {
                        // Sub Output -> Sub Input (different components)
                        self.plot_edge(
                            &source_transput.address,
                            &input_transput.address,
                            is_set,
                            source_transput.offset,
                            input_transput.offset,
                        );
                    } else if let Some(sink_address) =
                        Address::new(neighbour_index, sink, self.plottable.get_state())
                    {
                        //  Sub Output -> Node
                        self.plot_edge(
                            &source_transput.address,
                            &sink_address,
                            is_set,
                            source_transput.offset,
                            None,
                        );
                    }
                },
            );
        }
    }
}

//
// Transput Map
//
// Maps primitive to all the inputs (or) outputs) associated with it.
//

struct TransputMap {
    transputs: HashMap<GenericPin, Vec<Transput>>,
}

impl TransputMap {
    fn new() -> Self {
        Self {
            transputs: HashMap::new(),
        }
    }

    fn add(&mut self, generic_pin: GenericPin, transput: Transput) {
        self.transputs
            .entry(generic_pin)
            .or_default()
            .push(transput);
    }

    fn extend(&mut self, other: Self) {
        for (generic_pin, transputs) in other.transputs {
            self.transputs
                .entry(generic_pin)
                .or_default()
                .extend(transputs);
        }
    }

    fn get_first(&self, generic_pin: &GenericPin) -> Option<&Transput> {
        // TODO cycle through options if more than one
        self.transputs.get(generic_pin).and_then(|v| v.first())
    }

    fn iter(&self) -> impl Iterator<Item = (&GenericPin, &Transput)> + '_ {
        self.transputs
            .iter()
            .flat_map(|(k, v)| v.iter().map(move |t| (k, t)))
    }

    fn inputs(&self) -> impl Iterator<Item = &Transput> + '_ {
        self.transputs
            .iter()
            .filter(|(k, _)| k.is_sink())
            .flat_map(|(_, v)| v.iter())
    }

    fn outputs(&self) -> impl Iterator<Item = &Transput> + '_ {
        self.transputs
            .iter()
            .filter(|(k, _)| !k.is_sink())
            .flat_map(|(_, v)| v.iter())
    }
}

//
// Transput Handle
//

struct Transput {
    parent: ComponentId,
    address: Address,
    offset: Option<usize>,
}

impl Transput {
    fn new(parent: ComponentId, address: Address, offset: Option<usize>) -> Self {
        Self {
            parent,
            address,
            offset,
        }
    }
}

//
// Pin Type
//
// To allow abstraciton over Sink and Source
//

#[derive(Clone, Eq, Hash, PartialEq)]
struct GenericPin {
    pin_type: PinType,
    node_index: NodeIndex,
}

impl GenericPin {
    fn from_component(component: &plan::Component, index: usize, is_sink: bool) -> Self {
        match is_sink {
            true => {
                let pin = component.sinks[index];
                Self::from_sink(pin.index(), *pin.pin_type())
            }
            false => {
                let pin = component.sources[index];
                Self::from_source(pin.index(), *pin.pin_type())
            }
        }
    }

    fn from_sink(node_index: NodeIndex, sink: Sink) -> Self {
        Self {
            pin_type: PinType::Sink(sink),
            node_index,
        }
    }

    fn from_source(node_index: NodeIndex, source: Source) -> Self {
        Self {
            pin_type: PinType::Source(source),
            node_index,
        }
    }

    fn is_set(&self, state: &dyn State) -> Option<bool> {
        match self.pin_type {
            PinType::Sink(sink) => state.sink_value(&self.node_index, &sink),
            PinType::Source(source) => state.source_value(&self.node_index, &source),
        }
    }

    fn is_sink(&self) -> bool {
        match self.pin_type {
            PinType::Sink(_) => true,
            PinType::Source(_) => false,
        }
    }
}

#[derive(Eq, Hash, Clone, PartialEq)]
enum PinType {
    Sink(Sink),
    Source(Source),
}

//
// Addresses
//

trait AsName {
    fn as_name(&self) -> &str;
}

impl AsName for Sink {
    fn as_name(&self) -> &str {
        match self {
            Self::Power => "p",
            Self::Collector => "c",
        }
    }
}

impl AsName for Source {
    fn as_name(&self) -> &str {
        match self {
            Self::Ground => "g",
            Self::Emitter => "e",
        }
    }
}

#[derive(Clone, Debug)]
struct Address {
    id: String,
}

impl Address {
    fn node(node_id: dot_writer::NodeId) -> Self {
        Self { id: node_id.into() }
    }

    fn new(index: &NodeIndex, as_name: &dyn AsName, state: &dyn State) -> Option<Self> {
        Some(match state.node_type(index)? {
            Node::Junction => Self {
                id: format!("named_{}", index.index()),
            },
            Node::Transistor => Self {
                id: format!("named_{}:{}", index.index(), as_name.as_name()),
            },
        })
    }

    fn from_pin(generic_pin: &GenericPin, state: &dyn State) -> Option<Self> {
        match generic_pin.pin_type {
            PinType::Sink(sink) => Self::new(&generic_pin.node_index, &sink, state),
            PinType::Source(source) => Self::new(&generic_pin.node_index, &source, state),
        }
    }
}

impl<'a> From<&'a Address> for String {
    fn from(address: &'a Address) -> Self {
        address.id.clone()
    }
}

impl AsRef<[u8]> for Address {
    fn as_ref(&self) -> &[u8] {
        self.id.as_ref()
    }
}
