use petgraph::{
    algo::toposort,
    visit::{EdgeRef, VisitMap, Visitable},
    Direction,
};
use std::collections::{hash_map, BinaryHeap, HashMap, HashSet};

use super::plan::{Graph, Pin, Plan, Sink, Source};

// if a cyclic graph, then longest path is infinite
pub fn longest_path(plan: &Plan) -> Option<usize> {
    let ordered_nodes = toposort(&plan.graph, None).ok()?;
    let max_node = plan.graph.node_indices().next_back().map(|n| n.index())?;
    let mut distance: Vec<Option<usize>> = vec![None; max_node + 1];
    for sink in plan.component.sinks.iter() {
        distance[sink.index().index()] = Some(1);
    }
    for node in ordered_nodes {
        if let Some(max_parent) = plan
            .graph
            .neighbors_directed(node, Direction::Incoming)
            .filter_map(|parent| distance[parent.index()])
            .max()
        {
            let distance = &mut distance[node.index()];
            let old_distance = distance.unwrap_or(0);
            *distance = Some(std::cmp::max(old_distance, max_parent + 1));
        }
    }
    plan.component
        .sources
        .iter()
        .filter_map(|source| distance[source.index().index()])
        .max()
}

pub fn shortest_path(graph: &Graph, sinks: &[Pin<Sink>], sources: &[Pin<Source>]) -> usize {
    let outputs: HashSet<_> = sources.iter().map(Pin::index).collect();
    let mut visited = graph.visit_map();
    let mut scores = HashMap::new();
    let mut visit_next = BinaryHeap::new();

    for input in sinks {
        visit_next.push((1, input.index()));
        scores.insert(input.index(), 1);
    }
    while let Some((node_score, node)) = visit_next.pop() {
        if visited.is_visited(&node) {
            continue;
        }
        if outputs.contains(&node) {
            break;
        }
        for edge in graph.edges(node) {
            let next = edge.target();
            if visited.is_visited(&next) {
                continue;
            }
            let mut next_score = node_score + 1;
            match scores.entry(next) {
                hash_map::Entry::Occupied(ent) => {
                    if next_score < *ent.get() {
                        *ent.into_mut() = next_score;
                    } else {
                        next_score = *ent.get();
                    }
                }
                hash_map::Entry::Vacant(ent) => {
                    ent.insert(next_score);
                }
            }
            visit_next.push((next_score, next));
        }
        visited.visit(node);
    }
    *outputs
        .iter()
        .filter_map(|i| scores.get(i))
        .min()
        .unwrap_or(&0)
}

#[cfg(test)]
mod test {
    use super::super::plan::Graph;
    use super::*;

    #[test]
    fn test_path_length_trivial() {
        let graph = Graph::new();
        assert_eq!(shortest_path(&graph, &[], &[]), 0);
        // assert_eq!(longest_path(&graph, &[], &[]), Some(0));
    }

    // #[test]
    // fn test_path_length_one_node() {
    //     let mut graph = Graph::new();

    //     let mut builder = Builder::default();
    //     let transistor = builder.new_transistor();
    //     builder.set_input(&transistor.collector());
    //     builder.set_input(&transistor.power());
    //     builder.set_output(&transistor.ground());
    //     builder.set_output(&transistor.emitter());
    //     let plan = builder.finish();
    //     assert_eq!(plan.shortest_path_length(), 1);
    //     assert_eq!(plan.longest_path_length(), Some(1));
    // }

    // #[test]
    // fn test_path_length_simple_linear() {
    //     let mut builder = Builder::default();
    //     let transistor = builder.new_transistor();
    //     builder.set_input(&transistor.collector());
    //     let junction = builder.new_junction();
    //     builder.join(&transistor.emitter(), &junction.input());
    //     builder.set_output(&junction.output());
    //     let plan = builder.finish();
    //     assert_eq!(plan.shortest_path_length(), 2);
    //     assert_eq!(plan.longest_path_length(), Some(2));
    // }

    // #[test]
    // fn test_path_length_simple_diamond() {
    //     let mut builder = Builder::default();
    //     let first = builder.new_junction();
    //     let second = builder.new_junction();
    //     let third = builder.new_junction();
    //     builder.set_input(&first.input());
    //     builder.join(&first.output(), &second.input());
    //     builder.set_output(&second.output());
    //     builder.set_input(&third.input());
    //     builder.set_output(&third.output());
    //     let plan = builder.finish();
    //     assert_eq!(plan.shortest_path_length(), 1);
    //     assert_eq!(plan.longest_path_length(), Some(2));
    // }

    // #[test]
    // fn test_path_length_loop() {
    //     let mut builder = Builder::default();
    //     let first = builder.new_junction();
    //     let second = builder.new_junction();
    //     let third = builder.new_junction();
    //     builder.set_input(&first.input());
    //     builder.join(&first.output(), &second.input());
    //     builder.join(&second.output(), &second.input());
    //     builder.join(&second.output(), &third.input());
    //     builder.set_output(&third.output());
    //     let plan = builder.finish();
    //     assert_eq!(plan.shortest_path_length(), 3);
    //     assert_eq!(plan.longest_path_length(), None);
    // }
}
