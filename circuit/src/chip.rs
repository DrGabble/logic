use super::plan::{Builder, Pin, Sink, Source};
use crate::pinout::NamedRange;

//
// Traits
//

pub trait Chip {
    fn labels(&self) -> Vec<NamedRange>;
    fn sinks(&self) -> Vec<Pin<Sink>>;
    fn sources(&self) -> Vec<Pin<Source>>;
}

pub trait BuildChip {
    type Output: Chip + 'static;

    fn name(&self) -> String;
    fn build_chip(&self, builder: &mut Builder) -> Self::Output;
}

pub trait ChipBuilder {
    fn fields(&self) -> Vec<String>;
    fn set_field(&mut self, name: &str, value: &str) -> Result<(), String>;
    fn name(&self) -> String;
    fn build_boxed_chip(&self, builder: &mut Builder) -> Box<dyn Chip>;
}

//
// Macros
//

macro_rules! derive_builder {
    ($builder:ident $(; $field:ident : $type:ty = $default:expr )*) => {
        pub struct $builder {
            $(
                $field: $type
            ),*
        }

        impl Default for $builder {
            fn default() -> Self {
                Self {
                    $(
                        $field: $default,
                    )*
                }
            }
        }

        impl ChipBuilder for $builder {
            fn name(&self) -> String {
                <Self as BuildChip>::name(self)
            }

            fn fields(&self) -> Vec<String> {
                vec![
                    $(
                        format!("{} ({})", stringify!($field), stringify!($type))
                    ),*
                ]
            }

            #[allow(unreachable_code, unused_variables)]
            fn set_field(&mut self, name: &str, value: &str) -> Result<(), String> {
                parse_fields!(self, name, value $(, $field)*);
                Ok(())
            }

            fn build_boxed_chip(&self, builder: &mut Builder) -> Box<dyn Chip> {
                Box::new(self.build_chip(builder))
            }
        }

        impl $builder {
            #[allow(dead_code)]
            pub fn new($($field: $type),*) -> Self {
                Self {
                    $(
                        $field
                    ),*
                }
            }
        }
    };
}

macro_rules! parse_fields {
    ($instance:expr, $name:expr, $value:expr $(, $field:ident)*) => {
        match $name {
            $(
                stringify!($field) => $instance.$field = $value.parse().or(Err(format!("Could not parse value for '{}'", stringify!($field))))?,
            )*
            _ => return Err(format!("No field '{}'", $name)),
        }
    };
}

macro_rules! labels {
    ($($command:ident $name:ident $index:expr),*) => {
        {
            vec![
                $(
                    named_range!($command $name $index)
                ),*
            ]
        }
    };
}

macro_rules! derive_chip {
    ($chip:ident $(;)? $($command:ident $label:ident $callable:expr),*) => {
        impl $chip {
            $(
                derive_getter!($command $label $callable);
            )*
        }

        impl Chip for $chip {
            #[allow(clippy::redundant_closure_call)]
            fn labels(&self) -> Vec<NamedRange> {
                labels!($($command $label $callable(self)),*)
            }

            fn sinks(&self) -> Vec<Pin<Sink>> {
                self.sinks.iter().cloned().collect()
            }

            fn sources(&self) -> Vec<Pin<Source>> {
                self.sources.iter().cloned().collect()
            }
        }
    };
}

macro_rules! derive_getter {
    (sink $name:ident $callable:expr) => {
        #[allow(dead_code)]
        pub fn $name(&self) -> Pin<Sink> {
            #[allow(clippy::redundant_closure_call)]
            self.sinks[$callable(self)]
        }
    };

    (sinks $name:ident $callable:expr) => {
        pub fn $name(&self) -> &[Pin<Sink>] {
            #[allow(clippy::redundant_closure_call)]
            &self.sinks[$callable(self)]
        }
    };

    (source $name:ident $callable:expr) => {
        #[allow(dead_code)]
        pub fn $name(&self) -> Pin<Source> {
            #[allow(clippy::redundant_closure_call)]
            self.sources[$callable(self)]
        }
    };

    (sources $name:ident $callable:expr) => {
        pub fn $name(&self) -> &[Pin<Source>] {
            #[allow(clippy::redundant_closure_call)]
            &self.sources[$callable(self)]
        }
    };
}

macro_rules! named_range {
    (sink $name:ident $index:expr) => {
        NamedRange::sink(stringify!($name).into(), $index..($index + 1))
    };

    (sinks $name:ident $range:expr) => {
        NamedRange::sink(stringify!($name).into(), $range)
    };

    (source $name:ident $index:expr) => {
        NamedRange::source(stringify!($name).into(), $index..($index + 1))
    };

    (sources $name:ident $range:expr) => {
        NamedRange::source(stringify!($name).into(), $range)
    };
}
