#![allow(incomplete_features)]
pub mod device;
#[macro_use]
pub mod pinout;

mod delay;
mod dense;
#[macro_use]
mod chip;
mod emulator;
mod optimize;
mod plan;
mod simple;
#[macro_use]
mod testing;

pub mod cpu;
pub mod gate;
pub mod plot;

pub use chip::ChipBuilder;
pub use dense::DenseEmulator;
pub use emulator::ChipDevice;
pub use optimize::Optimizer;
pub use plan::{Component, Plan};
pub use simple::SimpleEmulator;
pub use testing::{TestCase, TestSuite};