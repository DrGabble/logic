use crate::device::NamedDevice;
use crate::pinout::{FindNamedRange, NamedRange};

//
// Test Suite
//

#[derive(Default)]
pub struct TestSuite {
    cases: Vec<TestCase>,
}

impl TestSuite {
    pub fn add(&mut self, case: TestCase) {
        self.cases.push(case);
    }

    pub fn set_clock_period(&mut self, steps: usize) {
        for case in self.cases.iter_mut() {
            case.set_clock_period(steps);
        }
    }

    pub fn cases(&self) -> &[TestCase] {
        &self.cases
    }

    pub fn run(&self, device: &mut dyn NamedDevice) {
        let mut step = 0;
        for case in self.cases.iter() {
            case.run(step, device);
            step += case.steps();
        }
    }
}

//
// Test Case
//

pub struct TestCase {
    steps: usize,
    inputs: Vec<Option<bool>>,
    outputs: Vec<Option<bool>>,
    clock_period: usize,
}

impl Default for TestCase {
    fn default() -> Self {
        Self {
            steps: 1,
            inputs: Vec::new(),
            outputs: Vec::new(),
            clock_period: 0,
        }
    }
}

impl TestCase {
    pub fn set_steps(&mut self, steps: usize) {
        self.steps = steps;
    }

    pub fn steps(&self) -> usize {
        self.steps
    }

    pub fn set_input(&mut self, index: usize, value: bool) {
        while index >= self.inputs.len() {
            self.inputs.push(None);
        }
        self.inputs[index] = Some(value);
    }

    pub fn set_output(&mut self, index: usize, value: bool) {
        while index >= self.outputs.len() {
            self.outputs.push(None);
        }
        self.outputs[index] = Some(value);
    }

    pub fn set_clock_period(&mut self, steps: usize) {
        self.clock_period = steps;
    }

    pub fn get_clock_period(&self) -> usize {
        self.clock_period
    }

    pub fn run(&self, start_step: usize, device: &mut dyn NamedDevice) {
        for step in start_step..(start_step + self.steps) {
            self.apply_inputs(device, step);
            device.step();
        }
        let outputs: Vec<_> = device
            .sources()
            .into_iter()
            .map(|o| Some(device.read(o)))
            .collect();
        let correct: Vec<_> = self
            .outputs
            .iter()
            .zip(outputs.iter())
            .map(|(expected, actual)| match (expected, actual) {
                (None, _) => true,
                (Some(a), Some(b)) => a == b,
                (Some(_), None) => false,
            })
            .collect();
        if !correct.iter().all(|b| *b) {
            panic!("{}", self.error_message(&outputs, &correct, device));
        }
    }

    fn error_message(
        &self,
        outputs: &[Option<bool>],
        correct: &[bool],
        device: &dyn NamedDevice,
    ) -> String {
        let labels = device.labels();
        let indent = labels.iter().map(|l| l.name().len()).max().unwrap();
        let mut message = self.error_summary(outputs, correct, indent);
        for output in labels.iter().filter(|n| !n.is_sink()) {
            let range = output.range();
            message += &format!("\n  {:<indent$}: ", output.name(), indent = indent);
            message.extend((0..range.start).map(|_| ' '));
            message += &Self::bits_to_string(&outputs[range], '0');
        }
        message
    }

    fn error_summary(&self, outputs: &[Option<bool>], correct: &[bool], indent: usize) -> String {
        format!(
            concat!(
                "Input {} gave incorrect output:\n",
                "  {:<indent$}: {}\n",
                "  {:<indent$}: {}\n",
                "  {:indent$}  {}",
            ),
            Self::bits_to_string(&self.inputs, '0'),
            "Expected",
            Self::bits_to_string(&self.outputs, '?'),
            "Actual",
            Self::bits_to_string(outputs, '0'),
            "",
            correct
                .iter()
                .map(|v| match v {
                    true => ' ',
                    false => '^',
                })
                .collect::<String>(),
            indent = indent
        )
    }

    pub fn apply_inputs(&self, device: &mut dyn NamedDevice, step: usize) {
        let labels = device.labels();

        for index in device.sinks() {
            device.write(index, self.get_input(&labels, step, index).unwrap_or(false));
        }
    }

    pub fn get_input(&self, labels: &[NamedRange], step: usize, index: usize) -> Option<bool> {
        // clock
        if self.clock_period > 0 {
            if let Some(clock_index) = labels.find("clock").map(|n| n.nth(0)) {
                if index == clock_index {
                    let clock_signal = ((step / self.clock_period) % 2) == 1;
                    return Some(clock_signal);
                }
            }
        }

        // power (if not already set deliberately to something)
        if let Some(power_index) = labels.find("power").map(|n| n.nth(0)) {
            if index == power_index && self.inputs.get(power_index).cloned().flatten().is_none() {
                return Some(true);
            }
        }

        self.inputs.get(index).cloned().flatten()
    }

    pub fn bits_to_string(bits: &[Option<bool>], default: char) -> String {
        let mut string = String::new();
        for bit in bits.iter() {
            string.push(match bit {
                None => default,
                Some(true) => '1',
                Some(false) => '0',
            });
        }
        string
    }
}

//
// Tests a chip by setting input and then asserting that output matches expected values.
// Runs the test again after optimizing, as a way of fuzz testing the Optimizer.
//
#[cfg(test)]
macro_rules! test_chip {
    ($builder:expr; $( $( $input:tt ),+ => $( $output:tt ),+ );+ ) => {
        {
            use $crate::ChipDevice;
            use $crate::plan::Plan;
            use $crate::{DenseEmulator, SimpleEmulator};
            use $crate::optimize;

            // test unoptimized
            let mut plan = Plan::from_chip(&$builder, 0);
            let unoptomized_steps = {
                let mut unoptimized_device = ChipDevice::<SimpleEmulator>::from(&plan);
                let steps = unoptimized_device.gate_delay().expect("Unknown gate delay");
                let tests = test_suite!(&unoptimized_device; period steps; $( $( $input ),+ => $( $output ),+ );+);
                tests.run(&mut unoptimized_device);
                steps
            };

            // test again optimized, to test that optimization doesn't break it
            optimize::Optimizer::new(&mut plan).run();
            let optimized_steps = {
                let mut optimized_device = ChipDevice::<DenseEmulator>::from(&plan);
                let steps = optimized_device.gate_delay().expect("Unknown gate delay");
                let tests = test_suite!(&optimized_device; period steps; $( $( $input ),+ => $( $output ),+ );+);
                tests.run(&mut optimized_device);
                steps
            };

            // check optimization is actually improving gate delay!
            assert!(unoptomized_steps >= optimized_steps);
        }
    };
}

#[cfg(test)]
macro_rules! test_device {
    ($builder:expr) => {{
        use $crate::plan::Plan;
        use $crate::ChipDevice;
        use $crate::SimpleEmulator;
        let plan = Plan::from_chip(&$builder, 0);
        ChipDevice::<SimpleEmulator>::from(&plan)
    }};
}

#[cfg(test)]
macro_rules! test_suite {
    ($device:expr; $(clock $clock_steps:expr;)? period $period:expr; $( $( $input:tt ),+ => $( $output:tt ),+ );+ ) => {
        {
            use $crate::testing::{TestCase, TestSuite};
            let mut test_suite = TestSuite::default();
            $(
                {
                    let mut test_case = TestCase::default();
                    test_case.set_steps($period);
                    set_value!($device, |i, v| test_case.set_input(i, v); set $( $input ),* );
                    set_value!($device, |i, v| test_case.set_output(i, v); set $( $output ),* );
                    test_suite.add(test_case);
                }
            );*
            $(
                test_suite.set_clock_period($clock_steps);
            )?
            test_suite
        }
    };
}

#[cfg(test)]
macro_rules! set_value {
    ($device:expr, $set_func:expr; set $values:literal $(, $rest:tt )*) => {
        {
            use $crate::testing;
            for (index, value) in testing::to_bool_iter($values).enumerate() {
                #[allow(clippy::redundant_closure_call)]
                $set_func(index, value);
            }
            set_value!($device, $set_func; $( $rest ),*);
        }
    };

    ($device:expr, $set_func:expr; set $first:tt $(, $rest:tt )*) => {
        set_value!($device, $set_func; $first);
        set_value!($device, $set_func; $( $rest ),*);
    };

    ($device:expr, $set_func:expr; $first:tt $(, $rest:tt )+) => {
        set_value!($device, $set_func; $first);
        set_value!($device, $set_func; $( $rest ),*);
    };

    ($device: expr, $set_func:expr; ( $name:literal $offset:literal : $value:expr ) ) => {
        {
            use $crate::pinout::{FindNamedRange, Pinout};
            let index = $device
                .labels()
                .find($name)
                .expect(&format!("No input / output found with name '{}'", $name))
                .nth($offset);
            #[allow(clippy::redundant_closure_call)]
            $set_func(index, $value);
        }
    };

    ($device: expr, $set_func:expr; ( $name:literal : $value:expr ) ) => {
        {
            use $crate::pinout::{Pinout, FindNamedRange};
            let range = $device.labels()
                .find($name)
                .expect(&format!("No input / output found with name '{}'", $name))
                .range();
            let mut value = $value;
            for index in range {
                #[allow(clippy::redundant_closure_call)]
                $set_func(index, (value & 1) == 1);
                value >>= 1;
            }
        }
    };

    ($device: expr, $test_case:expr; ) => {};
}

#[cfg(test)]
pub fn to_bool_iter(string: &str) -> impl Iterator<Item = bool> + '_ {
    string.chars().map(|c| match c {
        '1' => true,
        '0' => false,
        _ => panic!("Invalid digit for input, must be '1' or '0'"),
    })
}
