use std::collections::{HashMap, HashSet};

use super::plan::{Graph, Node, Plan};
use petgraph::{graph::NodeIndex, visit::EdgeRef, Direction};
use std::collections::VecDeque;

pub struct Optimizer<'a> {
    plan: &'a mut Plan,
    transputs: HashSet<NodeIndex>,
}

impl<'a> Optimizer<'a> {
    pub fn new(plan: &'a mut Plan) -> Self {
        Self {
            transputs: plan.component.transputs_recursive().collect(),
            plan,
        }
    }

    pub fn run(&mut self) {
        self.garbage_collection();
        self.minimise_all_junction_spans();
        self.remove_redundant_junctions();
        // TODO component sinks / sources where it allows more optimisation (e.g trailing junctions for readouts)
        // TODO can follow marked "power" input to caculate redudant "always high" paths
        // TODO resolve cycles with constant outputs and propogate
        // TODO check not broken: still path from input to output?
    }
}

//
// Merging Junction Spans
//
// Some spans of junctions can be replace with a single junction, which is faster to emulate.
// This only applies to junctions that have the same inputs, and carry the same signal.
// Combine all connected junctions into one super junction (taking care not to remove transputs).
//

impl<'a> Optimizer<'a> {
    fn minimise_all_junction_spans(&mut self) {
        // TODO not loop proof!
        // create span groups
        let mut junction_spans = SpanFinder::new();
        for node_index in self
            .plan
            .graph
            .node_indices()
            .filter(|i| self.is_junction(*i))
        {
            junction_spans.add(&self.plan.graph, node_index);
        }
        for group in junction_spans.spans() {
            // TODO assert!(!self.has_loops(&group));
            for sub_group in self.find_sub_groups(group) {
                self.merge_junctions(sub_group);
            }
        }
    }

    fn find_sub_groups(&self, group: impl Iterator<Item = NodeIndex>) -> Vec<Vec<NodeIndex>> {
        let mut junction_to_inputs: HashMap<NodeIndex, HashSet<NodeIndex>> = HashMap::new();

        // for each node...
        for ancestor in group {
            // get a list of it's inputs (include itself if a transput)
            let mut inputs: Vec<NodeIndex> = self
                .plan
                .graph
                .neighbors_directed(ancestor, Direction::Incoming)
                .filter(|n| !self.is_junction(*n))
                .collect();
            if self.transputs.contains(&ancestor) {
                inputs.push(ancestor);
            }
            if inputs.is_empty() {
                continue;
            }
            // trace descendants (including self), marking them as having all these inputs
            let mut descendants: Vec<_> = self
                .plan
                .graph
                .neighbors_directed(ancestor, Direction::Outgoing)
                .filter(|n| self.is_junction(*n))
                .chain(std::iter::once(ancestor))
                .collect();
            while let Some(node) = descendants.pop() {
                junction_to_inputs
                    .entry(node)
                    .or_default()
                    .extend(inputs.iter().cloned());
                descendants.extend(
                    self.plan
                        .graph
                        .neighbors_directed(node, Direction::Outgoing)
                        .filter(|n| self.is_junction(*n)),
                );
            }
        }

        // flip mapping from "junction -> inputs" to "inputs -> junctions"
        let mut sub_groups = HashMap::<Vec<NodeIndex>, Vec<NodeIndex>>::new();
        for (junction, inputs) in junction_to_inputs {
            sub_groups
                .entry(inputs.into_iter().collect())
                .or_default()
                .push(junction);
        }

        sub_groups.into_values().collect()
    }

    fn merge_junctions(&mut self, junctions: Vec<NodeIndex>) {
        let thief_index = junctions
            .iter()
            .find(|n| self.transputs.contains(n))
            .or_else(|| junctions.first())
            .unwrap();
        for victim_index in junctions.iter().filter(|n| *n != thief_index) {
            self.merge_junction(*thief_index, *victim_index);
        }
    }

    fn is_junction(&self, node_index: NodeIndex) -> bool {
        matches!(
            self.plan.graph.node_weight(node_index),
            Some(Node::Junction)
        )
    }

    fn is_deletable_junction(&self, node_index: NodeIndex) -> bool {
        self.is_junction(node_index) && !self.transputs.contains(&node_index)
    }

    fn merge_junction(&mut self, thief_index: NodeIndex, victim_index: NodeIndex) {
        assert!(self.is_junction(thief_index));
        assert!(self.is_deletable_junction(victim_index));
        assert!(thief_index != victim_index);
        self.steal_outgoing(thief_index, victim_index);
        self.steal_incoming(thief_index, victim_index);
        self.plan.graph.remove_node(victim_index);
    }

    fn steal_outgoing(&mut self, thief_index: NodeIndex, victim_index: NodeIndex) {
        for (weight, target_index) in self
            .plan
            .graph
            .edges_directed(victim_index, petgraph::Direction::Outgoing)
            .filter(|e| e.target() != thief_index && e.target() != victim_index)
            .map(|e| (*e.weight(), e.target()))
            .collect::<Vec<_>>()
        {
            self.plan.graph.add_edge(thief_index, target_index, weight);
        }
    }

    fn steal_incoming(&mut self, thief_index: NodeIndex, victim_index: NodeIndex) {
        for (weight, target_index) in self
            .plan
            .graph
            .edges_directed(victim_index, petgraph::Direction::Incoming)
            .filter(|e| e.source() != thief_index && e.source() != victim_index)
            .map(|e| (*e.weight(), e.source()))
            .collect::<Vec<_>>()
        {
            self.plan.graph.add_edge(target_index, thief_index, weight);
        }
    }
}

struct SpanFinder {
    span_to_node: Vec<Vec<NodeIndex>>,
    node_to_span: Vec<Option<Span>>,
}

impl SpanFinder {
    pub fn new() -> Self {
        Self {
            span_to_node: Vec::new(),
            node_to_span: Vec::new(),
        }
    }

    pub fn add(&mut self, graph: &Graph, node_index: NodeIndex) {
        if self.get_span(node_index).is_some() {
            return;
        }

        let span_id = graph
            .neighbors_undirected(node_index) // search all neighbours...
            .filter_map(|n| self.get_span(n)) // for one already in a span...
            .next() // join arbitrary neighbouring span...
            .unwrap_or_else(|| Span(self.next_span_id())); // or else start new span.

        let mut unvisited = vec![node_index];

        // propogate span_id to all connected descendants which already have a (different) span
        while let Some(parent_index) = unvisited.pop() {
            self.set_span(parent_index, span_id);
            unvisited.extend(graph.neighbors_undirected(parent_index).filter(|n| {
                match self.get_span(*n) {
                    Some(child_span) => child_span != span_id,
                    _ => false,
                }
            }));
        }
    }

    fn get_span(&self, node_index: NodeIndex) -> Option<Span> {
        self.node_to_span.get(node_index.index()).cloned().flatten()
    }

    fn set_span(&mut self, node_index: NodeIndex, span_id: Span) {
        // grow vectors to correct size
        while node_index.index() >= self.node_to_span.len() {
            self.node_to_span.push(None);
        }

        // remove from old span if any
        if let Some(old_span_id) = self.node_to_span[node_index.index()] {
            let old_span = &mut self.span_to_node[old_span_id.0];
            old_span.remove(old_span.iter().position(|n| n == &node_index).unwrap());
        }

        self.span_to_node[span_id.0].push(node_index);
        self.node_to_span[node_index.index()] = Some(span_id);
    }

    pub fn spans(&self) -> impl Iterator<Item = impl Iterator<Item = NodeIndex> + '_> + '_ {
        self.span_to_node.iter().map(|span| span.iter().cloned())
    }

    fn next_span_id(&mut self) -> usize {
        let next_span_id = self.span_to_node.len();
        self.span_to_node.push(Vec::new());
        next_span_id
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Span(usize);

//
// Removing Redundant Junctions
//
// Some junctions are redundant, and could be replaced with direct connections
// from all of its inputs to all of its outputs, and use less edges (and one less junction).
// This is the case when inputs * outputs <= inputs + outputs.

impl<'a> Optimizer<'a> {
    fn remove_redundant_junctions(&mut self) {
        for node_index in self
            .plan
            .graph
            .node_indices()
            .filter(|n| self.is_redundant(*n))
            .collect::<Vec<_>>()
        {
            let inputs: Vec<_> = self
                .plan
                .graph
                .edges_directed(node_index, petgraph::Direction::Incoming)
                .map(|e| (e.source(), e.weight().0))
                .collect();
            let outputs: Vec<_> = self
                .plan
                .graph
                .edges_directed(node_index, petgraph::Direction::Outgoing)
                .map(|e| (e.target(), e.weight().1))
                .collect();
            for (input_index, input_weight) in inputs.iter() {
                for (output_index, output_weight) in outputs.iter() {
                    if input_index != output_index {
                        self.plan.graph.add_edge(
                            *input_index,
                            *output_index,
                            (*input_weight, *output_weight),
                        );
                    }
                }
            }
            self.plan.graph.remove_node(node_index);
        }
    }

    fn is_redundant(&self, node_index: NodeIndex) -> bool {
        if !self.is_deletable_junction(node_index) {
            return false;
        }
        let inputs = self
            .plan
            .graph
            .neighbors_directed(node_index, petgraph::Direction::Incoming)
            .count();
        let outputs = self
            .plan
            .graph
            .neighbors_directed(node_index, petgraph::Direction::Outgoing)
            .count();
        inputs * outputs <= inputs + outputs
    }
}

//
// Garbage Collection
//
// Identify all nodes which are not reached by any of the inputs. These will never be set to anything,
// and so can be removed as redundant.
// Also identify any nodes which are not connect to any outputs. They have no side effects,
// so can be removed as redundant.
//

impl<'a> Optimizer<'a> {
    fn garbage_collection(&mut self) {
        let mut marked = Marked::new();
        let mut to_visit = VecDeque::new();
        self.mark_and_sweep(&mut marked, &mut to_visit, petgraph::Direction::Outgoing);
        marked.flip();
        self.mark_and_sweep(&mut marked, &mut to_visit, petgraph::Direction::Incoming);
    }

    fn mark_and_sweep(
        &mut self,
        marked: &mut Marked,
        to_visit: &mut VecDeque<NodeIndex>,
        direction: petgraph::Direction,
    ) {
        match direction {
            Direction::Outgoing => {
                to_visit.extend(self.plan.component.sinks.iter().map(|s| s.index()))
            }
            Direction::Incoming => {
                to_visit.extend(self.plan.component.sources.iter().map(|s| s.index()))
            }
        }
        while let Some(node_index) = to_visit.pop_front() {
            if !marked.is_set(node_index) {
                marked.set(node_index);
                to_visit.extend(
                    self.plan
                        .graph
                        .neighbors_directed(node_index, direction)
                        .filter(|n| !marked.is_set(*n)),
                );
            }
        }
        for node_index in marked.iter() {
            if !self.transputs.contains(&node_index) {
                self.plan.graph.remove_node(node_index);
            }
        }
    }
}

struct Marked {
    marked: Vec<bool>,
    flag: bool,
}

impl Marked {
    fn new() -> Self {
        Self {
            marked: Vec::new(),
            flag: true,
        }
    }

    fn set(&mut self, node_index: NodeIndex) {
        while self.marked.len() <= node_index.index() {
            self.marked.push(!self.flag);
        }
        self.marked[node_index.index()] = self.flag;
    }

    fn is_set(&self, node_index: NodeIndex) -> bool {
        self.marked.get(node_index.index()) == Some(&self.flag)
    }

    fn iter(&self) -> impl Iterator<Item = NodeIndex> + '_ {
        self.marked
            .iter()
            .enumerate()
            .filter(move |(_, f)| **f != self.flag)
            .map(|(i, _)| NodeIndex::new(i))
    }

    fn flip(&mut self) {
        self.flag = !self.flag;
    }
}
