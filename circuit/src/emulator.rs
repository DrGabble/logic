use super::delay;
use super::plan::{Component, Plan};
use super::plot::{Plottable, State};
use crate::device::{Device, NamedDevice};
use crate::pinout::{NamedRange, Pinout};

pub trait Emulator<'a>: Device + From<&'a Plan> {}

pub struct ChipDevice<E> {
    emulator: E,
    component: Component,
    gate_delay: Option<usize>,
}

impl<E> ChipDevice<E> {
    pub fn gate_delay(&self) -> Option<usize> {
        self.gate_delay
    }
}

impl<'a, E: Emulator<'a> + State> Plottable for ChipDevice<E> {
    fn get_component(&self) -> &Component {
        &self.component
    }

    fn get_state(&self) -> &dyn State {
        &self.emulator
    }
}

impl<'a, E: Emulator<'a>> From<&'a Plan> for ChipDevice<E> {
    fn from(plan: &'a Plan) -> Self {
        Self {
            gate_delay: delay::longest_path(plan),
            emulator: E::from(plan),
            component: plan.component.clone(),
        }
    }
}

impl<'a, E: Emulator<'a>> NamedDevice for ChipDevice<E> {}

impl<'a, E: Emulator<'a>> Device for ChipDevice<E> {
    fn write(&mut self, sink: usize, value: bool) {
        self.emulator.write(sink, value);
    }

    fn step(&mut self) {
        self.emulator.step();
    }

    fn read(&self, source: usize) -> bool {
        self.emulator.read(source)
    }

    fn reset(&mut self) {
        self.emulator.reset();
    }
}

impl<E> Pinout for ChipDevice<E> {
    fn name(&self) -> String {
        self.component.name.clone()
    }

    fn labels(&self) -> Vec<NamedRange> {
        self.component.labels.clone()
    }
}
