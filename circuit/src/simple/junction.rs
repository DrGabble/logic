#[derive(Default, Clone, Debug)]
pub struct Junction {
    input: bool,
    output: bool,
}

impl Junction {
    pub fn set(&mut self, value: bool) {
        self.input |= value;
    }

    pub fn get_sink(&self) -> bool {
        self.input
    }

    pub fn step(&mut self) {
        self.output = self.input;
        self.input = false;
    }

    pub fn get_source(&self) -> bool {
        self.output
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_stays_unset() {
        let mut junction = Junction::default();
        junction.set(false);
        junction.step();
        assert!(!junction.get_source());
    }

    #[test]
    fn test_stays_set() {
        let mut junction = Junction::default();
        junction.set(true);
        junction.step();
        assert!(junction.get_source());
        junction.step();
        assert!(!junction.get_source());
        junction.set(true);
        junction.set(false);
        junction.step();
        assert!(junction.get_source());
    }
}
