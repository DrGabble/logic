use super::super::plan::{Sink, Source};

#[derive(Default, Clone)]
pub struct Transistor {
    power: bool,
    collector: bool,
    ground: bool,
    emitter: bool,
}

impl Transistor {
    pub fn set(&mut self, sink: &Sink, value: bool) {
        match sink {
            Sink::Power => self.power |= value,
            Sink::Collector => self.collector |= value,
        }
    }

    pub fn get_sink(&self, sink: &Sink) -> bool {
        match sink {
            Sink::Power => self.power,
            Sink::Collector => self.collector,
        }
    }

    // 4 binary operations to do 1 transistor update
    pub fn step(&mut self) {
        self.ground = self.power & !self.collector;
        self.emitter = self.power & self.collector;
        self.power = false;
        self.collector = false;
    }

    pub fn get_source(&self, source: &Source) -> bool {
        match source {
            Source::Ground => self.ground,
            Source::Emitter => self.emitter,
        }
    }
}

impl std::fmt::Debug for Transistor {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(
            f,
            "Power: {} Collector: {} Ground: {} Emitter: {}",
            self.power as usize,
            self.collector as usize,
            self.ground as usize,
            self.emitter as usize
        )
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_no_inputs() {
        let mut transistor = Transistor::default();
        transistor.step();
        assert!(!transistor.get_source(&Source::Ground));
        assert!(!transistor.get_source(&Source::Emitter));
    }

    #[test]
    fn test_just_power() {
        let mut transistor = Transistor::default();
        transistor.set(&Sink::Power, true);
        transistor.step();
        assert!(transistor.get_source(&Source::Ground));
        assert!(!transistor.get_source(&Source::Emitter));
    }

    #[test]
    fn test_just_collector() {
        let mut transistor = Transistor::default();
        transistor.set(&Sink::Collector, true);
        transistor.step();
        assert!(!transistor.get_source(&Source::Ground));
        assert!(!transistor.get_source(&Source::Emitter));
    }

    #[test]
    fn test_power_and_collector() {
        let mut transistor = Transistor::default();
        transistor.set(&Sink::Power, true);
        transistor.set(&Sink::Collector, true);
        transistor.step();
        assert!(!transistor.get_source(&Source::Ground));
        assert!(transistor.get_source(&Source::Emitter));
    }
}
