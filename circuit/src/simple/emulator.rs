use petgraph::graph::NodeIndex;

use super::super::emulator::Emulator;
use super::super::plan::{self, Pin, Plan, Sink, Source};
use super::super::plot::State;
use super::junction::Junction;
use super::transistor::Transistor;
use crate::device::Device;

pub struct SimpleEmulator {
    graph: petgraph::stable_graph::StableGraph<Node, (Source, Sink)>,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

impl<'a> Emulator<'a> for SimpleEmulator {}

impl<'a> From<&'a Plan> for SimpleEmulator {
    fn from(plan: &'a Plan) -> Self {
        Self {
            graph: plan.graph.map(
                |_, node| match node {
                    plan::Node::Transistor => Node::Transistor(Transistor::default()),
                    plan::Node::Junction => Node::Junction(Junction::default()),
                },
                |_, e| *e,
            ),
            sinks: plan.component.sinks.clone(),
            sources: plan.component.sources.clone(),
        }
    }
}

impl State for SimpleEmulator {
    fn node_type(&self, node_index: &NodeIndex) -> Option<plan::Node> {
        Some(match self.graph.node_weight(*node_index)? {
            Node::Transistor(_) => plan::Node::Transistor,
            Node::Junction(_) => plan::Node::Junction,
        })
    }

    fn sink_value(&self, node_index: &NodeIndex, sink: &Sink) -> Option<bool> {
        self.graph
            .node_weight(*node_index)
            .map(|n| n.get_sink(sink))
    }

    fn source_value(&self, node_index: &NodeIndex, source: &Source) -> Option<bool> {
        self.graph
            .node_weight(*node_index)
            .map(|n| n.get_source(source))
    }

    fn visit_edges(
        &self,
        source_index: &NodeIndex,
        visitor: &mut dyn FnMut(&Source, &Sink, &NodeIndex),
    ) {
        let mut neighbours = self.graph.neighbors(*source_index).detach();
        while let Some((edge, sink_index)) = neighbours.next(&self.graph) {
            let (source, sink) = self.graph.edge_weight(edge).unwrap();
            visitor(source, sink, &sink_index);
        }
    }
}

impl Device for SimpleEmulator {
    fn write(&mut self, sink: usize, value: bool) {
        let pin = self.sinks[sink];
        self.graph[pin.index()].set(pin.pin_type(), value);
    }

    fn step(&mut self) {
        // step all nodes
        // TODO annoyingly inefficent (allocation and copying of all nodes!)
        for node_index in self.graph.node_indices().collect::<Vec<_>>() {
            match self.graph.node_weight_mut(node_index).unwrap() {
                Node::Transistor(transistor) => transistor.step(),
                Node::Junction(junction) => junction.step(),
            }
        }
        // move signal between each node (step all edges)
        for edge in self.graph.edge_indices().collect::<Vec<_>>() {
            let (source_node, sink_node) = self.graph.edge_endpoints(edge).unwrap();
            // TODO big hole in logic here!!! Need to deal with this or always optimize aggressively
            //assert!(!(self.graph[source_node].is_junction() & self.graph[sink_node].is_junction()));
            let (source_output, sink_input) = self.graph.edge_weight(edge).unwrap();
            let (source_output, sink_input) = (*source_output, *sink_input);
            let value = self.graph[source_node].get_source(&source_output);
            self.graph[sink_node].set(&sink_input, value);
        }
    }

    fn read(&self, source: usize) -> bool {
        let pin = self.sources[source];
        self.graph[pin.index()].get_source(pin.pin_type())
    }

    fn reset(&mut self) {
        for node in self.graph.node_indices().collect::<Vec<_>>() {
            self.graph[node].reset();
        }
    }
}

//
// Node
//

enum Node {
    Transistor(Transistor),
    Junction(Junction),
}

impl Node {
    fn set(&mut self, sink: &Sink, value: bool) {
        match self {
            Node::Transistor(transistor) => transistor.set(sink, value),
            Node::Junction(junction) => junction.set(value),
        }
    }

    fn get_sink(&self, sink: &Sink) -> bool {
        match self {
            Node::Transistor(transistor) => transistor.get_sink(sink),
            Node::Junction(junction) => junction.get_sink(),
        }
    }

    fn get_source(&self, source: &Source) -> bool {
        match self {
            Node::Transistor(transistor) => transistor.get_source(source),
            Node::Junction(junction) => junction.get_source(),
        }
    }

    fn reset(&mut self) {
        match self {
            Node::Transistor(transistor) => {
                transistor.set(&Sink::Power, false);
                transistor.set(&Sink::Collector, false);
                transistor.step();
            }
            Node::Junction(junction) => {
                junction.set(false);
                junction.step();
            }
        }
    }
}
