use std::borrow::Cow;

use itertools::Itertools;
use petgraph::graph::NodeIndex;

use super::chip::{BuildChip, Chip, ChipBuilder};
use crate::pinout::NamedRange;

//
// Plan
//
// A detailed layout of a potential circuit board.
//

pub struct Plan {
    pub graph: Graph,
    pub component: Component,
}

impl Plan {
    pub fn from_chip(chip_builder: &dyn ChipBuilder, component_depth: usize) -> Self {
        let mut graph = Graph::new();
        let mut builder = Builder::new(&mut graph, component_depth);
        let chip = chip_builder.build_boxed_chip(&mut builder);
        let component = Component::new(builder, &*chip, chip_builder.name());
        Self { graph, component }
    }

    pub fn transistor_count(&self) -> usize {
        self.graph
            .node_indices()
            .filter_map(|i| self.graph.node_weight(i))
            .filter(|n| **n == Node::Transistor)
            .count()
    }

    pub fn junction_count(&self) -> usize {
        self.graph
            .node_indices()
            .filter_map(|i| self.graph.node_weight(i))
            .filter(|n| **n == Node::Junction)
            .count()
    }

    pub fn wire_count(&self) -> usize {
        self.graph.edge_count()
    }
}

pub type Graph = petgraph::stable_graph::StableGraph<Node, (Source, Sink)>;

//
// Node
//
#[derive(Hash, Eq, Debug, Copy, Clone, PartialEq)]
pub enum Node {
    Transistor,
    Junction,
}

//
// Source
//

#[derive(Ord, PartialOrd, Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum Source {
    Ground,
    Emitter,
}

//
// Sink
//

#[derive(Ord, PartialOrd, Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum Sink {
    Power,
    Collector,
}

//
// Pin
//

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
pub struct Pin<T> {
    index: NodeIndex,
    pin_type: T,
}

impl<T> Pin<T> {
    pub fn index(&self) -> NodeIndex {
        self.index
    }

    pub fn pin_type(&self) -> &T {
        &self.pin_type
    }
}

impl<'a, T: Clone> From<Pin<T>> for Cow<'a, Pin<T>> {
    fn from(pin: Pin<T>) -> Self {
        Self::Owned(pin)
    }
}

impl<'a, T: Clone> From<&'a Pin<T>> for Cow<'a, Pin<T>> {
    fn from(pin: &'a Pin<T>) -> Self {
        Self::Borrowed(pin)
    }
}

//
// Builder
//

pub struct Builder<'g> {
    graph: &'g mut Graph,
    component_depth: usize,
    sub_components: Vec<Component>,
    start: u32,
}

impl<'g> Builder<'g> {
    fn new(graph: &'g mut Graph, component_depth: usize) -> Self {
        Self {
            component_depth,
            sub_components: Vec::new(),
            start: graph.node_count() as u32,
            graph,
        }
    }

    pub fn new_chip_vec<C, B>(&mut self, chip_builder: &B, size: usize) -> Vec<C>
    where
        C: Chip + 'static,
        B: BuildChip<Output = C>,
    {
        (0..size).map(|_| self.new_chip(chip_builder)).collect()
    }

    pub fn new_chip<C, B>(&mut self, chip_builder: &B) -> C
    where
        C: Chip + 'static,
        B: BuildChip<Output = C>,
    {
        if self.component_depth == 0 {
            return chip_builder.build_chip(self);
        }
        let mut builder = Builder::new(self.graph, self.component_depth - 1);
        let chip = chip_builder.build_chip(&mut builder);
        self.sub_components
            .push(Component::new(builder, &chip, chip_builder.name()));
        chip
    }

    pub fn new_transistor(&mut self) -> Transistor {
        let index = self.graph.add_node(Node::Transistor);
        Transistor {
            inputs: [
                Pin {
                    index,
                    pin_type: Sink::Power,
                },
                Pin {
                    index,
                    pin_type: Sink::Collector,
                },
            ],
            outputs: [
                Pin {
                    index,
                    pin_type: Source::Ground,
                },
                Pin {
                    index,
                    pin_type: Source::Emitter,
                },
            ],
        }
    }

    pub fn new_junction(&mut self) -> Junction {
        let index = self.graph.add_node(Node::Junction);
        Junction {
            input: Pin {
                index,
                pin_type: Sink::Power,
            },
            output: Pin {
                index,
                pin_type: Source::Ground,
            },
        }
    }

    pub fn new_junction_vec(&mut self, size: usize) -> Vec<Junction> {
        (0..size).map(|_| self.new_junction()).collect()
    }

    pub fn join_all<'a, AIter, A, BIter, B>(&mut self, sources: AIter, sinks: BIter)
    where
        AIter: IntoIterator<Item = A>,
        A: Into<Cow<'a, Pin<Source>>>,
        BIter: IntoIterator<Item = B>,
        B: Into<Cow<'a, Pin<Sink>>>,
    {
        let mut sinks_iter = sinks.into_iter();
        for source in sources.into_iter() {
            if let Some(sink) = sinks_iter.next() {
                self.join(*source.into(), *sink.into());
            } else {
                panic!("Cannot join all: less sinks than sources");
            }
        }
        if sinks_iter.next().is_some() {
            panic!("Cannot join all: less sources than sinks");
        }
    }

    pub fn join(&mut self, source: Pin<Source>, sink: Pin<Sink>) {
        self.graph
            .add_edge(source.index, sink.index, (source.pin_type, sink.pin_type));
    }
}

//
// Transistor
//

#[derive(Clone)]
pub struct Transistor {
    inputs: [Pin<Sink>; 2],
    outputs: [Pin<Source>; 2],
}

impl Transistor {
    pub fn power(&self) -> Pin<Sink> {
        self.inputs[0]
    }

    pub fn collector(&self) -> Pin<Sink> {
        self.inputs[1]
    }

    pub fn ground(&self) -> Pin<Source> {
        self.outputs[0]
    }

    pub fn emitter(&self) -> Pin<Source> {
        self.outputs[1]
    }
}

//
// Junction
//
// Note that for junctions Sink::Collector == Sink::Power, and Source::Emitter == Source::Ground.
// A junction loop will result in a permenant "high" once set.
//

#[derive(Clone)]
pub struct Junction {
    input: Pin<Sink>,
    output: Pin<Source>,
}

impl Junction {
    pub fn input(&self) -> Pin<Sink> {
        self.input
    }

    pub fn output(&self) -> Pin<Source> {
        self.output
    }
}

//
// Component
//

#[derive(Clone)]
pub struct Component {
    pub name: String,
    pub labels: Vec<NamedRange>,
    pub sinks: Vec<Pin<Sink>>,
    pub sources: Vec<Pin<Source>>,
    pub sub_components: Vec<Component>,
    pub nodes: std::ops::Range<u32>,
}

impl Component {
    fn new(builder: Builder, chip: &dyn Chip, name: String) -> Self {
        Self {
            sinks: chip.sinks(),
            sources: chip.sources(),
            name,
            labels: chip.labels(),
            nodes: builder.start..(builder.graph.node_count() as u32),
            sub_components: builder.sub_components,
        }
    }

    pub fn nodes(&self) -> impl Iterator<Item = u32> + '_ {
        std::iter::once(self.nodes.start..self.nodes.start)
            .chain(self.sub_components.iter().map(|c| c.nodes.clone()))
            .chain(std::iter::once(self.nodes.end..self.nodes.end))
            .tuple_windows()
            .flat_map(|(start, end)| start.end..end.start)
    }

    pub fn transputs_recursive(&self) -> Box<dyn Iterator<Item = NodeIndex> + '_> {
        let sinks = self.sinks.iter().map(|p| p.index());
        let sources = self.sources.iter().map(|p| p.index());
        let children = self
            .sub_components
            .iter()
            .flat_map(|c| c.transputs_recursive());
        Box::new(sinks.chain(sources).chain(children))
    }
}
