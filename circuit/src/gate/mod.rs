mod adder;
mod basic;
mod bitshift;
mod compare;
mod counter;
mod decoder;
mod flipflop;
mod memory;
mod multiplier;
mod mux;
mod subtract;

pub use adder::{FullAdder, Increment, RippleAdder, Xnor, Xor};
pub use basic::ORChip;
pub use basic::{And, Basic, Bitwise, Inverted, Nand, Nor, Not, OR};
pub use bitshift::{BarrelShift, FixedShift, ShiftType};
pub use compare::{Comparator, EqualHardCoded, HardCoded};
pub use counter::{RippleCounter, SyncCounter};
pub use decoder::{Decoder, Encoder, PriorityEncoder, Switch};
pub use flipflop::{FallingEdgeLatch, GatedLatch, JkMasterSlave, SetResetLatch};
pub use memory::{Ram, Word};
pub use multiplier::DadaMultiplier;
pub use mux::HalfDemultiplexerChip;
pub use mux::{Demultiplexer, HalfDemultiplexer, HalfMultiplexer, Multiplexer};
pub use subtract::{Decrement, HalfSubtractor, Negate};

macro_rules! builders {
    ($($label:expr => $builder:ty),*) => {
        pub fn find_builder(name: &str) -> Result<Box<dyn super::chip::ChipBuilder>, String> {
            match name {
                $(
                    $label => Ok(Box::<$builder>::default()),
                )*
                _ => return Err(format!("Gate name '{}' not found", name)),
            }
        }

        pub fn builder_labels() -> Vec<&'static str> {
            vec![$($label,)*]
        }
    };
}

builders!(
    "not" => Not,
    "or" => OR,
    "and" => And,
    "nor" => Nor,
    "nand" => Nand,
    "xor" => Xor,
    "halfsubtract" => HalfSubtractor,
    "decrement" => Decrement,
    "fulladder" => FullAdder,
    "increment" => Increment,
    "rippleadder" => RippleAdder,
    "decoder" => Decoder,
    "encoder" => Encoder,
    "priorityencoder" => PriorityEncoder,
    "mux" => Multiplexer,
    "demux" => Demultiplexer,
    "jkmasterslave" => JkMasterSlave,
    "srlatch" => SetResetLatch,
    "edgelatch" => FallingEdgeLatch,
    "gatedlatch" => GatedLatch,
    "word" => Word,
    "ram" => Ram,
    "ripplecounter" => RippleCounter,
    "synccounter" => SyncCounter,
    "compare" => Comparator,
    "multiplier" => DadaMultiplier,
    "bitwiseand" => Bitwise::<And>,
    "fixedshift" => FixedShift,
    "barrelshift" => BarrelShift,
    "negate" => Negate,
    "condition" => crate::cpu::Condition
    //"opdecode" => crate::circuit::cpu::OpDecode
);
