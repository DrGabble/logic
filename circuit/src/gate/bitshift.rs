use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Junction, Pin, Sink, Source};
use crate::pinout::NamedRange;

use super::mux;

//
// Barrel Shift
//
// Uses layers of multiplexers to shift input by varying amounts.
// https://www.d.umn.edu/~gshute/logic/barrel-shifter.xhtml
//

pub struct BarrelShiftChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(BarrelShiftChip;
    sink power |_: &BarrelShiftChip| 0,
    sinks shift |c: &BarrelShiftChip| 1..(c.sinks.len() - c.sources.len()),
    sinks inputs |c: &BarrelShiftChip| (c.sinks.len() - c.sources.len())..c.sinks.len(),
    sources outputs |c: &BarrelShiftChip| 0..c.sources.len()
);

derive_builder!(
    BarrelShift;
    bits: usize = 4;
    shift_type: ShiftType = ShiftType::Logical
);

impl BuildChip for BarrelShift {
    type Output = BarrelShiftChip;

    fn name(&self) -> String {
        "Barrel Bit Shift".to_string()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power_junction = builder.new_junction();

        // build final layer of outputs
        let mut last_layer_inputs = Vec::with_capacity(self.bits);
        let mut sources = Vec::with_capacity(self.bits);

        for _ in 0..self.bits {
            let junction = builder.new_junction();
            last_layer_inputs.push(junction.input());
            sources.push(junction.output());
        }

        // work backwards from final layer to first layer, to reduce itermediary junction count
        let mut shift_amount = self.bits.next_power_of_two() >> 1;
        let mut shift_address = Vec::new();
        while shift_amount > 0 {
            // dmultiplexer, switching to either the shifter, or to skip it
            let demux = builder.new_chip(&mux::Demultiplexer::new(1, self.bits));
            builder.join(power_junction.output(), demux.power());
            shift_address.push(demux.address()[0]);
            builder.join_all(demux.outputs(0), last_layer_inputs.iter());

            // bit shifter, in decreasing powers of two
            let shifter = builder.new_chip(&FixedShift::new(
                self.bits,
                self.shift_type.clone(),
                shift_amount as isize,
            ));
            builder.join_all(demux.outputs(1), shifter.inputs());
            builder.join_all(shifter.outputs(), last_layer_inputs.iter());

            // save inputs for the next layer (re-use vector)
            last_layer_inputs.clear();
            last_layer_inputs.extend(demux.inputs().iter().cloned());
            shift_amount >>= 1;
        }

        let power = std::iter::once(power_junction.input());
        let shifts = shift_address.into_iter().rev();
        let inputs = last_layer_inputs.iter().cloned();
        let sinks = power.chain(shifts).chain(inputs).collect();

        Self::Output { sinks, sources }
    }
}

//
// Fixed Bit Shift
//
// Shifts by a fixed amount (by simply cross wiring as desired)
//

pub struct FixedShiftChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(FixedShiftChip;
    sinks inputs |c: &FixedShiftChip| 0..c.sinks.len(),
    sources outputs |c: &FixedShiftChip| 0..c.sources.len()
);

derive_builder!(FixedShift;
    bits: usize = 8;
    shift_type: ShiftType = ShiftType::Logical;
    shift: isize = 1
);

impl BuildChip for FixedShift {
    type Output = FixedShiftChip;

    fn name(&self) -> String {
        match self.shift_type {
            ShiftType::Rotate => "Rotation Bit Shift",
            ShiftType::Logical => "Logical Bit Shift",
            ShiftType::Arithmetic => "Arithmetic Bit Shift",
        }
        .to_string()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let junctions: Vec<_> = builder.new_junction_vec(self.bits);
        let sinks = self.shift_type.apply(builder, &junctions, self.shift);
        let sources = junctions.iter().map(|j| j.output()).collect();
        FixedShiftChip { sinks, sources }
    }
}

//
// Shift Type
//

#[derive(Clone)]
pub enum ShiftType {
    Rotate,
    Logical,
    Arithmetic, // Most significant bit is sign, so is preserved
}

impl ShiftType {
    fn apply(&self, builder: &mut Builder, junctions: &[Junction], shift: isize) -> Vec<Pin<Sink>> {
        match self {
            Self::Rotate => Self::apply_rotate(junctions, shift),
            Self::Arithmetic => Self::apply_arithmetic(builder, junctions, shift),
            Self::Logical => Self::apply_logical(builder, junctions, shift),
        }
    }

    fn apply_rotate(junctions: &[Junction], shift: isize) -> Vec<Pin<Sink>> {
        (0..junctions.len())
            .map(|i| {
                let mut index = (i as isize + shift) % (junctions.len() as isize);
                if index < 0 {
                    index += junctions.len() as isize;
                }
                println!("DEBUG REMOVE {} {} = {}", i, shift, index);
                junctions[index as usize].input()
            })
            .collect()
    }

    fn apply_arithmetic(
        builder: &mut Builder,
        junctions: &[Junction],
        shift: isize,
    ) -> Vec<Pin<Sink>> {
        let mut sinks = Self::apply_logical(builder, &junctions[0..junctions.len() - 1], shift);
        sinks.push(junctions.last().unwrap().input());
        sinks
    }

    fn apply_logical(
        builder: &mut Builder,
        junctions: &[Junction],
        shift: isize,
    ) -> Vec<Pin<Sink>> {
        (0..junctions.len())
            .map(|i| {
                let index = i as isize + shift;
                if 0 <= index && (index as usize) < junctions.len() {
                    junctions[index as usize].input()
                } else {
                    builder.new_junction().input()
                }
            })
            .collect()
    }
}

impl std::str::FromStr for ShiftType {
    type Err = String;

    fn from_str(string: &str) -> Result<Self, Self::Err> {
        match string {
            "rotate" => Ok(Self::Rotate),
            "logical" => Ok(Self::Logical),
            "arithmetic" => Ok(Self::Arithmetic),
            _ => Err("Not a valid Shift Type".into()),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_barrel_logical_shift() {
        test_chip!(BarrelShift::new(8, ShiftType::Logical);
            ("inputs": 0b00001101), ("shift": 0) => ("outputs": 0b00001101);
            ("inputs": 0b00001101), ("shift": 1) => ("outputs": 0b00011010);
            ("inputs": 0b00001101), ("shift": 3) => ("outputs": 0b01101000);
            ("inputs": 0b00001101), ("shift": 4) => ("outputs": 0b11010000);
            ("inputs": 0b00001101), ("shift": 7) => ("outputs": 0b10000000)
        );
    }

    #[test]
    fn test_logical_shift() {
        test_chip!(FixedShift::new(8, ShiftType::Logical, -3);
            "01001100" => "01100000";
            "11111111" => "11111000"
        );
        test_chip!(FixedShift::new(8, ShiftType::Logical, 2);
            "01001100" => "00010011";
            "11111111" => "00111111"
        );
        test_chip!(FixedShift::new(8, ShiftType::Logical, 0);
            "01001100" => "01001100";
            "11111111" => "11111111"
        );
    }

    #[test]
    fn test_arithmetic_shift() {
        test_chip!(FixedShift::new(8, ShiftType::Arithmetic, -3);
            "01001100" => "01100000";
            "11111111" => "11110001"
        );
        test_chip!(FixedShift::new(8, ShiftType::Arithmetic, 2);
            "01001100" => "00010010";
            "11111111" => "00111111"
        );
        test_chip!(FixedShift::new(8, ShiftType::Arithmetic, 0);
            "01001100" => "01001100";
            "11111111" => "11111111"
        );
    }

    #[test]
    fn test_rotate_shift() {
        test_chip!(FixedShift::new(8, ShiftType::Rotate, -3);
            "01001100" => "01100010";
            "11111111" => "11111111"
        );
        test_chip!(FixedShift::new(8, ShiftType::Rotate, 2);
            "01001100" => "00010011";
            "11111111" => "11111111"
        );
        test_chip!(FixedShift::new(8, ShiftType::Rotate, 0);
            "01001100" => "01001100";
            "11111111" => "11111111"
        );
    }
}
