use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic, Not};
use super::adder::Increment;
use crate::pinout::NamedRange;

use super::adder::Xor;

//
// Two's Complement Negate
//

pub struct NegateChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>
}

derive_chip!(
    NegateChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    sources outputs |c: &Self| 0..c.sources.len()
);

derive_builder!(Negate; bits: usize = 8);

impl BuildChip for Negate {
    type Output = NegateChip;

    fn name(&self) -> String {
        "Negate".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power = builder.new_junction();
        let mut sinks = vec![power.input()];
        let increment = builder.new_chip(&Increment::new(self.bits));
        builder.join(power.output(), increment.power());
        for inc_input in increment.inputs() {
            let not = builder.new_chip(&Not::new());
            sinks.push(not.input());
            builder.join(power.output(), not.power());
            builder.join(not.output(), *inc_input);
        }
        NegateChip { sinks, sources: increment.outputs().to_vec() }
    }
}

//
// Decrementer
//

pub struct DecrementChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(
    DecrementChip;
    sink power |_: &Self| 0,
    sinks input |c: &Self| 1..c.sinks.len(),
    sources output |c: &Self| 0..(c.sources.len()-1),
    source underflow |c: &Self| c.sources.len() - 1
);

derive_builder!(Decrement; bits: usize = 2);

impl BuildChip for Decrement {
    type Output = DecrementChip;

    fn name(&self) -> String {
        "Decrement".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut sinks = Vec::with_capacity(1 + self.bits);
        let mut sources = Vec::with_capacity(self.bits + 1);

        let power = builder.new_junction();
        sinks.push(power.input());
        let mut borrow = power.output();

        for _ in 0..self.bits {
            let half_subtractor = builder.new_chip(&HalfSubtractor::new());
            builder.join(power.output(), half_subtractor.power());
            sinks.push(half_subtractor.inputs()[0]);
            builder.join(borrow, half_subtractor.inputs()[1]);
            sources.push(half_subtractor.diff());
            borrow = half_subtractor.borrow();
        }

        sources.push(borrow);

        Self::Output { sinks, sources }
    }
}

//
// Half Subtractor
//

pub struct HalfSubtractorChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(
    HalfSubtractorChip;
    sink power |_: &Self| 0,
    sinks inputs |_: &Self| 1..3,
    source diff |_: &Self| 0,
    source borrow |_: &Self| 1
);

derive_builder!(HalfSubtractor);

impl BuildChip for HalfSubtractor {
    type Output = HalfSubtractorChip;

    fn name(&self) -> String {
        "Half Subtractor".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let alpha_in = builder.new_junction();
        let beta_in = builder.new_junction();

        let diff_xor = builder.new_chip(&Xor::new());
        builder.join(alpha_in.output(), diff_xor.inputs()[0]);
        builder.join(beta_in.output(), diff_xor.inputs()[1]);

        let not_alpha = builder.new_chip(&Not::new());
        builder.join(alpha_in.output(), not_alpha.input());

        let borrow_and = builder.new_chip(&And::new(2));
        builder.join(not_alpha.output(), borrow_and.inputs()[0]);
        builder.join(beta_in.output(), borrow_and.inputs()[1]);

        Self::Output {
            sinks: [not_alpha.power(), alpha_in.input(), beta_in.input()],
            sources: [diff_xor.output(), borrow_and.output()],
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_twos_compliment_negate() {
        let bits = 4;
        let max = 1 << (bits-1);
        for input in 0..max {
            let output = !input + 1;
            test_chip!(Negate::new(bits); ("inputs": output) => ("outputs": input));
        }
    }

    #[test]
    fn test_decrement() {
        test_chip!(Decrement::new(8);
            ("input": 0) => ("output": 255), ("underflow" 0: true);
            ("input": 3) => ("output": 2), ("underflow" 0: false);
            ("input": 255) => ("output": 254), ("underflow" 0: false)
        );
    }

    #[test]
    pub fn test_half_subtractor() {
        test_chip!(HalfSubtractor::new();
            ("inputs" 0: false), ("inputs" 1: false) => ("diff": 0), ("borrow": 0);
            ("inputs" 0: false), ("inputs" 1: true) => ("diff": 1), ("borrow": 1);
            ("inputs" 0: true), ("inputs" 1: false) => ("diff": 1), ("borrow": 0);
            ("inputs" 0: true), ("inputs" 1: true) => ("diff": 0), ("borrow": 0)
        );
    }
}
