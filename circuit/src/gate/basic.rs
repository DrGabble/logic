use std::collections::VecDeque;

use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use crate::pinout::NamedRange;

//
// Basic Trait
//
// The basic trait is for gates with generic inputs and one output.
//

pub trait Basic {
    fn inputs(&self) -> &[Pin<Sink>];
    fn output(&self) -> Pin<Source>;
}

//
// Bitwise
//
// Applies a basic gate in each pair of bits from two multibyte values.
//

pub struct Bitwise<B> {
    basic_builder: B,
    number: usize,
}

impl<B> Bitwise<B> {
    pub fn new(basic_builder: B, number: usize) -> Self {
        Self {
            basic_builder,
            number,
        }
    }
}

impl<B: Default> Default for Bitwise<B> {
    fn default() -> Self {
        Self {
            basic_builder: B::default(),
            number: 4,
        }
    }
}

impl<C, B> ChipBuilder for Bitwise<B>
where
    C: Chip + Basic + 'static,
    B: ChipBuilder + BuildChip<Output = C> + 'static,
{
    fn name(&self) -> String {
        <Self as BuildChip>::name(self)
    }

    fn fields(&self) -> Vec<String> {
        let mut fields = self.basic_builder.fields();
        fields.push("number".into());
        fields
    }

    fn set_field(&mut self, name: &str, value: &str) -> Result<(), String> {
        if self.basic_builder.set_field(name, value).is_ok() {
            return Ok(());
        }
        parse_fields!(self, name, value, number);
        Ok(())
    }

    fn build_boxed_chip(&self, builder: &mut Builder) -> Box<dyn Chip> {
        Box::new(self.build_chip(builder))
    }
}

impl<C, B> BuildChip for Bitwise<B>
where
    C: Chip + Basic + 'static,
    B: BuildChip<Output = C> + 'static,
{
    type Output = BitwiseChip;

    fn name(&self) -> String {
        format!("Bitwise {}", &self.basic_builder.name())
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut alpha_in = Vec::new();
        let mut beta_in = Vec::new();
        let mut sources = Vec::new();
        for _ in 0..self.number {
            let basic_chip = builder.new_chip(&self.basic_builder);
            assert!(basic_chip.inputs().len() == 2);
            alpha_in.push(basic_chip.inputs()[0]);
            beta_in.push(basic_chip.inputs()[1]);
            sources.push(basic_chip.output());
        }
        let mut sinks = alpha_in;
        sinks.append(&mut beta_in);
        Self::Output { sinks, sources }
    }
}

pub struct BitwiseChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(
    BitwiseChip;
    sinks alpha_in |c: &Self| 0..(c.sinks.len()/2),
    sinks beta_in |c: &Self| (c.sinks.len()/2)..c.sinks.len(),
    sources outputs |c: &Self| 0..c.sources.len()
);

//
// Inverted
//
// Inverts the output of a Basic by running it through a Not gate.
//

pub struct Inverted<B> {
    basic_builder: B,
}

impl<B: Default> Default for Inverted<B> {
    fn default() -> Self {
        Self {
            basic_builder: B::default(),
        }
    }
}

impl<B> Inverted<B> {
    pub fn new(basic_builder: B) -> Self {
        Self { basic_builder }
    }
}

impl<C, B> ChipBuilder for Inverted<B>
where
    C: Chip + Basic + 'static,
    B: ChipBuilder + BuildChip<Output = C> + 'static,
{
    fn name(&self) -> String {
        format!("Not {}", <Self as BuildChip>::name(self))
    }

    fn fields(&self) -> Vec<String> {
        self.basic_builder.fields()
    }

    fn set_field(&mut self, name: &str, value: &str) -> Result<(), String> {
        self.basic_builder.set_field(name, value)
    }

    fn build_boxed_chip(&self, builder: &mut Builder) -> Box<dyn Chip> {
        Box::new(self.build_chip(builder))
    }
}

impl<C, B> BuildChip for Inverted<B>
where
    C: Chip + Basic + 'static,
    B: BuildChip<Output = C> + 'static,
{
    type Output = InvertedChip;

    fn name(&self) -> String {
        format!("Not {}", self.basic_builder.name())
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let basic = builder.new_chip(&self.basic_builder);
        let not = builder.new_chip(&Not::new());
        builder.join(basic.output(), not.input());
        Self::Output {
            sinks: std::iter::once(not.power())
                .chain(basic.inputs().iter().cloned())
                .collect(),
            sources: [not.output()],
        }
    }
}

pub struct InvertedChip {
    sinks: Vec<Pin<Sink>>,
    sources: [Pin<Source>; 1],
}

derive_chip!(
    InvertedChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    source output |_: &Self| 0
);

//
// Not Gate (inverts signal)
//

derive_builder!(Not);

impl BuildChip for Not {
    type Output = NotChip;

    fn name(&self) -> String {
        "Not".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let transistor = builder.new_transistor();
        Self::Output {
            sinks: [transistor.power(), transistor.collector()],
            source: transistor.ground(),
        }
    }
}

pub struct NotChip {
    sinks: [Pin<Sink>; 2],
    source: Pin<Source>,
}

impl NotChip {
    pub fn power(&self) -> Pin<Sink> {
        self.sinks[0]
    }

    pub fn input(&self) -> Pin<Sink> {
        self.sinks[1]
    }

    pub fn output(&self) -> Pin<Source> {
        self.source
    }
}

impl Chip for NotChip {
    fn labels(&self) -> Vec<NamedRange> {
        labels!(sink power 0, sink input 1, source output 0)
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.to_vec()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        vec![self.source]
    }
}

//
// And Gate (true if all inputs are true)
//
// In the non trivial case, created from a tree of Transistors.
//

derive_builder!(And; inputs: usize = 2);

impl BuildChip for And {
    type Output = ANDChip;

    fn name(&self) -> String {
        "And".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // handle trivial case
        if self.inputs < 2 {
            let junction = builder.new_junction();
            let sinks = if self.inputs == 1 {
                vec![junction.input()]
            } else {
                Vec::new()
            };
            return Self::Output {
                sinks,
                source: junction.output(),
            };
        }

        // non trivial case
        let mut transistor = builder.new_transistor();
        let source = transistor.emitter();
        let mut inputs = VecDeque::with_capacity(self.inputs);
        inputs.push_back(transistor.power());
        inputs.push_back(transistor.collector());

        while inputs.len() < self.inputs {
            transistor = builder.new_transistor();
            builder.join(transistor.emitter(), inputs.pop_front().unwrap());
            inputs.push_back(transistor.power());
            inputs.push_back(transistor.collector());
        }

        Self::Output {
            sinks: inputs.into_iter().collect(),
            source,
        }
    }
}

pub struct ANDChip {
    sinks: Vec<Pin<Sink>>,
    source: Pin<Source>,
}

impl Basic for ANDChip {
    fn inputs(&self) -> &[Pin<Sink>] {
        &self.sinks
    }

    fn output(&self) -> Pin<Source> {
        self.source
    }
}

impl Chip for ANDChip {
    fn labels(&self) -> Vec<NamedRange> {
        labels!(sinks input 0..self.sinks.len(), source output 0)
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        vec![self.source]
    }
}

//
// Nand (Inverted And Gate)
//

pub type Nand = Inverted<And>;

//
// Or Gate (true if any input is true)
//

derive_builder!(OR; inputs: usize = 2);

impl BuildChip for OR {
    type Output = ORChip;

    fn name(&self) -> String {
        "Or".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let junction = builder.new_junction();
        Self::Output {
            sinks: (0..self.inputs).map(|_| junction.input()).collect(),
            source: junction.output(),
        }
    }
}

pub struct ORChip {
    sinks: Vec<Pin<Sink>>,
    source: Pin<Source>,
}

impl Chip for ORChip {
    fn labels(&self) -> Vec<NamedRange> {
        labels!(
            sinks inputs 0..self.sinks.len(),
            source output 0
        )
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        vec![self.source]
    }
}

impl Basic for ORChip {
    fn inputs(&self) -> &[Pin<Sink>] {
        &self.sinks
    }

    fn output(&self) -> Pin<Source> {
        self.source
    }
}

//
// Nor (Inverted Or)
//

pub type Nor = Inverted<OR>;

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_not() {
        test_chip!(Not::new();
            ("power": 1), ("input": 0) => "1";
            ("power": 1), ("input": 1) => "0";
            ("power": 0), ("input": 0) => "0";
            ("power": 0), ("input": 1) => "0"
        );
    }

    #[test]
    pub fn test_binary_and() {
        test_chip!(And::new(2);
            "00" => "0";
            "01" => "0";
            "10" => "0";
            "11" => "1"
        );
    }

    #[test]
    pub fn test_trinary_and() {
        test_chip!(And::new(3);
            "000" => "0";
            "001" => "0";
            "010" => "0";
            "011" => "0";
            "100" => "0";
            "101" => "0";
            "110" => "0";
            "111" => "1"
        );
    }

    #[test]
    pub fn test_binary_nand() {
        test_chip!(Nand::new(And::new(2));
            ("inputs": 0b00) => "1";
            ("inputs": 0b01) => "1";
            ("inputs": 0b10) => "1";
            ("inputs": 0b11) => "0"
        );
    }

    #[test]
    pub fn test_binary_or() {
        test_chip!(OR::new(2);
            "00" => "0";
            "01" => "1";
            "10" => "1";
            "11" => "1"
        );
    }

    #[test]
    pub fn test_trinary_or() {
        test_chip!(OR::new(3);
            "000" => "0";
            "001" => "1";
            "010" => "1";
            "011" => "1";
            "100" => "1";
            "101" => "1";
            "110" => "1";
            "111" => "1"
        );
    }
}
