use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic};
use crate::pinout::NamedRange;

use super::decoder::Decoder;
use super::flipflop::FallingEdgeLatch;

//
// Random Access Memory
//
// Holds 2 ^ ADDRESS_BITS words of memory (each with the value 0 to 2 ^ DATA_BITS).
// Address sets both the read output, and the write input.
// Value will only be overwritten when write pin is set high.
//

derive_builder!(Ram; address_bits: usize = 2; data_bits: usize = 2);

impl BuildChip for Ram {
    type Output = RamChip;

    fn name(&self) -> String {
        "Ram".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power_junction = builder.new_junction();
        let clock_junction = builder.new_junction();
        let data_in = builder.new_junction_vec(self.data_bits);
        let data_out = builder.new_junction_vec(self.data_bits);

        // address decoder
        // TODO replace with HalfDemultiplexer
        let decoder = builder.new_chip(&Decoder::new(self.address_bits));
        builder.join(power_junction.output(), decoder.power());

        for select in decoder.outputs().iter().cloned() {
            let word = builder.new_chip(&Word::new(self.data_bits));

            // power
            builder.join(power_junction.output(), word.power());

            // data in
            for (junction, input) in data_in.iter().zip(word.inputs().iter().cloned()) {
                builder.join(junction.output(), input);
            }

            // clock control input
            let clock_and = builder.new_chip(&And::new(2));
            builder.join(clock_junction.output(), clock_and.inputs()[0]);
            builder.join(select, clock_and.inputs()[1]);
            builder.join(clock_and.output(), word.clock());

            // data out
            for (output, junction) in word.outputs().iter().cloned().zip(data_out.iter()) {
                let read_and = builder.new_chip(&And::new(2));
                builder.join(select, read_and.inputs()[0]);
                builder.join(output, read_and.inputs()[1]);
                builder.join(read_and.output(), junction.input());
            }
        }

        let mut sinks = Vec::with_capacity(2 + self.address_bits + self.data_bits);
        sinks.push(power_junction.input());
        sinks.push(clock_junction.input());
        sinks.extend(decoder.inputs().iter().cloned());
        sinks.extend(data_in.iter().map(|d| d.input()));

        let sources: Vec<_> = data_out.iter().map(|d| d.output()).collect();

        Self::Output { sinks, sources }
    }
}

pub struct RamChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(RamChip;
    sink power |_: &Self| 0,
    sink clock |_: &Self| 1,
    sinks address |c: &Self| 2..(c.sinks.len() - c.sources.len()),
    sinks data_in |c: &Self| {
        (c.sinks.len() - c.sources.len())..c.sinks.len()
    },
    sources data_out |c: &Self| 0..c.sources.len()
);

//
// Word
//
// A word of N bits of memory.
// Set from input on falling edge of clock.
//

derive_builder!(Word; bits: usize = 2);

impl BuildChip for Word {
    type Output = WordChip;

    fn name(&self) -> String {
        "Word".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut sinks = Vec::with_capacity(2 + self.bits);
        let mut sources = Vec::with_capacity(self.bits);

        // input junctions
        let power_junction = builder.new_junction();
        sinks.push(power_junction.input());

        let clock_junction = builder.new_junction();
        sinks.push(clock_junction.input());

        for _ in 0..self.bits {
            let gated_latch = builder.new_chip(&FallingEdgeLatch::new());
            builder.join(power_junction.output(), gated_latch.power());
            builder.join(clock_junction.output(), gated_latch.clock());
            sinks.push(gated_latch.input());
            sources.push(gated_latch.is_set());
        }

        Self::Output { sinks, sources }
    }
}

pub struct WordChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(WordChip;
    sink power |_: &Self| 0,
    sink clock |_: &Self| 1,
    sinks inputs |c: &Self| 2..c.sinks.len(),
    sources outputs |c: &Self| 0..c.sources.len()
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_ram() {
        // 4 x 8bit words
        let mut device = test_device!(Ram::new(2, 8));
        let gate_delay = 100;
        // set address 3 to value 69
        test_suite!(
            device; clock gate_delay/4; period gate_delay;
            ("address": 3), ("data_in": 69) => ("data_out": 69)
        )
        .run(&mut device);
        // value stays at 69 when no clock
        // (clock high so it doesnt interpret clock=0 as "falling edge" and set to zero)
        test_suite!(
            device; period gate_delay;
            ("clock": 1), ("address": 3) => ("data_out": 69)
        )
        .run(&mut device);
        // other words unaffected
        test_suite!(
            device; clock gate_delay/4; period gate_delay;
            ("address": 0) => ("data_out": 0)
        )
        .run(&mut device);
    }

    #[test]
    pub fn test_word() {
        let mut device = test_device!(Word::new(8));
        let gate_delay = 100;
        test_suite!(
            device; clock gate_delay/4; period gate_delay;
            ("inputs": 27) => ("outputs": 27);
            ("inputs": 9) => ("outputs": 9)
        )
        .run(&mut device);
    }
}
