use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic, OR};
use super::decoder::{Decoder, Switch};
use crate::pinout::NamedRange;

//
// Multiplexer
//
// A digital "switch" for routing one of the inputs to the output.
//

derive_builder!(Multiplexer; address_bits: usize = 2; data_bits: usize = 2);

impl BuildChip for Multiplexer {
    type Output = MultiplexerChip;

    fn name(&self) -> String {
        "Multiplexer".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let num_inputs = 1 << self.address_bits;
        let decoder = builder.new_chip(&Decoder::new(self.address_bits));

        let mut sinks = Vec::with_capacity(1 + self.address_bits + num_inputs * self.data_bits);
        sinks.push(decoder.power());
        sinks.extend(decoder.inputs().iter().cloned());

        let half_mux = builder.new_chip(&HalfMultiplexer::new(num_inputs, self.data_bits));
        builder.join_all(decoder.outputs(), half_mux.select());

        for input in 0..num_inputs {
            sinks.extend(half_mux.inputs(input));
        }

        Self::Output {
            address_bits: self.address_bits,
            sinks,
            sources: half_mux.outputs().to_vec(),
        }
    }
}

pub struct MultiplexerChip {
    address_bits: usize,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

impl MultiplexerChip {
    pub fn power(&self) -> Pin<Sink> {
        self.sinks[0]
    }

    pub fn address(&self) -> &[Pin<Sink>] {
        &self.sinks[1..(self.address_bits + 1)]
    }

    pub fn inputs(&self, index: usize) -> &[Pin<Sink>] {
        let data_bits = self.sources.len();
        let offset = 1 + self.address_bits;
        let start = offset + index * data_bits;
        let end = start + data_bits;
        &self.sinks[start..end]
    }

    pub fn outputs(&self) -> &[Pin<Source>] {
        &self.sources
    }
}

impl Chip for MultiplexerChip {
    fn labels(&self) -> Vec<NamedRange> {
        let mut labels = vec![
            named_range!(sink power 0),
            named_range!(sinks address 1..(self.address_bits+1)),
        ];
        let data_bits = self.sources.len();
        let offset = 1 + self.address_bits;
        let num_inputs = (self.sinks.len() - offset) / data_bits;
        for input in 0..num_inputs {
            let start = offset + input * data_bits;
            let end = start + data_bits;
            labels.push(NamedRange::sink(format!("input_{}", input), start..end));
        }
        labels.push(named_range!(sources output 0..data_bits));
        labels
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        self.sources.clone()
    }
}

//
// Half Multiplexer
//
// A digital "switch" for routing one (or more) of the inputs to the output.
// Each output has a corresponding select input bit to enable it.
//

derive_builder!(HalfMultiplexer; num_inputs: usize = 2; data_bits: usize = 2);

impl BuildChip for HalfMultiplexer {
    type Output = HalfMultiplexerChip;

    fn name(&self) -> String {
        "Half Multiplexer".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let input_junctions = builder.new_junction_vec(self.num_inputs);
        let output_ors = builder.new_chip_vec(&OR::new(self.num_inputs), self.data_bits);
        let mut sinks = Vec::with_capacity(self.num_inputs * (1 + self.data_bits));
        sinks.extend(input_junctions.iter().map(|j| j.input()));

        for (input_index, input) in input_junctions.iter().enumerate() {
            for or in output_ors.iter() {
                let and = builder.new_chip(&And::new(2));
                sinks.push(and.inputs()[0]);
                builder.join(input.output(), and.inputs()[1]);
                builder.join(and.output(), or.inputs()[input_index]);
            }
        }

        let sources = output_ors.iter().map(|o| o.output()).collect();

        Self::Output {
            num_inputs: self.num_inputs,
            sinks,
            sources,
        }
    }
}

pub struct HalfMultiplexerChip {
    num_inputs: usize,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

impl HalfMultiplexerChip {
    pub fn select(&self) -> &[Pin<Sink>] {
        &self.sinks[..self.num_inputs]
    }

    pub fn inputs(&self, index: usize) -> &[Pin<Sink>] {
        let data_bits = self.sources.len();
        let offset = self.num_inputs;
        let start = offset + index * data_bits;
        let end = start + data_bits;
        &self.sinks[start..end]
    }

    pub fn outputs(&self) -> &[Pin<Source>] {
        &self.sources
    }
}

impl Chip for HalfMultiplexerChip {
    fn labels(&self) -> Vec<NamedRange> {
        let mut labels = vec![named_range!(sinks select 0..self.num_inputs)];
        let data_bits = self.sources.len();
        let offset = self.num_inputs;
        let num_inputs = (self.sinks.len() - offset) / data_bits;
        for input in 0..num_inputs {
            let start = offset + input * data_bits;
            let end = start + data_bits;
            labels.push(NamedRange::sink(format!("input_{}", input), start..end));
        }
        labels.push(named_range!(sources output 0..data_bits));
        labels
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        self.sources.clone()
    }
}

//
// Demultiplexer
//
// A digital "switch" for routing the input to one of the outputs.
//

derive_builder!(Demultiplexer; address_bits: usize = 2; data_bits: usize = 2);

impl BuildChip for Demultiplexer {
    type Output = DemultiplexerChip;

    fn name(&self) -> String {
        "Demultiplexer".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let num_outputs = 1 << self.address_bits;
        let decoder = builder.new_chip(&Decoder::new(self.address_bits));

        let half_demux = builder.new_chip(&HalfDemultiplexer::new(num_outputs, self.data_bits));
        builder.join_all(decoder.outputs(), half_demux.select());

        let mut sinks = Vec::with_capacity(1 + self.address_bits + self.data_bits);
        sinks.push(decoder.power());
        sinks.extend(decoder.inputs().iter().cloned());
        sinks.extend(half_demux.inputs());

        let sources = (0..num_outputs)
            .flat_map(|i| half_demux.outputs(i).iter())
            .cloned()
            .collect();

        Self::Output {
            address_bits: self.address_bits,
            sinks,
            sources,
        }
    }
}

pub struct DemultiplexerChip {
    address_bits: usize,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

impl DemultiplexerChip {
    pub fn power(&self) -> Pin<Sink> {
        self.sinks[0]
    }

    pub fn address(&self) -> &[Pin<Sink>] {
        &self.sinks[1..(self.address_bits + 1)]
    }

    pub fn inputs(&self) -> &[Pin<Sink>] {
        &self.sinks[(self.address_bits + 1)..self.sinks.len()]
    }

    pub fn outputs(&self, index: usize) -> &[Pin<Source>] {
        let data_bits = self.sinks.len() - 1 - self.address_bits;
        let start = index * data_bits;
        let end = start + data_bits;
        &self.sources[start..end]
    }
}

impl Chip for DemultiplexerChip {
    fn labels(&self) -> Vec<NamedRange> {
        let mut labels = vec![
            named_range!(sink power 0),
            named_range!(sinks address 1..(self.address_bits+1)),
            named_range!(sinks input (self.address_bits+1)..self.sinks.len()),
        ];
        let data_bits = self.sinks.len() - 1 - self.address_bits;
        let num_inputs = self.sources.len() / data_bits;
        for input in 0..num_inputs {
            let start = input * data_bits;
            let end = start + data_bits;
            labels.push(NamedRange::source(format!("output_{}", input), start..end));
        }
        labels
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        self.sources.clone()
    }
}

//
// Half Demultiplexer
//
// A digital "switch" for routing the input to one of the outputs.
//

derive_builder!(HalfDemultiplexer; num_outputs: usize = 2; data_bits: usize = 2);

impl BuildChip for HalfDemultiplexer {
    type Output = HalfDemultiplexerChip;

    fn name(&self) -> String {
        "Half Demultiplexer".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let input_junctions = builder.new_junction_vec(self.data_bits);
        let mut sinks = Vec::with_capacity(self.num_outputs + self.data_bits);
        let mut sources = Vec::with_capacity(self.num_outputs * self.data_bits);

        for _ in 0..self.num_outputs {
            let switch = builder.new_chip(&Switch::new(self.data_bits));
            sinks.push(switch.control());
            builder.join_all(
                (0..self.data_bits).map(|i| input_junctions[i].output()),
                switch.inputs(),
            );
            sources.extend(switch.outputs());
        }
        sinks.extend(input_junctions.iter().map(|j| j.input()));

        Self::Output {
            num_outputs: self.num_outputs,
            sinks,
            sources,
        }
    }
}

pub struct HalfDemultiplexerChip {
    num_outputs: usize,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

impl HalfDemultiplexerChip {
    pub fn select(&self) -> &[Pin<Sink>] {
        &self.sinks[..self.num_outputs]
    }

    pub fn inputs(&self) -> &[Pin<Sink>] {
        &self.sinks[self.num_outputs..]
    }

    pub fn outputs(&self, index: usize) -> &[Pin<Source>] {
        let data_bits = self.sinks.len() - self.num_outputs;
        let start = index * data_bits;
        let end = start + data_bits;
        &self.sources[start..end]
    }
}

impl Chip for HalfDemultiplexerChip {
    fn labels(&self) -> Vec<NamedRange> {
        let mut labels = vec![
            named_range!(sinks select 0..self.num_outputs),
            named_range!(sinks input self.num_outputs..self.sinks.len()),
        ];
        let data_bits = self.sinks.len() - self.num_outputs;
        let num_inputs = self.sources.len() / data_bits;
        for input in 0..num_inputs {
            let start = input * data_bits;
            let end = start + data_bits;
            labels.push(NamedRange::source(format!("output_{}", input), start..end));
        }
        labels
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.sinks.clone()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        self.sources.clone()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_four_to_one_multiplexer() {
        test_chip!(Multiplexer::new(2, 8);
            ("input_0": 3), ("input_1": 5), ("address": 0) => ("output": 3);
            ("input_0": 3), ("input_1": 5), ("address": 1) => ("output": 5);
            ("input_0": 3), ("input_1": 5), ("address": 2) => ("output": 0)
        );
    }

    #[test]
    pub fn test_one_to_four_demultiplexer() {
        test_chip!(Demultiplexer::new(2, 8);
            ("input": 3), ("address": 0) => ("output_0": 3), ("output_1": 0), ("output_2": 0);
            ("input": 3), ("address": 1) => ("output_0": 0), ("output_1": 3), ("output_2": 0);
            ("input": 3), ("address": 2) => ("output_0": 0), ("output_1": 0), ("output_2": 3)
        );
    }
}
