use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic, Inverted, Not, OR};
use crate::pinout::NamedRange;

//
// Incrementer
//
// Outputs the input plus one.
//

pub struct IncrementChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(
    IncrementChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    sources outputs |c: &Self| 0..(c.sources.len()-1),
    source overflow |c: &Self| c.sources.len() - 1
);

derive_builder!(Increment; bits: usize = 2);

impl BuildChip for Increment {
    type Output = IncrementChip;

    fn name(&self) -> String {
        "Increment".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut sinks = Vec::with_capacity(1 + self.bits);
        let mut sources = Vec::with_capacity(self.bits + 1);

        let power = builder.new_junction();
        sinks.push(power.input());
        let mut carry = power.output();

        for _ in 0..self.bits {
            let half_adder = builder.new_chip(&HalfAdder::new());
            builder.join(carry, half_adder.inputs()[0]);
            sinks.push(half_adder.inputs()[1]);
            sources.push(half_adder.xor());
            carry = half_adder.carry();
        }

        sources.push(carry);

        Self::Output { sinks, sources }
    }
}

//
// Ripple Adder
//
// TODO there are adders with less gate delay possible
// This is simpler and less gates, but has higher gate delay due to carry propogation.
//
// A1 ---
//      * Half Adder *--- Output 1
// B1 ---     *
//             \ Carry 1
// A2 ---       *
//      * Full Adder *--- Output 2
// B2 ---     *
//             \ Carry 2
// A3 ---       *
//      * Full Adder *--- Output 3
// B3 ---     *
//             \ Carry 3
// ...        ...
derive_builder!(RippleAdder; bits: usize = 2);

impl BuildChip for RippleAdder {
    type Output = RippleAdderChip;

    fn name(&self) -> String {
        "Ripple Adder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        if self.bits == 0 {
            return Self::Output {
                sinks: Vec::new(),
                sources: Vec::new(),
            };
        }
        let mut alpha_inputs = Vec::with_capacity(self.bits * 2);
        let mut beta_inputs = Vec::with_capacity(self.bits);
        let mut outputs = Vec::with_capacity(self.bits + 1);

        // no carry input on first bit, so can use a faster half adder
        let half_adder = builder.new_chip(&HalfAdder::new());
        alpha_inputs.push(half_adder.inputs()[0]);
        beta_inputs.push(half_adder.inputs()[1]);
        outputs.push(half_adder.xor());

        let mut carry = half_adder.carry();
        for _ in 1..self.bits {
            let full_adder = builder.new_chip(&FullAdder::new());
            builder.join(carry, full_adder.carry_in());
            alpha_inputs.push(full_adder.inputs()[0]);
            beta_inputs.push(full_adder.inputs()[1]);
            outputs.push(full_adder.sum());
            carry = full_adder.carry_out();
        }

        let mut inputs = alpha_inputs;
        inputs.append(&mut beta_inputs);
        outputs.push(carry);

        Self::Output {
            sinks: inputs,
            sources: outputs,
        }
    }
}

pub struct RippleAdderChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(
    RippleAdderChip;
    sinks alpha_in |c: &Self| 0..(c.sinks.len()/2),
    sinks beta_in |c: &Self| (c.sinks.len()/2)..c.sinks.len(),
    sources output |c: &Self| 0..(c.sources.len()-1),
    source carry |c: &Self| c.sources.len() - 1
);

//
// Full Adder
//
// Chains two half adders together to allow carry in (to propagate carry between bits).
//

derive_builder!(FullAdder);

impl BuildChip for FullAdder {
    type Output = FullAdderChip;

    fn name(&self) -> String {
        "Full Adder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let half_add_a_b = builder.new_chip(&HalfAdder::new());
        let half_add_ab_c = builder.new_chip(&HalfAdder::new());
        let or = builder.new_chip(&OR::new(2));
        builder.join(half_add_a_b.xor(), half_add_ab_c.inputs()[0]);
        builder.join(half_add_a_b.carry(), or.inputs()[0]);
        builder.join(half_add_ab_c.carry(), or.inputs()[1]);
        Self::Output {
            sinks: [
                half_add_a_b.inputs()[0],
                half_add_a_b.inputs()[1],
                half_add_ab_c.inputs()[1],
            ],
            sources: [half_add_ab_c.xor(), or.output()],
        }
    }
}

pub struct FullAdderChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(
    FullAdderChip;
    sinks inputs |_c: &Self| 0..2,
    sink carry_in |_c: &Self| 2,
    source sum |_c: &Self| 0,
    source carry_out |_c: &Self| 1
);

//
// Xor Gate
//
// Xor is the same as a half adder, but we don't need the (A & B) output (the carry)
//
derive_builder!(Xor);

impl BuildChip for Xor {
    type Output = XorChip;

    fn name(&self) -> String {
        "Xor".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        XorChip(HalfAdder {}.build_chip(builder))
    }
}

pub struct XorChip(HalfAdderChip);

impl Basic for XorChip {
    fn inputs(&self) -> &[Pin<Sink>] {
        self.0.inputs()
    }

    fn output(&self) -> Pin<Source> {
        self.0.xor()
    }
}

impl Chip for XorChip {
    fn labels(&self) -> Vec<NamedRange> {
        labels!(sinks input 0..2, source output 0)
    }

    fn sinks(&self) -> Vec<Pin<Sink>> {
        self.0.sinks()
    }

    fn sources(&self) -> Vec<Pin<Source>> {
        self.0.sources()
    }
}

//
// Xnor (Inverted Xor)
//

pub type Xnor = Inverted<Xor>;

//
// Binary Half Adder
//
// (An Xor gate with a tap for the (A & B) carry).
//

derive_builder!(HalfAdder);

impl BuildChip for HalfAdder {
    // A ---------
    //           *OR*------
    // B ---------        *And*--- (0) Xor
    //                    |
    // A ---              |
    //     *And*-*-*NOT*---
    // B ---     *---------------- (1) Carry
    //
    // Note splitter junctions to get A and B to both first And and OR.
    // Note NOT is powered from OR, as must be high for total Xor to be high anyway
    type Output = HalfAdderChip;

    fn name(&self) -> String {
        "Half Adder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let alpha_splitter = builder.new_junction();
        let beta_splitter = builder.new_junction();

        let or = builder.new_chip(&OR::new(2));
        builder.join(alpha_splitter.output(), or.inputs()[0]);
        builder.join(beta_splitter.output(), or.inputs()[1]);

        let first_and = builder.new_chip(&And::new(2));
        builder.join(alpha_splitter.output(), first_and.inputs()[0]);
        builder.join(beta_splitter.output(), first_and.inputs()[1]);

        let not = builder.new_chip(&Not::new());
        builder.join(or.output(), not.power());
        builder.join(first_and.output(), not.input());

        let second_and = builder.new_chip(&And::new(2));
        builder.join(or.output(), second_and.inputs()[0]);
        builder.join(not.output(), second_and.inputs()[1]);

        Self::Output {
            sinks: [alpha_splitter.input(), beta_splitter.input()],
            sources: [second_and.output(), first_and.output(), or.output()],
        }
    }
}

#[derive(Clone)]
pub struct HalfAdderChip {
    sinks: [Pin<Sink>; 2],
    sources: [Pin<Source>; 3],
}

derive_chip!(HalfAdderChip;
    sinks inputs |_c: &Self| 0..2,
    source xor |_c: &Self| 0,
    source carry |_c: &Self| 1,
    source propogate |_c: &Self| 2
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_increment() {
        test_chip!(Increment::new(8);
            ("inputs": 0) => ("outputs": 1), ("overflow" 0: false);
            ("inputs": 3) => ("outputs": 4), ("overflow" 0: false);
            ("inputs": 255) => ("outputs": 0), ("overflow" 0: true)
        );
    }

    #[test]
    pub fn test_ripple_adder() {
        const BITS: usize = 4;
        const MAX_NUMBER: usize = 1 << BITS;
        let mut device = test_device!(RippleAdder::new(BITS));
        let gate_delay = device.gate_delay().unwrap();
        for alpha in 0..MAX_NUMBER {
            for beta in 0..MAX_NUMBER {
                let result = alpha + beta;
                let carry = result >= MAX_NUMBER;
                test_suite!(
                    device; period gate_delay;
                    ("alpha_in": alpha), ("beta_in": beta) => ("output": result), ("carry" 0: carry)
                )
                .run(&mut device);
            }
        }
    }

    #[test]
    pub fn test_full_adder() {
        //assert_eq!(gate_delay(&FullAdder::new), Some(8));
        test_chip!(FullAdder::new();
            ("inputs" 0: false), ("inputs" 1: false), ("carry_in": 0) => ("sum": 0), ("carry_out": 0);
            ("inputs" 0: false), ("inputs" 1: false), ("carry_in": 1) => ("sum": 1), ("carry_out": 0);
            ("inputs" 0: false), ("inputs" 1: true), ("carry_in": 0) => ("sum": 1), ("carry_out": 0);
            ("inputs" 0: false), ("inputs" 1: true), ("carry_in": 1) => ("sum": 0), ("carry_out": 1);
            ("inputs" 0: true), ("inputs" 1: false), ("carry_in": 0) => ("sum": 1), ("carry_out": 0);
            ("inputs" 0: true), ("inputs" 1: false), ("carry_in": 1) => ("sum": 0), ("carry_out": 1);
            ("inputs" 0: true), ("inputs" 1: true), ("carry_in": 0) => ("sum": 0), ("carry_out": 1);
            ("inputs" 0: true), ("inputs" 1: true), ("carry_in": 1) => ("sum": 1), ("carry_out": 1)
        );
    }

    #[test]
    pub fn test_half_adder() {
        //assert_eq!(gate_delay(&FullAdder::new), Some(8));
        test_chip!(HalfAdder::new();
            ("inputs" 0: false), ("inputs" 1: false) => ("xor": 0), ("carry": 0);
            ("inputs" 0: false), ("inputs" 1: true) => ("xor": 1), ("carry": 0);
            ("inputs" 0: true), ("inputs" 1: false) => ("xor": 1), ("carry": 0);
            ("inputs" 0: true), ("inputs" 1: true) => ("xor": 0), ("carry": 1)
        );
    }

    #[test]
    pub fn test_xor() {
        //assert_eq!(gate_delay(&FullAdder::new), Some(8));
        test_chip!(Xor::new();
            "00" => "0";
            "01" => "1";
            "10" => "1";
            "11" => "0"
        );
    }
}
