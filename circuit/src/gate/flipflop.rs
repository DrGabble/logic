use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic, Not};
use crate::pinout::NamedRange;

//
// Flip Flops - for storing signals (aka 1 bit of memory)
//
// https://www.falstad.com/circuit/e-edgedff.html
// https://www.electronics-tutorials.ws/sequential/conversion-of-flip-flops.html
// https://en.wikipedia.org/wiki/Flip-flop_(electronics)

//
// Edge Triggered Flip Flop
//
// Takes input only on the rising edge of the clock input.
//

derive_builder!(FallingEdgeLatch);

impl BuildChip for FallingEdgeLatch {
    type Output = FallingEdgeLatchChip;

    fn name(&self) -> String {
        "Falling Edge Latch".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power = builder.new_junction();
        let clock = builder.new_junction();

        let master = builder.new_chip(&GatedLatch::new());
        let slave = builder.new_chip(&GatedLatch::new());
        let not = builder.new_chip(&Not::new());

        let input = master.input();
        builder.join(power.output(), master.power());
        builder.join(clock.output(), not.input());
        builder.join(clock.output(), master.write());

        builder.join(power.output(), not.power());

        builder.join(power.output(), slave.power());
        builder.join(master.is_set(), slave.input());
        builder.join(not.output(), slave.write());

        Self::Output {
            sinks: [power.input(), clock.input(), input],
            sources: [slave.is_reset(), slave.is_set()],
        }
    }
}

pub struct FallingEdgeLatchChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(FallingEdgeLatchChip;
    sink power |_: &Self| 0,
    sink clock |_: &Self| 1,
    sink input |_: &Self| 2,
    source is_reset |_: &Self| 0,
    source is_set |_: &Self| 1
);

//
// Gated Latch
//
// A single bit of memory. Can be set high or low only when write input is set high.
// Note takes 6 steps to stabilise!
//

derive_builder!(GatedLatch);

impl BuildChip for GatedLatch {
    type Output = GatedLatchChip;

    fn name(&self) -> String {
        "Gated Latch".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // power
        let power_junction = builder.new_junction();

        // gate
        let gate = builder.new_chip(&Gate::new());
        builder.join(power_junction.output(), gate.power());

        // set / reset latch
        let set_reset_latch = builder.new_chip(&SetResetLatch::new());
        builder.join(power_junction.output(), set_reset_latch.power());
        builder.join(gate.reset(), set_reset_latch.reset());
        builder.join(gate.set(), set_reset_latch.set());

        Self::Output {
            sinks: [power_junction.input(), gate.write(), gate.input()],
            sources: [set_reset_latch.is_reset(), set_reset_latch.is_set()],
        }
    }
}
pub struct GatedLatchChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(GatedLatchChip;
    sink power |_: &Self| 0,
    sink write |_: &Self| 1,
    sink input |_: &Self| 2,
    source is_reset |_: &Self| 0,
    source is_set |_: &Self| 1
);

//
// Gate
//
// Turns a "write" and "input" signal into the "set" and "reset" signals.
//

derive_builder!(Gate);

impl BuildChip for Gate {
    type Output = GateChip;

    fn name(&self) -> String {
        "Gate".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // input junctions
        let write_junction = builder.new_junction();
        let input_junction = builder.new_junction();

        // reset half
        let data_not = builder.new_chip(&Not::new());
        builder.join(input_junction.output(), data_not.input());

        let reset_and = builder.new_chip(&And::new(2));
        builder.join(data_not.output(), reset_and.inputs()[0]);
        builder.join(write_junction.output(), reset_and.inputs()[1]);

        // set half
        let set_and = builder.new_chip(&And::new(2));
        builder.join(input_junction.output(), set_and.inputs()[0]);
        builder.join(write_junction.output(), set_and.inputs()[1]);

        Self::Output {
            sinks: [
                data_not.power(),
                write_junction.input(),
                input_junction.input(),
            ],
            sources: [reset_and.output(), set_and.output()],
        }
    }
}

pub struct GateChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(GateChip;
    sink power |_: &Self| 0,
    sink write |_: &Self| 1,
    sink input |_: &Self| 2,
    source reset |_: &Self| 0,
    source set |_: &Self| 1
);

//
// JK Master-Slave Flip Flop
//
// J == set, R == reset, but also J == K == 1 == toggle
// Prone to racing however
// https://www.electronics-tutorials.ws/sequential/seq_2.html
// Signal wont leave chip until falling edge of clock + some delay.
//

derive_builder!(JkMasterSlave);

impl BuildChip for JkMasterSlave {
    type Output = JkMasterSlaveChip;

    fn name(&self) -> String {
        "JK Master Slave".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let clock = builder.new_junction();
        let power = builder.new_junction();

        let set_and = builder.new_chip(&And::new(2));
        let reset_and = builder.new_chip(&And::new(2));
        let master = builder.new_chip(&JkFlipFlop::new());
        let not = builder.new_chip(&Not::new());
        let slave = builder.new_chip(&JkFlipFlop::new());

        builder.join(slave.is_reset(), set_and.inputs()[0]);
        let set = set_and.inputs()[1];

        builder.join(slave.is_set(), reset_and.inputs()[0]);
        let reset = reset_and.inputs()[1];

        // master
        builder.join(power.output(), master.power());
        builder.join(clock.output(), master.clock());
        builder.join(set_and.output(), master.set());
        builder.join(reset_and.output(), master.reset());

        // clock invert
        builder.join(power.output(), not.power());
        builder.join(clock.output(), not.input());

        // slave
        builder.join(power.output(), slave.power());
        builder.join(not.output(), slave.clock());
        builder.join(master.is_set(), slave.set());
        builder.join(master.is_reset(), slave.reset());

        Self::Output {
            sinks: [power.input(), clock.input(), reset, set],
            sources: [slave.is_reset(), slave.is_set()],
        }
    }
}

pub struct JkMasterSlaveChip {
    sinks: [Pin<Sink>; 4],
    sources: [Pin<Source>; 2],
}

derive_chip!(JkMasterSlaveChip;
    sink power |_: &Self| 0,
    sink clock |_: &Self| 1,
    sink reset |_: &Self| 2,
    sink set |_: &Self| 3,
    source is_reset |_: &Self| 0,
    source is_set |_: &Self| 1
);

//
// JK Flip Flop (incomplete)
//
// J == set, R == reset, but also J == K == 1 == toggle
// note inverted output needs to be anded with input to get true JK Flip Flip
// Prone to racing however
//

derive_builder!(JkFlipFlop);

impl BuildChip for JkFlipFlop {
    type Output = JkFlipFlopChip;

    fn name(&self) -> String {
        "JK Flip Flop".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let clock = builder.new_junction();

        let set_and = builder.new_chip(&And::new(2));
        let reset_and = builder.new_chip(&And::new(2));
        let set_reset_latch = builder.new_chip(&SetResetLatch::new());

        let set = set_and.inputs()[0];
        builder.join(clock.output(), set_and.inputs()[1]);
        builder.join(set_and.output(), set_reset_latch.set());

        let reset = reset_and.inputs()[0];
        builder.join(clock.output(), reset_and.inputs()[1]);
        builder.join(reset_and.output(), set_reset_latch.reset());

        Self::Output {
            sinks: [set_reset_latch.power(), clock.input(), reset, set],
            sources: [set_reset_latch.is_reset(), set_reset_latch.is_set()],
        }
    }
}

pub struct JkFlipFlopChip {
    sinks: [Pin<Sink>; 4],
    sources: [Pin<Source>; 2],
}

derive_chip!(JkFlipFlopChip;
    sink power |_: &Self| 0,
    sink clock |_: &Self| 1,
    sink reset |_: &Self| 2,
    sink set |_: &Self| 3,
    source is_reset |_: &Self| 0,
    source is_set |_: &Self| 1
);

//
// Set Reset Latch (Asynchronous)
//
// N bits of memory, that can be set or reset and keeps it's state.
//
// reset ---\
//           *OR*---*NOT*---*---> is set
//          /   ___________/
//          \ /
//           X
//          / \
//          \   -----------\
//           *OR*---*NOT*---*---> is reset
// set   ---/

derive_builder!(SetResetLatch);

impl BuildChip for SetResetLatch {
    type Output = SetResetLatchChip;

    fn name(&self) -> String {
        "Set Reset Latch".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power = builder.new_junction();

        let and = builder.new_chip(&And::new(2));
        builder.join(and.output(), and.inputs()[0]);

        let reset_not = builder.new_chip(&Not::new());
        builder.join(power.output(), reset_not.power());
        builder.join(reset_not.output(), and.inputs()[1]);

        let output_not = builder.new_chip(&Not::new());
        builder.join(power.output(), output_not.power());
        builder.join(and.output(), output_not.input());

        Self::Output {
            sinks: [power.input(), reset_not.input(), and.inputs()[0]],
            sources: [output_not.output(), and.output()],
        }
    }
}

pub struct SetResetLatchChip {
    sinks: [Pin<Sink>; 3],
    sources: [Pin<Source>; 2],
}

derive_chip!(SetResetLatchChip;
    sink power |_: &Self| 0,
    sink reset |_: &Self| 1,
    sink set |_: &Self| 2,
    source is_reset |_: &Self| 0,
    source is_set |_: &Self| 1
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_gated_latch() {
        let mut device = test_device!(GatedLatch::new());
        test_suite!(
            device; period 6;
            ("write": 1), ("input": 1) => ("is_set": 1), ("is_reset": 0); // set
            ("power": 1) => ("is_set": 1), ("is_reset": 0); // remains stable
            ("input": 0) => ("is_set": 1), ("is_reset": 0); // input ignored if write not set
            ("write": 1), ("input": 0) => ("is_set": 0), ("is_reset": 1); // set low
            ("power": 1) => ("is_set": 0), ("is_reset": 1); // remains stable
            ("write": 1), ("input": 1) => ("is_set": 1), ("is_reset": 0) // set high again
        )
        .run(&mut device);
    }

    #[test]
    pub fn test_set_reset_latch() {
        let mut device = test_device!(SetResetLatch::new());
        test_suite!(
            device; period 4;
            ("set": 1) => ("is_set": 1), ("is_reset": 0); // inital set
            ("power": 1) => ("is_set": 1), ("is_reset": 0); // stable when no signal
            ("reset": 1) => ("is_set": 0), ("is_reset": 1); //reset
            ("power": 1) => ("is_set": 0), ("is_reset": 1); // remains stable when no signal
            ("set": 1) => ("is_set": 1), ("is_reset": 0) // set again
        )
        .run(&mut device);
    }
}
