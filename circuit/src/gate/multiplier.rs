use std::collections::VecDeque;

use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Junction, Pin, Sink, Source};
use super::adder::{FullAdder, HalfAdder, RippleAdder};
use super::basic::{And, Basic};
use crate::pinout::NamedRange;

//
// Wallace Tree Binary Multiplier
//
// https://en.wikipedia.org/wiki/Wallace_tree
//

derive_builder!(DadaMultiplier;
    alpha_in_bits: usize = 2;
    beta_in_bits: usize = 2;
    output_bits: usize = 2
);

impl DadaMultiplier {
    pub fn uniform(bits: usize) -> Self {
        Self::new(bits, bits, bits)
    }
}

impl BuildChip for DadaMultiplier {
    type Output = DadaMultiplierChip;

    fn name(&self) -> String {
        "Multiplier".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let alpha_junctions = builder.new_junction_vec(self.alpha_in_bits);
        let beta_junctions = builder.new_junction_vec(self.beta_in_bits);
        let sinks = alpha_junctions
            .iter()
            .chain(beta_junctions.iter())
            .map(|j| j.input())
            .collect();
        let sources = AllWeights::new(builder, &alpha_junctions, &beta_junctions, self.output_bits)
            .reduce_to_sources(builder);

        Self::Output {
            alpha_in_bits: self.alpha_in_bits,
            sinks,
            sources,
        }
    }
}

struct AllWeights {
    weights: Vec<Weight>,
    overflows: Vec<Pin<Source>>,
    min_input_bits: usize,
    max_output_bits: usize,
}

impl AllWeights {
    fn new(
        builder: &mut Builder,
        alpha_junctions: &[Junction],
        beta_junctions: &[Junction],
        max_output_bits: usize,
    ) -> Self {
        let min_input_bits = std::cmp::max(alpha_junctions.len(), beta_junctions.len());
        let mut weights = Self {
            weights: Vec::new(),
            overflows: Vec::new(),
            min_input_bits,
            max_output_bits,
        };
        for (shift, alpha) in alpha_junctions.iter().enumerate() {
            for (index, beta) in beta_junctions.iter().enumerate() {
                let and = builder.new_chip(&And::new(2));
                builder.join(alpha.output(), and.inputs()[0]);
                builder.join(beta.output(), and.inputs()[1]);
                weights.add_source(shift + index, and.output());
            }
        }
        weights
    }

    fn add_source(&mut self, weight: usize, source: Pin<Source>) {
        if weight >= self.max_output_bits {
            self.overflows.push(source);
        } else {
            while weight >= self.weights.len() {
                self.weights.push(Weight::new());
            }
            self.weights[weight].add_source(source);
        }
    }

    fn reduce_to_height_of_two(&mut self, builder: &mut Builder) {
        // reduce to only two products per weight
        for max_height in max_heights(self.min_input_bits).iter().rev() {
            for weight_index in 0..self.weights.len() {
                while let Some(source) = self.weights[weight_index].reduce(builder, *max_height) {
                    self.add_source(weight_index + 1, source);
                }
            }
        }
        assert!(self.weights.iter().map(Weight::len).max().unwrap_or(0) <= 2);
    }

    fn reduce_to_sources(mut self, builder: &mut Builder) -> Vec<Pin<Source>> {
        self.reduce_to_height_of_two(builder);

        // create sources
        let mut sources = Vec::new();

        // sum products with full adder
        let non_unary_weights = self.weights.iter().filter(|w| w.len() == 2).count();
        let ripple_adder = builder.new_chip(&RippleAdder::new(non_unary_weights));
        let mut alpha_in = ripple_adder.alpha_in().iter().cloned();
        for weight in self.weights.iter_mut() {
            match weight.len() {
                1 => sources.push(weight.pop()),
                2 => builder.join(weight.pop(), alpha_in.next().unwrap()),
                _ => panic!("Weight not fully reduced"),
            }
        }
        let weight_outputs: Vec<_> = self
            .weights
            .iter_mut()
            .filter(|w| w.len() == 1)
            .map(Weight::pop)
            .collect();
        builder.join_all(weight_outputs.iter(), ripple_adder.beta_in());
        sources.extend(ripple_adder.output());

        // carry out of ripple adder can be output if not enough sources yet,
        // else contributes to overflow flag
        if sources.len() < self.max_output_bits {
            sources.push(ripple_adder.carry());
        } else {
            self.overflows.push(ripple_adder.carry())
        }

        // pad with zeros (if output is not large enough)
        while sources.len() < self.max_output_bits {
            sources.push(builder.new_junction().output());
        }

        // add overflow as final source
        sources.push(self.create_overflow_out(builder));

        sources
    }

    fn create_overflow_out(&mut self, builder: &mut Builder) -> Pin<Source> {
        let junction = builder.new_junction();
        for overflow in self.overflows.iter() {
            builder.join(*overflow, junction.input());
        }
        junction.output()
    }
}

struct Weight {
    sources: VecDeque<Pin<Source>>,
}

impl Weight {
    fn new() -> Self {
        Self {
            sources: VecDeque::new(),
        }
    }

    fn add_source(&mut self, source: Pin<Source>) {
        self.sources.push_front(source);
    }

    fn len(&self) -> usize {
        self.sources.len()
    }

    fn reduce(&mut self, builder: &mut Builder, max_len: usize) -> Option<Pin<Source>> {
        if self.sources.len() <= max_len {
            return None;
        }
        match self.sources.len() {
            0..=2 => None,
            3 => Some(self.add_half_adder(builder)),
            _ => Some(self.add_full_adder(builder)),
        }
    }

    fn add_half_adder(&mut self, builder: &mut Builder) -> Pin<Source> {
        let half_adder = builder.new_chip(&HalfAdder::new());
        builder.join(self.pop(), half_adder.inputs()[0]);
        builder.join(self.pop(), half_adder.inputs()[1]);
        self.sources.push_back(half_adder.xor());
        half_adder.carry()
    }

    fn add_full_adder(&mut self, builder: &mut Builder) -> Pin<Source> {
        let full_adder = builder.new_chip(&FullAdder::new());
        builder.join(self.pop(), full_adder.inputs()[0]);
        builder.join(self.pop(), full_adder.inputs()[1]);
        builder.join(self.pop(), full_adder.carry_in());
        self.sources.push_back(full_adder.sum());
        full_adder.carry_out()
    }

    fn pop(&mut self) -> Pin<Source> {
        self.sources.pop_back().unwrap()
    }
}

fn max_heights(min_bits: usize) -> Vec<usize> {
    std::iter::successors(Some(2), |last| Some((last * 3) / 2))
        .take_while(|n| *n < min_bits)
        .collect()
}

pub struct DadaMultiplierChip {
    alpha_in_bits: usize,
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(DadaMultiplierChip;
    sinks alpha_in |c: &Self| 0..c.alpha_in_bits,
    sinks beta_in |c: &Self| c.alpha_in_bits..c.sinks.len(),
    sources output |c: &Self| 0..(c.sources.len()-1),
    source overflow |c: &Self| c.sources.len()-1
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_multiply() {
        const BITS: usize = 3;
        const MAX_NUMBER: usize = 1 << BITS;
        let mut device = test_device!(DadaMultiplier::uniform(BITS));
        let gate_delay = device.gate_delay().unwrap();
        for alpha in 0..MAX_NUMBER {
            for beta in 0..MAX_NUMBER {
                let result = alpha * beta;
                let overflow = result >= MAX_NUMBER;
                test_suite!(device; period gate_delay;
                    ("alpha_in": alpha), ("beta_in": beta) => ("output": result), ("overflow" 0: overflow)
                ).run(&mut device);
            }
        }
    }
}
