use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic, Not, OR};
use crate::pinout::NamedRange;

//
// Priority Encoder
//
// Outputs an N bits number corresponding to the hiehgest index of the any inputs that are high.
// If more that one is high, it will output the highest index (unlike a normal Encoder).
// Note if no inputs, then output is not valid.
//
// https://en.wikipedia.org/wiki/Priority_encoder
// http://web.yl.is.s.u-tokyo.ac.jp/~affeldt/examination/examination/node157.html
//

derive_builder!(PriorityEncoder; outputs: usize = 2);

impl BuildChip for PriorityEncoder {
    type Output = PriorityEncoderChip;

    fn name(&self) -> String {
        "Priority Encoder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let num_inputs = 1 << self.outputs;
        let output_ors = builder.new_chip_vec(&OR::new(num_inputs >> 1), self.outputs);
        let input_junctions: Vec<_> = builder.new_junction_vec(num_inputs);
        let power_junction = builder.new_junction();

        // invert inputs
        // TODO don't need 0 or 2 ever
        let input_nots: Vec<_> = input_junctions
            .iter()
            .map(|j| {
                let not_gate = builder.new_chip(&Not::new());
                builder.join(power_junction.output(), not_gate.power());
                builder.join(j.output(), not_gate.input());
                not_gate
            })
            .collect();

        // build a collection of gates outputing "I am the highest bit set"
        // TODO 0 is never used, and Nth is trivial
        let highest_ands: Vec<_> = (0..num_inputs)
            .map(|n| {
                let and_gate = builder.new_chip(&And::new(num_inputs - n));
                builder.join(input_junctions[n].output(), and_gate.inputs()[0]);
                builder.join_all(
                    input_nots[(n + 1)..].iter().map(|n| n.output()),
                    &and_gate.inputs()[1..],
                );
                and_gate
            })
            .collect();

        // assign and gates to correct outputs
        for (output_index, or_gate) in output_ors.iter().enumerate() {
            let period = 2 << output_index;
            let window = 1 << output_index;
            let mut input = 0;
            for (and_index, and_gate) in highest_ands.iter().enumerate() {
                if and_index % period >= window {
                    builder.join(and_gate.output(), or_gate.inputs()[input]);
                    input += 1;
                }
            }
        }

        // "is valid" output
        let valid_or = builder.new_chip(&OR::new(num_inputs));
        builder.join_all(
            input_junctions.iter().map(|j| j.output()),
            valid_or.inputs(),
        );

        let sinks = std::iter::once(power_junction)
            .chain(input_junctions)
            .map(|j| j.input())
            .collect();
        let sources = std::iter::once(valid_or)
            .chain(output_ors)
            .map(|o| o.output())
            .collect();
        Self::Output { sinks, sources }
    }
}

pub struct PriorityEncoderChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(PriorityEncoderChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    source valid |_: &Self| 0,
    sources outputs |c: &Self| 1..c.sources.len()
);

//
// Encoder
//
// Outputs an N bits number corresponding to the index of the input that is high.
// Note if no inputs are high, output is garbage.
// Note if more than one input is high, output is garbage.
// See also: Priority Encoder
// TODO add valid signal output
//

derive_builder!(Encoder; outputs: usize = 2);

impl BuildChip for Encoder {
    type Output = EncoderChip;

    fn name(&self) -> String {
        "Encoder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let num_inputs = 1 << self.outputs;
        let junctions: Vec<_> = builder.new_junction_vec(num_inputs);
        let or_gates = builder.new_chip_vec(&OR::new(num_inputs), self.outputs);

        for (output, or) in or_gates.iter().enumerate() {
            let period = 2 << output;
            let window = 1 << output;
            for (input, junction) in junctions.iter().enumerate() {
                if input % period >= window {
                    builder.join(junction.output(), or.inputs()[input]);
                }
            }
        }

        // "is valid" output
        let valid_or = builder.new_chip(&OR::new(num_inputs));
        builder.join_all(junctions.iter().map(|j| j.output()), valid_or.inputs());

        let sinks = junctions.into_iter().map(|j| j.input()).collect();
        let sources = std::iter::once(valid_or)
            .chain(or_gates)
            .map(|o| o.output())
            .collect();
        Self::Output { sinks, sources }
    }
}

pub struct EncoderChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(EncoderChip;
    sinks inputs |c: &Self| 0..c.sinks.len(),
    source valid |_: &Self| 0,
    sources outputs |c: &Self| 1..c.sources.len()
);

//
// Decoder
//
// Turns an N bit number in to one of 2^N outputs (only)
//

derive_builder!(Decoder; inputs: usize = 2);

impl BuildChip for Decoder {
    type Output = DecoderChip;

    fn name(&self) -> String {
        "Decoder".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // trivial case
        if self.inputs == 0 {
            let power_junction = builder.new_junction();
            return Self::Output {
                sinks: vec![power_junction.input()],
                sources: vec![power_junction.output()],
            };
        }

        // non trivial case
        let output_bits = 1 << self.inputs;
        let junctions: Vec<_> = builder.new_junction_vec(self.inputs);
        let power_junction = builder.new_junction();
        let nots = builder.new_chip_vec(&Not::new(), self.inputs);

        for index in 0..self.inputs {
            builder.join(junctions[index].output(), nots[index].input());
            builder.join(power_junction.output(), nots[index].power());
        }

        let mut sources = Vec::with_capacity(output_bits);
        for output_index in (0..output_bits).rev() {
            let and = builder.new_chip(&And::new(self.inputs));
            for input_index in 0..self.inputs {
                // e.g. 0: output_bits % 2 < 1  ****----
                //      1: output_bits % 4 < 2  **--**--
                //      2: output_bits % 8 < 4  *-*-*-*-
                if (output_index % (2 << input_index)) < (1 << input_index) {
                    builder.join(junctions[input_index].output(), and.inputs()[input_index]);
                } else {
                    builder.join(nots[input_index].output(), and.inputs()[input_index]);
                }
            }
            sources.push(and.output());
        }

        let mut sinks = vec![power_junction.input()];
        sinks.extend(junctions.iter().map(|j| j.input()));

        Self::Output { sinks, sources }
    }
}

pub struct DecoderChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(DecoderChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    sources outputs |c: &Self| 0..c.sources.len()
);

//
// Switch
//
// Simple circuit where output is equal to output but only if control is set.
// Else output is all zeros.
//

pub struct SwitchChip {
    sinks: Vec<Pin<Sink>>,
    sources: Vec<Pin<Source>>,
}

derive_chip!(SwitchChip;
    sink control |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    sources outputs |c: &Self| 0..c.sources.len()
);

derive_builder!(Switch; bits: usize = 2);

impl BuildChip for Switch {
    type Output = SwitchChip;

    fn name(&self) -> String {
        "Switch".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut sinks = Vec::with_capacity(self.bits);
        let mut sources = Vec::with_capacity(self.bits);

        let control_junction = builder.new_junction();
        sinks.push(control_junction.input());

        for _ in 0..self.bits {
            let and = builder.new_chip(&And::new(2));
            builder.join(control_junction.output(), and.inputs()[0]);
            sinks.push(and.inputs()[1]);
            sources.push(and.output());
        }

        Self::Output { sinks, sources }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_four_to_two_priority_encoder() {
        test_chip!(PriorityEncoder::new(2);
            ("inputs": 0b0000) => ("outputs": 0), ("valid": 0);
            ("inputs": 0b0001) => ("outputs": 0), ("valid": 1);
            ("inputs": 0b0010) => ("outputs": 1), ("valid": 1);
            ("inputs": 0b0011) => ("outputs": 1), ("valid": 1);
            ("inputs": 0b0100) => ("outputs": 2), ("valid": 1);
            ("inputs": 0b0101) => ("outputs": 2), ("valid": 1);
            ("inputs": 0b0110) => ("outputs": 2), ("valid": 1);
            ("inputs": 0b0111) => ("outputs": 2), ("valid": 1);
            ("inputs": 0b1000) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1001) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1010) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1011) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1100) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1101) => ("outputs": 3), ("valid": 1);
            ("inputs": 0b1111) => ("outputs": 3), ("valid": 1)
        );
    }

    #[test]
    pub fn test_four_to_two_encoder() {
        test_chip!(Encoder::new(2);
            "0000" => ("outputs": 0), ("valid": 0);
            "1000" => ("outputs": 0), ("valid": 1);
            "0100" => ("outputs": 1), ("valid": 1);
            "0010" => ("outputs": 2), ("valid": 1);
            "0001" => ("outputs": 3), ("valid": 1)
        );
    }

    #[test]
    pub fn test_two_to_four_decoder() {
        test_chip!(Decoder::new(2);
            ("inputs": 0b00) => "1000";
            ("inputs": 0b01) => "0100";
            ("inputs": 0b10) => "0010";
            ("inputs": 0b11) => "0001"
        );
    }

    #[test]
    pub fn test_three_to_eight_decoder() {
        test_chip!(Decoder::new(3);
            ("inputs": 0b000) => "10000000";
            ("inputs": 0b001) => "01000000";
            ("inputs": 0b010) => "00100000";
            ("inputs": 0b011) => "00010000";
            ("inputs": 0b100) => "00001000";
            ("inputs": 0b101) => "00000100";
            ("inputs": 0b110) => "00000010";
            ("inputs": 0b111) => "00000001"
        );
    }

    #[test]
    pub fn test_switch() {
        test_chip!(Switch::new(4);
            ("control": 0), ("inputs": 0b1010) => ("outputs": 0b0000);
            ("control": 1), ("inputs": 0b1010) => ("outputs": 0b1010)
        )
    }
}
