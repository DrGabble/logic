use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::basic::{And, Basic};
use super::flipflop::{FallingEdgeLatch, JkMasterSlave};
use crate::pinout::NamedRange;

//
// Synchronous Counter
//

derive_builder!(SyncCounter; bits: usize = 4);

impl BuildChip for SyncCounter {
    type Output = SyncCounterChip;

    fn name(&self) -> String {
        "Synchronised Counter".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        assert!(self.bits >= 1);
        let mut sources = Vec::new();
        let power = builder.new_junction();
        let clock = builder.new_junction();

        let gated_latch = builder.new_chip(&JkMasterSlave::new());
        builder.join(power.output(), gated_latch.power());
        builder.join(clock.output(), gated_latch.clock());
        builder.join(power.output(), gated_latch.set());
        builder.join(power.output(), gated_latch.reset());
        sources.push(gated_latch.is_set());
        let mut input = gated_latch.is_set();

        for index in 1..self.bits {
            let gated_latch = builder.new_chip(&JkMasterSlave::new());
            builder.join(power.output(), gated_latch.power());
            builder.join(clock.output(), gated_latch.clock());
            builder.join(input, gated_latch.set());
            builder.join(input, gated_latch.reset());
            sources.push(gated_latch.is_set());

            if index < self.bits - 1 {
                let and = builder.new_chip(&And::new(2));
                builder.join(input, and.inputs()[0]);
                builder.join(gated_latch.is_set(), and.inputs()[1]);
                input = and.output();
            }
        }

        Self::Output {
            sinks: [power.input(), clock.input()],
            sources,
        }
    }
}

pub struct SyncCounterChip {
    sinks: [Pin<Sink>; 2],
    sources: Vec<Pin<Source>>,
}

derive_chip!(SyncCounterChip;
    sink power |_c: &Self| 0,
    sink clock |_c: &Self| 1,
    sources outputs |c: &Self| 0..c.sources.len()
);

//
// RippleCounter
//
// NB falling edge triggered
// Counts the number of times the inputs is set high
// There is a delay to the output signal of clock * 2 ^ BITS
//

derive_builder!(RippleCounter; bits: usize = 2);

impl BuildChip for RippleCounter {
    type Output = RippleCounterChip;

    fn name(&self) -> String {
        "Ripple Counter".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        assert!(self.bits >= 1);
        let mut sources = Vec::new();
        let power_junction = builder.new_junction();

        let gated_latch = builder.new_chip(&FallingEdgeLatch::new());
        builder.join(power_junction.output(), gated_latch.power());
        builder.join(gated_latch.is_reset(), gated_latch.input());
        sources.push(gated_latch.is_set());
        let clock = gated_latch.clock();
        let mut input = gated_latch.is_set();

        // // TODO DEBUG REMOVE vvv
        // let debug_clock = builder.new_chip::<Clock>();
        // builder.join(power_junction.output(), debug_clock.power());
        // builder.join(debug_clock.output(), gated_latch.write());
        // // TODO DEBUG REMOVE ^^^

        for _ in 1..self.bits {
            let gated_latch = builder.new_chip(&FallingEdgeLatch::new());
            builder.join(power_junction.output(), gated_latch.power());
            builder.join(gated_latch.is_reset(), gated_latch.input());
            builder.join(input, gated_latch.clock());
            sources.push(gated_latch.is_set());
            input = gated_latch.is_set();
        }

        Self::Output {
            sinks: [power_junction.input(), clock],
            sources,
        }
    }
}

pub struct RippleCounterChip {
    sinks: [Pin<Sink>; 2],
    sources: Vec<Pin<Source>>,
}

derive_chip!(RippleCounterChip;
    sink power |_c: &Self| 0,
    sink clock |_c: &Self| 1,
    sources outputs |c: &Self| 0..c.sources.len()
);

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    pub fn test_sync_counter() {
        const BITS: usize = 4;
        let mut device = test_device!(SyncCounter::new(BITS));

        test_suite!(device; clock 8; period 16;
            ("power": 1) => ("outputs": 0);
            ("power": 1) => ("outputs": 1);
            ("power": 1) => ("outputs": 2);
            ("power": 1) => ("outputs": 3);
            ("power": 1) => ("outputs": 4);
            ("power": 1) => ("outputs": 5);
            ("power": 1) => ("outputs": 6);
            ("power": 1) => ("outputs": 7);
            ("power": 1) => ("outputs": 8);
            ("power": 1) => ("outputs": 9);
            ("power": 1) => ("outputs": 10);
            ("power": 1) => ("outputs": 11);
            ("power": 1) => ("outputs": 12);
            ("power": 1) => ("outputs": 13);
            ("power": 1) => ("outputs": 14);
            ("power": 1) => ("outputs": 15);
            ("power": 1) => ("outputs": 0)
        )
        .run(&mut device);
    }
}
