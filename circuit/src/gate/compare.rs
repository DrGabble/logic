use super::super::chip::{BuildChip, Chip, ChipBuilder};
use super::super::plan::{Builder, Pin, Sink, Source};
use super::adder::Xor;
use super::basic::{And, Basic, Inverted, Not, OR};
use crate::pinout::NamedRange;

//
// Digital Comparator
//
// Calculates if input A is greater than, equal to, or less than input B.
// https://www.electrical4u.com/digital-comparator/.
//

derive_builder!(Comparator; bits: usize = 2);

impl BuildChip for Comparator {
    type Output = ComparatorChip;

    fn name(&self) -> String {
        "Comparator".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        // inputs
        let power_junction = builder.new_junction();

        let alpha_junctions: Vec<_> = builder.new_junction_vec(self.bits);
        let alpha: Vec<_> = alpha_junctions.iter().map(|j| j.output()).collect();
        let not_alpha: Vec<_> = alpha_junctions
            .iter()
            .map(|j| {
                let not = builder.new_chip(&Not::new());
                builder.join(power_junction.output(), not.power());
                builder.join(j.output(), not.input());
                not.output()
            })
            .collect();

        let beta_junctions: Vec<_> = builder.new_junction_vec(self.bits);
        let beta: Vec<_> = beta_junctions.iter().map(|j| j.output()).collect();
        let not_beta: Vec<_> = beta_junctions
            .iter()
            .map(|j| {
                let not = builder.new_chip(&Not::new());
                builder.join(power_junction.output(), not.power());
                builder.join(j.output(), not.input());
                not.output()
            })
            .collect();

        // "equal" XnorS
        let mut xnor_outputs = Vec::new();
        let equal_and = builder.new_chip(&And::new(self.bits));

        for bit in 0..self.bits {
            let xnor = builder.new_chip(&Inverted::new(Xor::new()));
            builder.join(power_junction.output(), xnor.power());
            builder.join(alpha_junctions[bit].output(), xnor.inputs()[0]);
            builder.join(beta_junctions[bit].output(), xnor.inputs()[1]);
            builder.join(xnor.output(), equal_and.inputs()[bit]);
            xnor_outputs.push(xnor.output());
        }

        // "a greater than b" ands and (one) or
        let greater = builder.new_chip(&OR::new(self.bits));
        build_compare_ands(builder, &alpha, &not_beta, &xnor_outputs, greater.inputs());

        // "a greater than or equal to"
        let greater_equal_or = builder.new_chip(&OR::new(2));
        builder.join(equal_and.output(), greater_equal_or.inputs()[0]);
        builder.join(greater.output(), greater_equal_or.inputs()[1]);

        // "a less than b " ands and (one) or
        let less = builder.new_chip(&OR::new(self.bits));
        build_compare_ands(builder, &not_alpha, &beta, &xnor_outputs, less.inputs());

        // "a less than or equal to"
        let less_equal_or = builder.new_chip(&OR::new(2));
        builder.join(equal_and.output(), less_equal_or.inputs()[0]);
        builder.join(less.output(), less_equal_or.inputs()[1]);

        // sinks
        let mut sinks = vec![power_junction.input()];
        sinks.extend(alpha_junctions.into_iter().map(|j| j.input()));
        sinks.extend(beta_junctions.into_iter().map(|j| j.input()));

        // sources
        let sources = [
            greater.output(),
            greater_equal_or.output(),
            equal_and.output(),
            less_equal_or.output(),
            less.output(),
        ];

        Self::Output { sinks, sources }
    }
}

fn build_compare_ands(
    builder: &mut Builder,
    alpha: &[Pin<Source>],
    beta: &[Pin<Source>],
    xnor: &[Pin<Source>],
    or: &[Pin<Sink>],
) {
    for bit in 0..alpha.len() {
        let and_inputs = alpha.len() + 1 - bit;
        let and = builder.new_chip(&And::new(and_inputs));
        builder.join(alpha[bit], and.inputs()[0]);
        builder.join(beta[bit], and.inputs()[1]);
        for (sink, source) in and.inputs()[2..and_inputs].iter().zip(xnor.iter().rev()) {
            builder.join(*source, *sink);
        }
        builder.join(and.output(), or[bit]);
    }
}

pub struct ComparatorChip {
    sinks: Vec<Pin<Sink>>,
    sources: [Pin<Source>; 5],
}

derive_chip!(ComparatorChip;
    sink power |_: &Self| 0,
    sinks alpha_in |c: &Self| 1..((c.sinks.len()/2) + 1),
    sinks beta_in |c: &Self| ((c.sinks.len()/2) + 1)..c.sinks.len(),
    source a_greater_than_b |_: &Self| 0,
    source a_greater_or_equal_b |_: &Self| 1,
    source a_equal_b |_: &Self| 2,
    source a_less_or_equal_b |_: &Self| 3,
    source a_less_b |_: &Self| 4
);

//
// EqualHardcoded
//
// Output is 1 if input matches hardcoded pattern.
// TODO could be cunning here and skip power if value to match is non zero?
//      use one of the high values for power to Not gates instead?
//

pub struct EqualHardCodedChip {
    sinks: Vec<Pin<Sink>>,
    sources: [Pin<Source>; 1],
}

derive_chip!(EqualHardCodedChip;
    sink power |_: &Self| 0,
    sinks inputs |c: &Self| 1..c.sinks.len(),
    source output |_: &Self| 0
);

derive_builder!(EqualHardCoded; bits: usize = 2; value: usize = 1);

impl BuildChip for EqualHardCoded {
    type Output = EqualHardCodedChip;

    fn name(&self) -> String {
        "Equal Hardcoded".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let mut sinks = Vec::with_capacity(self.bits);

        let power_junction = builder.new_junction();
        sinks.push(power_junction.input());

        let and = builder.new_chip(&And::new(self.bits));

        let mut value = self.value;
        for index in 0..self.bits {
            match (value & 1) != 0 {
                true => sinks.push(and.inputs()[index]),
                false => {
                    let not = builder.new_chip(&Not::new());
                    builder.join(power_junction.output(), not.power());
                    sinks.push(not.input());
                    builder.join(not.output(), and.inputs()[index]);
                }
            }
            value >>= 1;
        }

        Self::Output {
            sinks,
            sources: [and.output()],
        }
    }
}

//
// Hardcoded
//
// Outputs a constant binary value.
// In theory this should optimize out to just be power in.
//

pub struct HardCodedChip {
    sinks: [Pin<Sink>; 1],
    sources: Vec<Pin<Source>>,
}

derive_chip!(HardCodedChip;
    sink power |_: &Self| 0,
    sources outputs |c: &Self| 0..c.sources.len()
);

derive_builder!(HardCoded; bits: usize = 2; value: usize = 1);

impl BuildChip for HardCoded {
    type Output = HardCodedChip;

    fn name(&self) -> String {
        "Hardcoded Number".into()
    }

    fn build_chip(&self, builder: &mut Builder) -> Self::Output {
        let power_junction = builder.new_junction();
        let off_junction = builder.new_junction();
        let mut sources = Vec::with_capacity(self.bits);

        let mut value = self.value;
        for _ in 0..self.bits {
            match (value & 1) != 0 {
                true => sources.push(power_junction.output()),
                false => sources.push(off_junction.output()),
            }
            value >>= 1;
        }

        Self::Output {
            sinks: [power_junction.input()],
            sources,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_compare_four_bit() {
        test_chip!(Comparator::new(4);
            ("alpha_in": 7), ("beta_in": 0) => "11000";
            ("alpha_in": 7), ("beta_in": 7) => "01110";
            ("alpha_in": 0), ("beta_in": 7) => "00011"
        );
    }

    #[test]
    fn test_compare_two_bit() {
        test_chip!(Comparator::new(2);
            ("alpha_in": 3), ("beta_in": 0) => "11000";
            ("alpha_in": 3), ("beta_in": 3) => "01110";
            ("alpha_in": 0), ("beta_in": 3) => "00011"
        );
    }

    #[test]
    fn test_equal_hardcoded() {
        test_chip!(EqualHardCoded::new(3, 3);
            ("inputs": 0) => ("output" 0: false);
            ("inputs": 1) => ("output" 0: false);
            ("inputs": 2) => ("output" 0: false);
            ("inputs": 3) => ("output" 0: true);
            ("inputs": 4) => ("output" 0: false);
            ("inputs": 5) => ("output" 0: false);
            ("inputs": 6) => ("output" 0: false);
            ("inputs": 7) => ("output" 0: false)
        );
    }

    #[test]
    fn test_hardcoded() {
        test_chip!(HardCoded::new(3, 0b11111);
            ("power": 0) => ("outputs": 0);
            ("power": 1) => ("outputs": 0b111)
        );
        test_chip!(HardCoded::new(3, 2);
            ("power": 0) => ("outputs": 0);
            ("power": 1) => ("outputs": 2)
        );
        test_chip!(HardCoded::new(4, 1);
            ("power": 0) => ("outputs": 0);
            ("power": 1) => ("outputs": 1)
        );
    }
}
