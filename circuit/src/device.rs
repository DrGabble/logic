use super::pinout::Pinout;

//
// Device
//
// An object that can have inputs set, then signal propogated by calling step, and then output read.
//

pub trait Device {
    fn write(&mut self, input: usize, value: bool);
    fn step(&mut self);
    fn read(&self, output: usize) -> bool;
    fn reset(&mut self);
}

//
// A Device with a Pinout
//
// NB usefull for testing (see TestCase etc)
//
pub trait NamedDevice: Device + Pinout {}

//
// System
//
// A Device composed of other Devices.
//

// pub struct System {
//     devices: Vec<Box<dyn NamedDevice<usize, usize>>>,
//     joins: Vec<(Pin<Source>, Pin<Sink>)>,
// }

// impl System {
//     pub fn new() -> Self {
//         Self {
//             devices: Vec::new(),
//             joins: Vec::new(),
//         }
//     }

//     pub fn add<D: NamedDevice<usize, usize> + 'static>(&mut self, device: D) -> DeviceHandle {
//         let device_index = self.devices.len();
//         let pinout = device.pinout();
//         self.devices.push(Box::new(device));
//         DeviceHandle {
//             device_index,
//             pinout,
//         }
//     }

//     pub fn join(&mut self, source: Pin<Source>, sink: Pin<Sink>) {
//         self.joins.push((source, sink));
//     }
// }

// impl Device<Pin<Sink>, Pin<Source>> for System {
//     fn set(&mut self, sink: Pin<Sink>, value: bool) {
//         self.devices[sink.device_index].set(sink.pin_index, value);
//     }

//     fn step(&mut self) {
//         for device in self.devices.iter_mut() {
//             device.step();
//         }
//         for (source, sink) in self.joins.iter() {
//             let value = self.devices[source.device_index].get(source.pin_index);
//             self.devices[sink.device_index].set(sink.pin_index, value);
//         }
//     }

//     fn gate_delay(&self) -> Option<usize> {
//         let mut total = 0;
//         for device in self.devices.iter() {
//             if let Some(delay) = device.gate_delay() {
//                 total += delay
//             } else {
//                 return None;
//             }
//         }
//         Some(total)
//     }

//     fn get(&self, source: Pin<Source>) -> bool {
//         self.devices[source.device_index].get(source.pin_index)
//     }
// }

// //
// // Device Handle
// //

// pub struct DeviceHandle {
//     device_index: usize,
//     pinout: Vec<NamedRange>,
// }

// impl DeviceHandle {
//     pub fn nth_sink(&self, index: usize) -> Pin<Sink> {
//         Pin {
//             device_index: self.device_index,
//             pin_index: index,
//             tag: Sink {},
//         }
//     }

//     pub fn find_sink(&self, name: &str) -> PinRange<Sink> {
//         let named_pins = self
//             .pinout
//             .iter()
//             .find(|n| n.name() == name)
//             .expect("Range not found");
//         assert!(named_pins.is_sink());
//         PinRange {
//             device_index: self.device_index,
//             range: named_pins.range().clone(),
//             tag: Sink {},
//         }
//     }

//     pub fn nth_source(&self, index: usize) -> Pin<Source> {
//         Pin {
//             device_index: self.device_index,
//             pin_index: index,
//             tag: Source {},
//         }
//     }

//     pub fn find_source(&self, name: &str) -> PinRange<Source> {
//         let named_pins = self
//             .pinout
//             .iter()
//             .find(|n| n.name() == name)
//             .expect("Range not found");
//         assert!(!named_pins.is_sink());
//         PinRange {
//             device_index: self.device_index,
//             range: named_pins.range().clone(),
//             tag: Source {},
//         }
//     }
// }

// //
// // Pin Range
// //

// pub struct PinRange<T: Clone> {
//     device_index: usize,
//     range: std::ops::Range<usize>,
//     tag: T,
// }

// impl<T: Clone> PinRange<T> {
//     pub fn nth(&self, index: usize) -> Pin<T> {
//         let pin_index = self.range.start + index;
//         assert!(index < self.range.end);
//         Pin {
//             device_index: self.device_index,
//             pin_index,
//             tag: self.tag.clone(),
//         }
//     }
// }

// //
// // Pin
// //

// #[derive(Clone)]
// pub struct Pin<T: Clone> {
//     device_index: usize,
//     pin_index: usize,
//     tag: T,
// }

// #[derive(Clone)]
// pub struct Sink {}

// #[derive(Clone)]
// pub struct Source {}
