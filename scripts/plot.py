#!/usr/bin/python3

import matplotlib.pyplot as plt
import csv
import numpy
import sys

limit = 100000

with open(sys.argv[1]) as csvfile:
    data = list(zip(*list(csv.reader(csvfile))))

print(data[0][0], data[1][0])

x = list(int(d) for d in data[0][:limit])
y = list(int(d) for d in data[1][:limit])

distance = list(abs(a-b) for a, b in zip(x, y))
hist = numpy.bincount(distance)

target = 64
less_than = sum(h for i, h in enumerate(hist) if i < target)
greater_than = sum(h for i, h in enumerate(hist) if i >= target)
print("Less than {target}: {}, greater or equal to {target}: {}".format(less_than, greater_than, target=target))

plt.figure(0)
plt.plot(hist)

plt.figure(1)
plt.scatter(x, y)
plt.show()

